#!/usr/bin/env python

"""
TODO
"""


############################### IMPORTS ################################ {{{1
########################################################################

import numpy as np
import math
import copy
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from enum import Enum
from collections import deque

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"



############################### LOCALS ################################# {{{1
########################################################################

def _normAngle(angle):
	return math.fmod(angle + 2. * math.pi, 2. * math.pi)

def _cart2pol(x, y):
	angle = np.arctan2(y, x)
	dist = np.sqrt(x * x + y * y)
	return [angle, dist]

def _intersect(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y):
	s1_x = p1_x - p0_x
	s1_y = p1_y - p0_y
	s2_x = p3_x - p2_x
	s2_y = p3_y - p2_y
	s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y)
	t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y)
	if s >= 0 and s <= 1 and t >= 0 and t <= 1:
		intX = p0_x + (t * s1_x)
		intY = p0_y + (t * s1_y)
		return intX, intY
	return None


############################### DATASET ################################ {{{1
########################################################################


class DataSet(object):
	def __init__(self, frameSkip = None):
		self.currentTimeStep = 0
		self.frameSkip = frameSkip

	def fromFile(self, filename):
		self.filename = filename
		with open(filename, "r") as source:
			data_full = np.loadtxt(source, skiprows = 1)
		if self.frameSkip != None:
			data_full = data_full[::self.frameSkip]
		self.time = data_full[:,0]
		nbColumns = 2
		self.nbAgents = int((data_full.shape[1] - 1) / nbColumns)
		#self.agents = [ np.array(zip(data_full[:,1+i*nbColumns], data_full[:,1+i*nbColumns+1], data_full[:,1+i*nbColumns+2]), dtype = [('x', 'float64'), ('y', 'float64'), ('theta', 'float64')]) for i in range(self.nbAgents) ]
		self.agents = [ np.array(list(zip(data_full[:,1+i*nbColumns], data_full[:,1+i*nbColumns+1], np.zeros(data_full.shape[0]))), dtype = [('x', 'float64'), ('y', 'float64'), ('theta', 'float64')]) for i in range(self.nbAgents) ]
		#self.agents = [ np.array(zip(data_full[:,1+i*nbColumns], data_full[:,1+i*nbColumns+1]), dtype = [('x', 'float64'), ('y', 'float64')]) for i in range(self.nbAgents) ]

#		for a in self.agents:
#			for i in range(1, a.shape[0]):
#				a[i]['theta'] = math.atan2(a['y'][i] - a['y'][i-1], a['x'][i] - a['x'][i-1])

		thres = 0.010
		for a in self.agents:
			for i in range(2, a.shape[0]-2):
				if np.abs(a['x'][i+2] - a['x'][i-2]) < thres and np.abs(a['y'][i+2] - a['x'][i-2]) < thres:
					a['theta'][i] = a['theta'][i-1]
				else:
					a['theta'][i] = math.atan2(a['y'][i+2] - a['y'][i-2], a['x'][i+2] - a['x'][i-2])
				if np.isnan(a[i]['theta']):
					a['x'][i] = a['y'][i] = np.nan

	def getNextPos(self):
		x = np.empty(shape = len(self.agents))
		y = np.empty(shape = len(self.agents))
		theta = np.empty(shape = len(self.agents))
		self.currentTimeStep += 1
		for i in range(len(self.agents)):
			x[i] = self.agents[i]['x'][self.currentTimeStep]
			y[i] = self.agents[i]['y'][self.currentTimeStep]
			theta[i] = self.agents[i]['theta'][self.currentTimeStep]
		return x, y, theta, self.time[self.currentTimeStep]




############################### VISION ################################# {{{1
########################################################################


# INPUTS:
#  - angle to other agents
#  - distances to other agents
#  - angle of other agents wrt focal agent angle
#  - linear speed of other agents wrt focal agent speed
#  - angle to nearest wall
#  - distance to nearest wall


class FishSim(object):
	agentLength = 0.03
	distancePerceptionAgents = 2.0
	distancePerceptionWalls = 2.0
	#fov = 169.5 / 180. * np.pi
	#nbSensors = 27 #54 #68 #340 # 339
	maxLinearSpeed = 0.20
	minDeltaAngle = -30. / 180. * np.pi # -30. / 180. * np.pi
	maxDeltaAngle = 30. / 180. * np.pi #30. / 180. * np.pi
	arenaX = 1.0
	arenaY = 1.0


	def __init__(self, nbAgents):
		self.time = 0.
		self.prevTime = 0.
		self.agents = [ {'x':0.0, 'y':0.0, 'theta':0.0} for i in range(nbAgents) ]
		self.prevAgents = [ {'x':0.0, 'y':0.0, 'theta':0.0} for i in range(nbAgents) ]
		self.tailPos = [ (0., 0.) for i in range(nbAgents) ]
		self.deltaAngles = np.zeros(shape=(nbAgents))
		self.prevDeltaAngles = np.zeros(shape=(nbAgents))
		self.instLinearSpeed = np.zeros(shape=(nbAgents))
		self.prevInstLinearSpeed = np.zeros(shape=(nbAgents))
		self.inputs = np.zeros(shape=(nbAgents, self.nbInputs()))
		self.outputs = np.zeros(shape=(nbAgents, self.nbOutputs()))

		self.angleToAgents = np.zeros(shape=(nbAgents, nbAgents))
		self.distToAgents = np.zeros(shape=(nbAgents, nbAgents))
		self.deltaAngleToAgentsAngle = np.zeros(shape=(nbAgents, nbAgents))
		self.deltaLinSpeedToAgentsLinSpeed = np.zeros(shape=(nbAgents, nbAgents))
		self.angleToNearestWall = np.zeros(shape=(nbAgents))
		self.distToNearestWall = np.zeros(shape=(nbAgents))

		#self._updateAgents()


	def _outOfBoundsCorrection(self, x, y, theta):
		# Verify if out of bounds
		outOfBounds = False
		if x > self.arenaX:
			x = self.arenaX - 0.01
			theta = math.pi - theta
			outOfBounds = True
		elif x < 0.0:
			x = 0.01
			theta = math.pi - theta
			outOfBounds = True
		if y > self.arenaY:
			y = self.arenaY - 0.01
			theta = - theta
			outOfBounds = True
		elif y < 0.0:
			y = 0.01
			theta = - theta
			outOfBounds = True
		return x, y, theta, outOfBounds


	def _updateAgents(self, nextPosX, nextPosY, nextPosTheta, nextTime):
		self.prevTime = self.time
		self.time = nextTime
		deltaTime = self.time - self.prevTime

		for i in range(len(self.agents)):
			oldTheta = self.agents[i]['theta']
			self.agents[i]['x'], self.agents[i]['y'], self.agents[i]['theta'], outOfBounds = self._outOfBoundsCorrection(nextPosX[i], nextPosY[i], nextPosTheta[i])

			self.prevDeltaAngles[i] = self.deltaAngles[i]
			self.deltaAngles[i] = self.agents[i]['theta'] - oldTheta
			if self.deltaAngles[i] > self.maxDeltaAngle:
				self.deltaAngles[i] = self.maxDeltaAngle
			elif self.deltaAngles[i] < self.minDeltaAngle:
				self.deltaAngles[i] = self.minDeltaAngle

			diffX = self.prevAgents[i]['x'] - self.agents[i]['x']
			diffY = self.prevAgents[i]['y'] - self.agents[i]['y']
			self.prevInstLinearSpeed[i] = self.instLinearSpeed[i]
			#self.instLinearSpeed[i] = np.sqrt(diffX * diffX + diffY * diffY) / (1. * self.dt)
			self.instLinearSpeed[i] = np.sqrt(diffX * diffX + diffY * diffY) / deltaTime
			#print(i, diffX, diffY, self.instLinearSpeed[i])
			if self.instLinearSpeed[i] > self.maxLinearSpeed:
				self.instLinearSpeed[i] = self.maxLinearSpeed
			self.tailPos[i] = (
					self.agents[i]['x'] - math.cos(self.agents[i]['theta']) * self.agentLength,
					self.agents[i]['y'] - math.sin(self.agents[i]['theta']) * self.agentLength)
		#print(self.prevAgents)
		#print(self.agents)
		#print(self.instLinearSpeed, self.deltaAngles, deltaTime)


	def _computeAgentFeatures(self, i):
		a0 = self.agents[i]

		for j in range(len(self.agents)):
			if i == j:
				continue
			a1 = self.agents[j]
			relativeAgentsHeadsPosX = a1['x'] - a0['x']
			relativeAgentsHeadsPosY = a1['y'] - a0['y']
			relativeAgentsTailPosX = self.tailPos[j][0] - self.tailPos[i][0]
			relativeAgentsTailPosY = self.tailPos[j][1] - self.tailPos[i][1]
			headRetinaX = relativeAgentsHeadsPosX * math.cos(a0['theta']) + relativeAgentsHeadsPosY * math.sin(a0['theta'])
			headRetinaY = - relativeAgentsHeadsPosX * math.sin(a0['theta']) + relativeAgentsHeadsPosY * math.cos(a0['theta'])
			tailRetinaX = relativeAgentsTailPosX * math.cos(a0['theta']) + relativeAgentsTailPosY * math.sin(a0['theta'])
			tailRetinaY = - relativeAgentsTailPosX * math.sin(a0['theta']) + relativeAgentsTailPosY * math.cos(a0['theta'])
			#headAngle = _normAngle(math.atan2(headRetinaX, headRetinaY))
			headAngle = math.atan2(headRetinaX, headRetinaY)
			distAgent = math.sqrt(headRetinaX * headRetinaX + headRetinaY * headRetinaY)
			#tailAngle = _normAngle(math.atan2(tailRetinaY, tailRetinaX))
			tailAngle = math.atan2(tailRetinaY, tailRetinaX)
			#agentCenter = _normAngle(math.atan2(math.cos(headAngle) + math.cos(tailAngle), math.sin(headAngle) + math.sin(tailAngle)))
			agentCenter = math.atan2(math.cos(headAngle) + math.cos(tailAngle), math.sin(headAngle) + math.sin(tailAngle))
			#diffDirections = _normAngle(a1['theta'] - a0['theta'])
			diffDirections = a1['theta'] - a0['theta']
			#agentLength = abs(_normAngle(headAngle) - _normAngle(tailAngle))
			#if agentLength > math.pi:
			#	agentLength = 2.0 * math.pi - agentLength

			self.distToAgents[i, j] = distAgent / self.distancePerceptionAgents
			if self.distToAgents[i, j] > 1.0:
				self.distToAgents[i, j] = 1.0
			elif self.distToAgents[i, j] < 0.0:
				self.distToAgents[i, j] = 0.0
			self.angleToAgents[i, j] = agentCenter / (2. * math.pi)
			#self.angleToAgents[i, j] = (agentCenter + math.pi) / (2. * math.pi)
			self.deltaAngleToAgentsAngle[i, j] = diffDirections / (2. * math.pi)
			#self.deltaAngleToAgentsAngle[i, j] = (diffDirections + 2. * math.pi) / (4. * math.pi)
			self.deltaLinSpeedToAgentsLinSpeed[i, j] = (self.instLinearSpeed[i] - self.instLinearSpeed[j]) / self.maxLinearSpeed
			if self.deltaLinSpeedToAgentsLinSpeed[i, j] > 1.0:
				self.deltaLinSpeedToAgentsLinSpeed[i, j] = 1.0




	def _computeWallFeatures(self, i):
		a0 = self.agents[i]

		# RMQ: 0 = left wall, 1 = top wall, 2 = right wall, 3 = bottom wall
		#wallCoords = [ ((0.0, 0.0), (0.0, self.arenaY)), ((0.0, self.arenaY), (self.arenaX, self.arenaY)), ((self.arenaX, self.arenaY), (self.arenaX, 0.0)), ((self.arenaX, 0.0), (0.0, 0.0)) ]
		wallCoords_ = [ ((0.0, -42.0), (0.0, self.arenaY+42.0)), ((-42.0, self.arenaY), (self.arenaX+42.0, self.arenaY)), ((self.arenaX, self.arenaY+42.0), (self.arenaX, -42.0)), ((self.arenaX+42.0, 0.0), (-42.0, 0.0)) ]
		wallXPosTmp = np.array([0.0, a0['x'], self.arenaX, a0['x']]) - a0['x']
		wallYPosTmp = np.array([a0['y'], self.arenaY, a0['y'], 0.0]) - a0['y']

		wallRetinaX = wallXPosTmp * np.cos(a0['theta']) + wallYPosTmp * np.sin(a0['theta'])
		wallRetinaY = -wallXPosTmp * np.sin(a0['theta']) + wallYPosTmp * np.cos(a0['theta'])
		wallCenterAngleTmp, distWallTmp = _cart2pol(wallRetinaX, wallRetinaY)

		minDistIndex = np.argmin(distWallTmp)
		minDist = distWallTmp[minDistIndex]
		#minDistAngle = _normAngle(wallCenterAngleTmp[minDistIndex] + np.abs(np.arccos(distWallTmp[minDistIndex] / self.distancePerceptionWalls)))
		#minDistAngle = wallCenterAngleTmp[minDistIndex] + np.abs(np.arccos(distWallTmp[minDistIndex] / self.distancePerceptionWalls))
		#self.angleToNearestWall[i] = minDistAngle / (2. * math.pi)
		self.angleToNearestWall[i] = wallCenterAngleTmp[minDistIndex] / math.pi
		#self.angleToNearestWall[i] = (minDistAngle + math.pi) / (2. * math.pi)
		self.distToNearestWall[i] = minDist / self.distancePerceptionWalls



	def _computeInputs(self, i):
		currentIndex = 0

		for j in range(len(self.agents)):
			if i != j:
				self.inputs[i, currentIndex] = np.nan_to_num(self.angleToAgents[i,j])
				currentIndex += 1
				self.inputs[i, currentIndex] = np.nan_to_num(self.distToAgents[i,j])
				currentIndex += 1
				self.inputs[i, currentIndex] = np.nan_to_num(self.deltaAngleToAgentsAngle[i,j])
				currentIndex += 1
				#self.inputs[i, currentIndex] = np.nan_to_num(self.deltaLinSpeedToAgentsLinSpeed[i,j])
				#currentIndex += 1
				self.inputs[i, currentIndex] = np.nan_to_num(self.prevInstLinearSpeed[j]) / self.maxLinearSpeed
				currentIndex += 1

		self.inputs[i, currentIndex] = np.nan_to_num(self.angleToNearestWall[i])
		currentIndex += 1
		self.inputs[i, currentIndex] = np.nan_to_num(self.distToNearestWall[i])
		currentIndex += 1

		self.inputs[i, currentIndex] = (self.prevDeltaAngles[i] - self.minDeltaAngle) / (self.maxDeltaAngle - self.minDeltaAngle)
		currentIndex += 1
		self.inputs[i, currentIndex] = self.prevInstLinearSpeed[i] / self.maxLinearSpeed
		currentIndex += 1

		self.inputs[i, currentIndex] = self.agents[i]['x']
		currentIndex += 1
		self.inputs[i, currentIndex] = self.agents[i]['y']
		currentIndex += 1
		assert(currentIndex == self.nbInputs())


	def nbInputs(self):
		nbAgents = len(self.agents)
		res = 2 # prevInstLinearSpeed, prevDeltaAngles
		res += (nbAgents - 1) # self.angleToAgents
		res += (nbAgents - 1) # self.distToAgents
		res += (nbAgents - 1) # self.deltaAngleToAgentsAngle
		res += (nbAgents - 1) # self.deltaLinSpeedToAgentsLinSpeed
		res += 1 # self.angleToNearestWall
		res += 1 # self.distToNearestWall
		#res += 2 # x, y
		return res


	def _computeOutputs(self, i):
		currentIndex = 0
		self.outputs[i, currentIndex] = (self.deltaAngles[i] - self.minDeltaAngle) / (self.maxDeltaAngle - self.minDeltaAngle)
		currentIndex += 1
		self.outputs[i, currentIndex] = self.instLinearSpeed[i] / (self.maxLinearSpeed)
		currentIndex += 1
		assert(currentIndex == self.nbOutputs())


	def nbOutputs(self):
		return 2


	def step(self, nextPosX, nextPosY, nextPosTheta, nextTime):
		self._updateAgents(nextPosX, nextPosY, nextPosTheta, nextTime)
		self.time = nextTime

		# Compute Agent Features
		for i in range(len(self.agents)):
			self._computeAgentFeatures(i)
			self._computeWallFeatures(i)

		# Compute Agent Inputs/Outputs
		for i in range(len(self.agents)):
			self._computeInputs(i)
			self._computeOutputs(i)

		for i in range(len(self.agents)):
			self.prevAgents[i] = copy.copy(self.agents[i])




############################ BASE FUNCTIONS ############################ {{{1
########################################################################



def animationResults(inputFilename, interactive = False):
	data = DataSet()
	data.fromFile(inputFilename)
	sim = FishSim(data.nbAgents)

	#fig, (ax0, ax1, ax2, ax3) = plt.subplots(4, 1)
	fig = plt.figure(0)
	ax0 = plt.subplot2grid((4,1), (0,0), rowspan=3)
	ax1 = plt.subplot2grid((4,1), (3,0), colspan=6)
	#ax2 = plt.subplot2grid((3,1), (2,0), colspan=6)

	ax0.set_xlim(0.0, 1.0)
	ax0.set_ylim(0.0, 1.0)
	ax0.set_aspect(1.0)
	colormap = plt.cm.jet
	nbAgents = len(data.agents)
	ax0.set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, nbAgents)])
	points = [ax0.plot([], [], marker='o', linestyle='None')[0] for i in range(nbAgents)]
	lines = [ax0.plot([], [], lw=2)[0] for x in range(nbAgents)]
	nbFrame = 1
	textNbFrame = plt.text(0.1, 0.9, "#" + str(sim.time),
			horizontalalignment='center',
			verticalalignment='center',
			transform = ax0.transAxes)

	imgInputs = ax1.imshow(np.random.random((nbAgents, sim.nbInputs())), interpolation='none', vmin=0., vmax=1., cmap='Greys')
	#imgDistWalls = ax2.imshow(np.random.random((nbAgents, sim.nbSensors)), interpolation='none', vmin=0., vmax=1., cmap='Greys')

	plt.tight_layout()

	for t in range(data.time.shape[0]):
		for i in range(nbAgents):
			points[i].set_data(sim.agents[i]['x'], sim.agents[i]['y'])
			tailPosX = sim.agents[i]['x'] - math.cos(sim.agents[i]['theta']) * sim.agentLength
			tailPosY = sim.agents[i]['y'] - math.sin(sim.agents[i]['theta']) * sim.agentLength
			lines[i].set_data([sim.agents[i]['x'], sim.tailPos[i][0]], [sim.agents[i]['y'], sim.tailPos[i][1]])

		textNbFrame.set_text("#%.4f" % sim.time)

		imgInputs.set_array(sim.inputs)
		#imgDistWalls.set_array(sim.distWalls)

		#print("outputs: ", sim.outputs)
		print("inputs ", sim.inputs[4])

		nextPosX, nextPosY, nextPosTheta, nextTime = data.getNextPos()
		sim.step(nextPosX, nextPosY, nextPosTheta, nextTime)

		nbFrame += 1
		plt.pause(0.001)
		if t > 60 and interactive:
			plt.waitforbuttonpress()




################################# MAIN ################################# {{{1
########################################################################

if __name__ == "__main__":
	from optparse import OptionParser
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)
	parser.add_option("-i", "--inputFilename", dest = "inputFilename", help = "Filename of the input data file")
	parser.add_option("-I", "--interactive", action="store_true", dest="interactive", default=False, help = "Wait for key between each time step")
	(options, args) = parser.parse_args()

	animationResults(options.inputFilename, options.interactive)


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
