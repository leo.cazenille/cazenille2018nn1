#!/usr/bin/env python

"""
Calibrate the NN using CMA-ES
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

# Use only CPU in Keras
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""

import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt

import pickle
import math

from evoKeras import *
from ESN import *
from SESN import *
from evoLBN import *

from fitness import *
import sys
import numpy as np
from deap import algorithms
from deap import base
from deap import benchmarks
from deap import cma
from deap import creator
from deap import tools
import array
import copy

import tensorflow as tf
from keras import backend as K
#graph = tf.get_default_graph()

import multiprocessing
import threading
import gc

np.seterr(divide='ignore', invalid='ignore')



########### OPTIM ########### {{{1


def evalFit(indiv):
	global f

	resList = []
	allScoresList = []

	for i in range(nbSimsPerEvals):
		config = tf.ConfigProto(intra_op_parallelism_threads=1,
							inter_op_parallelism_threads=1, 
							allow_soft_placement=True)    
		session = tf.Session(config=config)
		K.set_session(session)

		#print("DEBUGEVAL3: ", threading.get_ident(), multiprocessing.current_process())
		fC = copy.deepcopy(f)
		##with graph.as_default():
		#with tf.Graph().as_default(), tf.Session() as sess:
		#	tf.contrib.keras.backend.set_session(sess)
		#	res, allScores = fC.fitness(indiv, nnBase, annStepsPerSimStep, fC.referencePath, simulateOnlyLastAgent)
		#	#print("DEBUGRES: ", res, allScores)
		#	K.clear_session()
		#	sess.close()

		resInstance, allScoresInstance = fC.fitness(indiv, nnBase, annStepsPerSimStep, fC.referencePath, simulateOnlyLastAgent)
		K.clear_session()
		resList.append(resInstance)
		allScoresList.append(allScoresInstance)

	res = np.mean(resList)
	#allScores = np.mean(allScoresList, axis = 0)

	return (res,)



creator.create("FitnessMax", base.Fitness, weights = (1.0,))
#creator.create("Individual", list, fitness=creator.FitnessMax)
creator.create("Individual", array.array, typecode="d", fitness=creator.FitnessMax, strategy=None)
creator.create("Strategy", array.array, typecode="d")

def initES(icls, scls, size, imin, imax, smin, smax):
    ind = icls(np.random.uniform(imin, imax) for _ in range(size))
    ind.strategy = scls(np.random.uniform(smin, smax) for _ in range(size))
    return ind

#IND_SIZE = nnBase.getNbWeights()
#MIN_VALUE, MAX_VALUE = -0.4, 0.4
#MIN_STRAT, MAX_STRAT = -1.00, 1.00

toolbox = base.Toolbox()
toolbox.register("evaluate", evalFit)
toolbox.register("individual", initES, creator.Individual, creator.Strategy)



########### MAIN ########### {{{1
if __name__ == "__main__":
	np.seterr(divide='ignore', invalid='ignore')

	from optparse import OptionParser
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-i", "--inputDir", dest = "inputDir", default = "",
			help = "Path of input data files")
	parser.add_option("-I", "--inputGenerationDir", dest = "inputGenerationDir", default = None,
			help = "Path of input data files")
	parser.add_option("-o", "--outputPrefix", dest = "outputPrefix", default = None,
			help = "Path prefix of output files")
	parser.add_option("-a", "--arenaFilename", dest = "arenaFilename", default = "FishModel/arenas/SetupLargeModelzoneBlack70x70v2.png",
			help = "Filename of arena map file")
	parser.add_option("-z", "--nbZones", dest = "nbZones", default = 6,
			help = "Number of zones")
	parser.add_option("-s", "--seed", dest = "seed", default = 42,
			help = "Random number generator seed")
	parser.add_option("-n", "--nbRuns", dest = "nbRuns", default = 10,
			help = "Number of runs per eval")
	parser.add_option("-S", "--nbSteps", dest = "nbSteps", default = 26996,
			help = "Number of steps per eval")
	parser.add_option("--nbStepsGeneration", dest = "nbStepsGeneration", default = 26996,
			help = "Number of steps when generating simulation results on best individual")
	parser.add_option("-g", "--nbGen", dest = "nbGen", default = 200,
			help = "Number of generations")
	parser.add_option("-L", "--lambda", dest = "nbIndiv", default = 40,
			help = "Number of individuals per generation")
	parser.add_option("-F", "--FPS", dest = "FPS", default = 15,
			help = "Frames per seconds")

	parser.add_option("-T", "--annType", dest = "annType", default = "ESN",
			help = "Type of ANN (MLP, ESN, SESN)")

	parser.add_option("--annStepsPerSimStep", dest = "annStepsPerSimStep", default = 1,
			help = "Number of ANN steps per simulation step")
	parser.add_option("--nbSimsPerEvals", dest = "nbSimsPerEvals", default = 1,
			help = "Number of simulations per evaluation (useful for stochastic ANN)")

	parser.add_option("--simulateOnlyLastAgent", action="store_true", default = False, dest = "simulateOnlyLastAgent",
			help = "Simulate only the last agents (others are taken from dataset)")


	(options, args) = parser.parse_args()

	np.random.seed(int(options.seed))
	annStepsPerSimStep = int(options.annStepsPerSimStep)
	nbSimsPerEvals = int(options.nbSimsPerEvals)
	simulateOnlyLastAgent = options.simulateOnlyLastAgent
	prefix = str(options.outputPrefix)
	f = FitnessCalibration(options.inputDir, mapFilename = options.arenaFilename, nbZones=int(options.nbZones), seed=int(options.seed), nbRuns=int(options.nbRuns), nbSteps=int(options.nbSteps), dt=1./float(options.FPS))
	inputGenerationDir = options.inputGenerationDir or options.inputDir
	fGeneration = FitnessCalibration(options.inputDir, mapFilename = options.arenaFilename, nbZones=int(options.nbZones), seed=int(options.seed), nbRuns=int(options.nbRuns), nbSteps=int(options.nbStepsGeneration), dt=1./float(options.FPS))


	# Create NN
	nbInputs = 20
	nbOutputs = 2
	nbHiddenNeurons = 100
	reservoirSize = nbHiddenNeurons
	if options.annType == "ESN":
		from ESN import *
		nnBase = ESN(nbInputs, nbOutputs, reservoirSize, 0.10, 0.88, allowInputToOutputDirectConnections=False, allowOutputSelfRecurrentConnections=False, allowInputToReservoirConnections=True, fixedInputToReservoirConnections=False, allowOutputToReservoirConnections=True)
	elif options.annType == "SESN":
		from SESN import *
		nnBase = SESN(nbInputs, nbOutputs, reservoirSize, 0.10, 0.88, allowInputToOutputDirectConnections=False, allowOutputSelfRecurrentConnections=False, allowInputToReservoirConnections=True, fixedInputToReservoirConnections=False, allowOutputToReservoirConnections=True, densityOfStochasticConnections=0.05)
	elif options.annType == "MLP":
		from keras.models import Model, Input, Sequential
		from keras.layers import Dense, Activation
		#from evoKeras import *

		model = Sequential()
		model.add(Dense(units=nbHiddenNeurons, input_dim=nbInputs))
		model.add(Activation('tanh'))
		model.add(Dense(units=nbOutputs))
		model.add(Activation('tanh'))
		#model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) # XXX do NOT use compile in main thread
		#nbWeights = nbInputs * nbHiddenNeurons + nbHiddenNeurons + nbHiddenNeurons * nbOutputs + nbOutputs
		nnBase = EvolvableKerasModel(model, nbInputs, nbOutputs)

	elif options.annType == "LBN":
		n_hidden = [100, 100] # [100]
		det_activations = ['sigmoid', 'sigmoid', 'sigmoid'] # ['sigmoid', 'sigmoid'] 
		stoch_activations = ['sigmoid', 'sigmoid', 'sigmoid'] # ['sigmoid', 'sigmoid'] 
		stoch_n_hidden = [50, 50] # [5]
		nnBase = EvolvableLBN(nbInputs, nbOutputs, n_hidden, det_activations, stoch_activations, stoch_n_hidden)


	N = nnBase.getNbWeights()
	nbGen = int(options.nbGen)
	nbIndiv = int(options.nbIndiv)
	print("N = ", N)

	#centroids = np.array([0.5] * nnBase.getNbWeights())
	centroids = np.array([0.0] * nnBase.getNbWeights())

	# The CMA-ES algorithm takes a population of one individual as argument
	# The centroid is set to a vector of 5.0 see http://www.lri.fr/~hansen/cmaes_inmatlab.html
	# for more details about the rastrigin and other tests for CMA-ES
	#strategy = cma.Strategy(centroid=[0.5]*N, sigma=0.5, lambda_=2*N)
	strategy = cma.Strategy(centroids, sigma=0.5, lambda_=nbIndiv)
	toolbox.register("generate", strategy.generate, creator.Individual)
	#toolbox.register("generate", strategy.generate, generatePop)
	toolbox.register("update", strategy.update)

	halloffame = tools.HallOfFame(1)
	stats = tools.Statistics(lambda ind: ind.fitness.values)
	stats.register("avg", np.mean)
	stats.register("std", np.std)
	stats.register("min", np.min)
	stats.register("max", np.max)

	logbook = tools.Logbook()
	logbook.header = "gen", "evals", "std", "min", "avg", "max"

	# Objects that will compile the data
	sigma = np.ndarray((nbGen,1))
	axis_ratio = np.ndarray((nbGen,1))
	diagD = np.ndarray((nbGen,N))
	fbest = np.ndarray((nbGen,1))
	best = np.ndarray((nbGen,N))
	std = np.ndarray((nbGen,N))

	pool = multiprocessing.Pool()
	toolbox.register("map", pool.map)


	for gen in range(nbGen):
		# Generate a new population
		population = toolbox.generate()
		## Normalize (remove negative values)
		#for ind in population:
		#	for i in range(len(ind)):
		#		ind[i] = np.abs(ind[i])

		# Evaluate the individuals
		fitnesses = toolbox.map(toolbox.evaluate, population)
		for ind, fit in zip(population, fitnesses):
			ind.fitness.values = fit

		# Update the strategy with the evaluated individuals
		toolbox.update(population)

		# Update the hall of fame and the statistics with the
		# currently evaluated population
		halloffame.update(population)
		record = stats.compile(population)
		logbook.record(evals=len(population), gen=gen, **record)

		if True: #if verbose:
			print(logbook.stream)
			#print(f.indivToParams(halloffame[0]))
			sys.stdout.flush()

		# Save more data along the evolution for latter plotting
		# diagD is sorted and sqrooted in the update method
		sigma[gen] = strategy.sigma
		axis_ratio[gen] = max(strategy.diagD)**2/min(strategy.diagD)**2
		diagD[gen, :N] = strategy.diagD**2
		fbest[gen] = halloffame[0].fitness.values
		best[gen, :N] = halloffame[0]
		std[gen, :N] = np.std(population, axis=0)

		# Empty cache
		while gc.collect() > 0:
			pass

	bestever = [best[0]]
	fbestever = [fbest[0]]
	for gen in range(1, nbGen):
		if fbest[gen] > fbestever[-1]:
			fbestever.append(fbest[gen])
			bestever.append(best[gen])
		else:
			fbestever.append(fbestever[-1])
			bestever.append(bestever[-1])

	outputDict = {'best': best, 'fbest': fbest, 'bestever': bestever, 'fbestever': fbestever}
	# Save the best individuals
	pickle.dump(outputDict, open(os.path.join(prefix, "best.p"), "wb"))
	print("fbestever:", fbestever[-1])

	nn = nnBase.clone()
	nn.reinit()
	#res, allScores = f.fitness(bestever[-1], nn, f.referencePath, options.outputPrefix)
	res, allScores = fGeneration.fitness(bestever[-1], nn, annStepsPerSimStep, inputGenerationDir, simulateOnlyLastAgent, options.outputPrefix)
	print("bestever Scores: ", res, allScores)


	# The x-axis will be the number of evaluations
	x = list(range(0, strategy.lambda_ * nbGen, strategy.lambda_))
	avg, max_, min_ = logbook.select("avg", "max", "min")
	fig = plt.figure()
	plt.subplot(2, 2, 1)
	plt.semilogy(x, avg, "--b")
	plt.semilogy(x, max_, "--b")
	plt.semilogy(x, min_, "-b")
	plt.semilogy(x, fbest, "-c")
	plt.semilogy(x, sigma, "-g")
	plt.semilogy(x, axis_ratio, "-r")
	plt.grid(True)
	plt.title("blue: f-values, green: sigma, red: axis ratio")

	plt.subplot(2, 2, 2)
	plt.plot(x, best)
	plt.grid(True)
	plt.title("Object Variables")

	plt.subplot(2, 2, 3)
	plt.semilogy(x, diagD)
	plt.grid(True)
	plt.title("Scaling (All Main Axes)")

	plt.subplot(2, 2, 4)
	plt.semilogy(x, std)
	plt.grid(True)
	plt.title("Standard Deviations in All Coordinates")

	#plt.show()
	plt.tight_layout()
	fig.savefig("optimStats.pdf")
	plt.close(fig)


	fig, ax = plt.subplots(figsize=(10, 10))
	fig.subplots_adjust(hspace=0.12, top=0.75)
	#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
	#plt.rc('xtick', labelsize=20)
	#plt.rc('ytick', labelsize=20)
	#rc('text', usetex=True)
	ax.set_ylim([0.0, 1.0])
	#ax.set_xlim([0.0, 1.0])
	ax.set_ylabel('Performance', fontsize = 25)
	ax.set_xlabel('Evaluations', fontsize = 25)
	plt.tight_layout()
	plt.plot(x, fbest, "k")
	plt.grid(False)
	fig.savefig(os.path.join(prefix, "bestEverFitness.pdf"))
	plt.close(fig)

	pool.close()
	pool.terminate()


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
