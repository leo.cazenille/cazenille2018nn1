#!/usr/bin/env python3

"""
Make density of presence plots
"""

import os
import sys
#from dataAnalysis import *
#from dataPlot import *
from math import *
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors
from matplotlib.collections import LineCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"



def plotspeedtraj(position_xy, color_values, size, nfish, outputname, cmap, drawCbar = True, cbarFile = None, **kwargs):
	ii = 0
	#for ii in range (0, nfish):
	xy = position_xy[:, ii*2 : ii*2+2]
	xy = xy.reshape(-1, 1, 2)
	ColorValues = color_values[1:-1]
	segments = np.hstack([xy[:-1], xy[1:]])#[~np.isnan(ColorValues[:,0]),:,:]
	#coll = LineCollection(segments, cmap=plt.cm.rainbow)
	coll = LineCollection(segments, cmap=cmap)
	coll.set_array(ColorValues[~np.isnan(ColorValues)])

	fig, ax  = plt.subplots(figsize=(8,8))
	cax = ax.add_collection(coll)
	cax.set_clim(vmin=0, vmax= 0.20)

	ax.set_xlim(0,size)
	ax.set_ylim(0,size)
	ax.set_xticks([])
	ax.set_yticks([])

	if drawCbar and cbarFile == None:
		divider = make_axes_locatable(ax)
		cax2 = divider.append_axes("right", size="5%", pad=0.05)
		cbar = fig.colorbar(cax, cax=cax2, format="%.2f")
		cbar.ax.tick_params(labelsize=30)
		for label in cbar.ax.get_yticklabels()[1::2]:
			label.set_visible(False)

	ax.set_aspect('equal')
	ax.autoscale_view()
	plt.tight_layout()
	fig.savefig(outputname)

	if drawCbar and cbarFile != None:
		plt.gca().set_visible(False)
		fig.set_size_inches(2.0,8,forward=True)
		cax2 = plt.axes([0.05, 0.10, 0.2, 0.80])
		cbar = fig.colorbar(cax, cax=cax2, format="%.2f")
		cbar.ax.tick_params(labelsize=40)
		for label in cbar.ax.get_yticklabels()[1::2]:
			label.set_visible(False)
		ax.set_aspect('equal')
		ax.autoscale_view()
		fig.savefig(cbarFile)

	plt.close(fig)


def plotSubTrajInGridMultiIndiv(x, y, size, outputname, **kwargs):
	#f = plt.figure(figsize=(3,3))
	#colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('Greys_r'))
	colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('Set1'))
	#colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('tab10'))
	nbSubX = kwargs.get('nbSubX') or 6
	nbSubY = kwargs.get('nbSubY') or 5
	binSize = int(x.shape[0] / (nbSubX * nbSubY))

	#f = plt.figure(figsize=(3*nbSubX,3*nbSubY))
	f = plt.figure(figsize=(30,30))

	for i in range(nbSubX):
		for j in range(nbSubY):
			index = i*nbSubY+j
			ax = f.add_subplot(nbSubX, nbSubY, index+1)
			ax.set_xlim(0, size)
			ax.set_ylim(0, size)
			ax.set_xticks([])
			ax.set_yticks([])

			for k in range(x.shape[1]):
				if kwargs.get('colors') is not None and k < len(kwargs['colors']):
					continue
				else:
					colorVal = colorMap.to_rgba(k)
				X = x[binSize*index:binSize*(index+1), k]
				Y = y[binSize*index:binSize*(index+1), k]
				X2 = [1. - X[0]]
				Y2 = [Y[0]]
				for l in range(1, len(X)):
					if abs(X[l] - X[l-1]) > 0.3 or abs(Y[l] - Y[l-1]) > 0.3:
						X2.append(np.nan)
						Y2.append(np.nan)
					else:
						X2.append(1. - X[l])
						Y2.append(Y[l])
				#ax.plot(x[binSize*index:binSize*(index+1), k], y[binSize*index:binSize*(index+1), k], color = colorVal)
				ax.plot(X2, Y2, color = colorVal)

			if kwargs.get('colors') is not None:
				for k in range(min(x.shape[1], len(kwargs['colors']))):
					colorVal = kwargs['colors'][k]
					X = x[binSize*index:binSize*(index+1), k]
					Y = y[binSize*index:binSize*(index+1), k]
					X2 = [1. - X[0]]
					Y2 = [Y[0]]
					for l in range(1, len(X)):
						if abs(X[l] - X[l-1]) > 0.3 or abs(Y[l] - Y[l-1]) > 0.3:
							X2.append(np.nan)
							Y2.append(np.nan)
						else:
							X2.append(1. - X[l])
							Y2.append(Y[l])
					ax.plot(X2, Y2, color = colorVal)

	plt.tight_layout(pad=0.01, w_pad=0.01, h_pad=0.01)
	plt.savefig(outputname)
	plt.close()




def plotFirstSubTrajMultiIndiv(x, y, size, outputname, **kwargs):
	#f = plt.figure(figsize=(3,3))
	#colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('Greys_r'))
	colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('Set1'))
	#colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('tab10'))
	nbSubX = kwargs.get('nbSubX') or 6
	nbSubY = kwargs.get('nbSubY') or 5
	binSize = int(x.shape[0] / (nbSubX * nbSubY))
	index = 1

	fig, ax  = plt.subplots(figsize=(8,8))

	ax.set_xlim(0, size)
	ax.set_ylim(0, size)
	ax.set_xticks([])
	ax.set_yticks([])

	for k in range(x.shape[1]):
		if kwargs.get('colors') is not None and k < len(kwargs['colors']):
			continue
		else:
			colorVal = colorMap.to_rgba(k)
		X = x[binSize*index:binSize*(index+1), k]
		Y = y[binSize*index:binSize*(index+1), k]
		X2 = [1. - X[0]]
		Y2 = [Y[0]]
		for l in range(1, len(X)):
			if abs(X[l] - X[l-1]) > 0.3 or abs(Y[l] - Y[l-1]) > 0.3:
				X2.append(np.nan)
				Y2.append(np.nan)
			else:
				X2.append(1. - X[l])
				Y2.append(Y[l])
		ax.plot(X2, Y2, color = colorVal)

	if kwargs.get('colors') is not None:
		for k in range(min(x.shape[1], len(kwargs['colors']))):
			colorVal = kwargs['colors'][k]
			X = x[binSize*index:binSize*(index+1), k]
			Y = y[binSize*index:binSize*(index+1), k]
			X2 = [1. - X[0]]
			Y2 = [Y[0]]
			for l in range(1, len(X)):
				if abs(X[l] - X[l-1]) > 0.3 or abs(Y[l] - Y[l-1]) > 0.3:
					X2.append(np.nan)
					Y2.append(np.nan)
				else:
					X2.append(1. - X[l])
					Y2.append(Y[l])
			ax.plot(X2, Y2, color = colorVal)

	plt.tight_layout(pad=0.01, w_pad=0.01, h_pad=0.01)
	plt.savefig(outputname)
	plt.close()



def plotFirstSubTrajOneIndiv(x, y, indexIndiv, size, outputname, **kwargs):
	#f = plt.figure(figsize=(3,3))
	#colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('Greys_r'))
	colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('Set1'))
	#colorMap = cm.ScalarMappable(norm=matplotlib.colors.Normalize(vmin=0, vmax=x.shape[1] + 2), cmap=plt.get_cmap('tab10'))
	nbSubX = kwargs.get('nbSubX') or 6
	nbSubY = kwargs.get('nbSubY') or 5
	binSize = int(x.shape[0] / (nbSubX * nbSubY))
	index = 1

	fig, ax  = plt.subplots(figsize=(8,8))

	ax.set_xlim(0, size)
	ax.set_ylim(0, size)
	ax.set_xticks([])
	ax.set_yticks([])

	if x.shape[1] > indexIndiv:
		colorVal = colorMap.to_rgba(indexIndiv)
		X = x[binSize*index:binSize*(index+1), indexIndiv]
		Y = y[binSize*index:binSize*(index+1), indexIndiv]
		X2 = [1. - X[0]]
		Y2 = [Y[0]]
		for l in range(1, len(X)):
			if abs(X[l] - X[l-1]) > 0.3 or abs(Y[l] - Y[l-1]) > 0.3:
				X2.append(np.nan)
				Y2.append(np.nan)
			else:
				X2.append(1. - X[l])
				Y2.append(Y[l])
		ax.plot(X2, Y2, color = colorVal)


	plt.tight_layout(pad=0.01, w_pad=0.01, h_pad=0.01)
	plt.savefig(outputname)
	plt.close()




def linSpeed(positions, fps):
	nbAgents = int(positions.shape[1] / 2)
	speeds = np.empty((positions.shape[0], nbAgents))
	speeds[:2,:]=np.nan
	posX = positions[:, 0 : 2 * nbAgents : 2]
	posY = positions[:, 1 : 2 * nbAgents + 1 : 2]
	for i in range (1, posY.shape[0] - 1):
		for j in range(nbAgents):
			speeds[i,j] = sqrt((posY[i+1][j]-posY[i-1][j]) ** 2 + (posY[i+1][j]-posY[i-1][j]) ** 2) / (2./fps)
	return speeds



def analyse_traces(dataDir, plotsDir, arenaSizeX, arenaSizeY, nAnimals, nRobots, outputFilenameSuffix, fps = 3, nColumn = 3, cmap = 'Blues', drawCbar = True, **kwargs):
	nAgents = nAnimals + nRobots

	files = []
	for f in os.listdir(dataDir):
		if f.endswith(".txt"):
			files.append(f)

	if len(files) == 0:
		ii = 0
		time = np.empty((0))
		data = np.empty((0,0))
		lspeed = np.empty((0,0))

		outputname = os.path.join(plotsDir, "Traces" + str(ii) + "-" + outputFilenameSuffix)
		cbarFile = os.path.join(plotsDir, "TracesCBAR" + str(ii) + "-" + outputFilenameSuffix)
		plotspeedtraj(data, lspeed, arenaSizeX, nAgents, outputname, cmap, drawCbar=drawCbar, cbarFile=cbarFile)
		#plotgrid(grid/len(files), resolution, outputnameBatch, nmanip = len(files), title=title, vmin=0.0, vmax=4.0, cmap = cmap)

		plotFirstSubTrajMultiIndiv(data[:,::2], data[:,1::2], arenaSizeX, os.path.join(plotsDir, "FstSplittedTraces" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)
		plotFirstSubTrajMultiIndiv(data[:,::2], data[:,1::2], arenaSizeX, os.path.join(plotsDir, "SplittedTraces" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)
		plotFirstSubTrajMultiIndiv(data[:,::2], data[:,1::2], arenaSizeX, os.path.join(plotsDir, "FstSplittedIndiv0Trace" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)
		plotFirstSubTrajMultiIndiv(data[:,::2], data[:,1::2], arenaSizeX, os.path.join(plotsDir, "FstSplittedIndiv" + str(nAgents-1) + "Trace" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)

	else:
		for ii in range(0, len(files)):
		#for ii in range(0, 1):
			filename, fileext = os.path.splitext(files[ii])

			with open(os.path.join(dataDir, files[ii]), "r") as source:
				data_full = np.loadtxt(source, skiprows = 1)#[::5][:2000]
			time = data_full[:,0]
			data = data_full[:,np.sort(np.hstack([np.arange(nAgents)* nColumn + 1, np.arange(nAgents) * nColumn + 2]))]
			data = 1. - data
			#data = data[:180,:]
			lspeed = linSpeed(data, fps)

			outputname = os.path.join(plotsDir, "Traces" + str(ii) + "-" + outputFilenameSuffix)
			cbarFile = os.path.join(plotsDir, "TracesCBAR" + str(ii) + "-" + outputFilenameSuffix)
			plotspeedtraj(data, lspeed, arenaSizeX, nAgents, outputname, cmap, drawCbar=drawCbar, cbarFile=cbarFile)
			#plotgrid(grid/len(files), resolution, outputnameBatch, nmanip = len(files), title=title, vmin=0.0, vmax=4.0, cmap = cmap)

			plotSubTrajInGridMultiIndiv(data[:,::2], data[:,1::2], arenaSizeX, os.path.join(plotsDir, "SplittedTraces" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)
			plotFirstSubTrajMultiIndiv(data[:,::2], data[:,1::2], arenaSizeX, os.path.join(plotsDir, "FstSplittedTraces" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)
			plotFirstSubTrajOneIndiv(data[:,::2], data[:,1::2], 0, arenaSizeX, os.path.join(plotsDir, "FstSplittedIndiv0Trace" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)
			plotFirstSubTrajOneIndiv(data[:,::2], data[:,1::2], nAgents - 1, arenaSizeX, os.path.join(plotsDir, "FstSplittedIndiv" + str(nAgents-1) + "Trace" + str(ii) + "-" + outputFilenameSuffix), colors = None, nbSubX = 3, nbSubY = 3)



if __name__ == "__main__":
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option("-d", "--inDirectory", dest = "inDirectory", help = "Directory of the data")
	parser.add_option("-o", "--outDirectory", dest = "outDirectory", help = "Directory of the plots")
	parser.add_option("-x", "--sizeX", type = "float", dest = "sizeX", help = "Size of the square arena", default=1)
	parser.add_option("-y", "--sizeY", type = "float", dest = "sizeY", help = "Size of the square arena", default=1)
	parser.add_option("-a", "--nAnimals", type = "int", dest = "nAnimals", help = "Number of Animals")
	parser.add_option("-r", "--nRobots", type = "int", dest = "nRobots", help = "Number of robots", default=0)
	parser.add_option("-S", "--outputFilenameSuffix", dest = "outputFilenameSuffix", help = "Output Filename Suffix")
	parser.add_option("-C", "--nColumn", type="int", dest = "nColumn", help = "nColumn", default=3)
	parser.add_option("-M", "--cmap", dest = "cmap", help = "cmap", default='Blues')
	parser.add_option("-f", "--fps", dest = "fps", help = "fps", default=15)
	parser.add_option("-B", "--drawCbar", action="store_true", dest="drawCbar", default=False, help="drawCbar")

	(options, args) = parser.parse_args()

	analyse_traces(options.inDirectory, options.outDirectory, float(options.sizeX), float(options.sizeY), int(options.nAnimals), int(options.nRobots), options.outputFilenameSuffix, int(options.fps), int(options.nColumn), cmap = options.cmap, drawCbar = bool(options.drawCbar))


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
