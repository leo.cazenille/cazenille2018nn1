#!/usr/bin/env python

"""
Compute fitness
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

#from FishModel import model
import numpy as np
from features import *
import sys
import multiprocessing
import scipy.stats.mstats
import fishSim
import copy
import math


########### FITNESS COMPUTATION ########### {{{1

def hellingerDistance(data1, data2):
	assert len(data1) == len(data2)
	return np.sqrt(np.sum(np.array([ (np.sqrt(data1[x]) - np.sqrt(data2[x]))**2. for x in range(len(data1))]))) / np.sqrt(2.)


class FeaturesSet(object):
	def __init__(self, xList, yList, simParams, dc = 0.30):
		self.xList = xList
		self.yList = yList
		self.simParams = simParams
		self.dc = dc
		self.orientationList = computeOrientation(self.xList, self.yList)
		self.presenceZonesList = presenceInZones(self.xList, self.yList, self.simParams.arenaMap)
		#self.sgs = findSGs(self.xList, self.yList, self.dc)

	def getCoordsShape(self):
		if len(self.xList):
			return self.xList[0].shape
		else:
			return None

	def getZonesOccupation(self):
		if hasattr(self, 'zonesOccupation'):
			return self.zonesOccupation
		res = zonesOccupation(self.xList, self.yList, self.presenceZonesList, self.simParams.nbZones)
		self.zonesOccupation = res
		return res

	def getLinearSpeedInZones(self):
		if hasattr(self, 'linearSpeedsInZones'):
			return self.linearSpeedsInZones
		res = linearSpeedsInZones(self.xList, self.yList, self.presenceZonesList, self.simParams.nbZones, 1.0 / self.simParams.dt, self.simParams.linearSpeedsInSGOfSizeNbBin, self.simParams.linearSpeedsInSGOfSizeDomain)
		#print("DEBUG: getLinearSpeedInZones: ", res)
		self.linearSpeedsInZones = res
		return res

	def getAngularSpeedInZones(self):
		if hasattr(self, 'angularSpeedsInZones'):
			return self.angularSpeedsInZones
		res = angularSpeedsInZones(self.xList, self.yList, self.orientationList, self.presenceZonesList, self.simParams.nbZones, 1.0 / self.simParams.dt, self.simParams.angularSpeedsInZonesNbBin, self.simParams.angularSpeedsInZonesDomain)
		self.angularSpeedsInZones = res
		return res

	def getInterindivDistInZones(self):
		if hasattr(self, 'interindivDistInZones'):
			return self.interindivDistInZones
		res = interindivDistInZones(self.xList, self.yList, self.presenceZonesList, self.simParams.nbZones, self.simParams.interindivDistInZonesNbBin, self.simParams.interindivDistInZonesDomain)
		self.interindivDistInZones = res
		return res

	def getPolarizationInZones(self):
		if hasattr(self, 'polarizationInZones'):
			return self.polarizationInZones
		res = polarizationInZones(self.xList, self.yList, self.orientationList, self.presenceZonesList, self.simParams.nbZones, self.simParams.polarizationInZonesNbBin, self.simParams.polarizationInZonesDomain)
		self.polarizationInZones = res
		return res

	def getZonesTransitions(self):
		if hasattr(self, 'zonesTransitions'):
			return self.zonesTransitions
		res = zonesTransitions(self.xList, self.yList, self.presenceZonesList, self.simParams.nbZones)
		self.zonesTransitions = res
		return res

	def getDistToWallsByZones(self):
		if hasattr(self, 'distToWallsByZones'):
			return self.distToWallsByZones
		res = distanceToWalls(self.xList, self.yList, self.presenceZonesList, self.simParams.nbZones, self.simParams.distToWallsByZonesNbBin, self.simParams.distToWallsByZonesDomain)
		self.distToWallsByZones = res
		return res


#	def getMeanFractionInSGOfSize(self):
#		if hasattr(self, 'meanFractionInSGOfSize'):
#			return self.meanFractionInSGOfSize
#		res = meanFractionInSGOfSize(self.xList, self.yList, self.sgs)
#		self.meanFractionInSGOfSize = res
#		return res
#
#	def getLinearSpeedInSGOfSize(self):
#		if hasattr(self, 'linearSpeedsInSGOfSize'):
#			return self.linearSpeedsInSGOfSize
#		res = linearSpeedsInSGOfSize(self.xList, self.yList, self.sgs, 1.0 / self.simParams.dt, self.simParams.linearSpeedsInSGOfSizeNbBin, self.simParams.linearSpeedsInSGOfSizeDomain)
#		self.linearSpeedsInSGOfSize = res
#		return res
#
#	def getAngularSpeedInSGOfSize(self):
#		if hasattr(self, 'angularSpeedsInSGOfSize'):
#			return self.angularSpeedsInSGOfSize
#		res = angularSpeedsInSGOfSize(self.xList, self.yList, self.orientationList, self.sgs, 1.0 / self.simParams.dt, self.simParams.angularSpeedsInSGOfSizeNbBin, self.simParams.angularSpeedsInSGOfSizeDomain)
#		self.angularSpeedsInSGOfSize = res
#		return res

	def getInterindivDistInSGs(self):
		if hasattr(self, 'interindivDistInSGs'):
			return self.interindivDistInSGs
		res = interindivDistInSGs(self.xList, self.yList, self.sgs, self.simParams.interindivDistInSGsNbBin, self.simParams.interindivDistInSGsDomain)
		self.interindivDistInSGs = res
		return res

#	def getPolarizationInSGOfSize(self):
#		if hasattr(self, 'polarizationInSGOfSize'):
#			return self.polarizationInSGOfSize
#		res = polarizationInSGOfSize(self.xList, self.yList, self.orientationList, self.sgs, self.simParams.polarizationInSGOfSizeNbBin, self.simParams.polarizationInSGOfSizeDomain)
#		self.polarizationInSGOfSize = res
#		return res


	def getProbabilityOfPresence(self):
		if hasattr(self, 'probabilityOfPresence'):
			return self.probabilityOfPresence
		res = probabilityOfPresence(self.xList, self.yList, self.presenceZonesList, self.simParams.nbZones, self.simParams.gridResolution)
		self.probabilityOfPresence = res
		return res



class FitnessCalibration(object):
	def __init__(self, referencePath, nbRuns = 10, mapFilename="FishModel/arenas/arena2roomsLargeSmall.png", nbSteps=1800, dt=1./15., nbFishes=5, nbZones=10, seed=42, skip=0):
		self.referencePath = referencePath
		self.nbRuns = nbRuns
		self.mapFilename = mapFilename
		self.nbSteps = nbSteps
		self.dt = dt
		self.nbFishes = nbFishes
		self.nbZones = nbZones
		self.seed = seed + 1
		self.arenaMap = loadArenaMap(mapFilename)
		self.skip = skip
		self.initReference()

	def initReference(self):
		xList = []
		yList = []

		# Determine list of data files from given path
		files = []
		if os.path.isdir(self.referencePath):
			for f in os.listdir(self.referencePath):
				if f.endswith(".txt"):
					files.append(os.path.join(self.referencePath, f))
		else:
			files.append(self.referencePath)

		# Load xList and yList from data files
		for f in files:
			with open(f, "r") as source:
				data_full = np.loadtxt(source, skiprows = 1)
			time = data_full[:, 0]
			data = data_full[:, np.sort(np.hstack([np.arange(self.nbFishes) * 2 + 1, np.arange(self.nbFishes) * 2 + 2]))]
			if self.skip == 0:
				xList.append(data[:, 0 : 2 * self.nbFishes : 2])
				yList.append(data[:, 1 : 2 * self.nbFishes + 1 : 2])
			else:
				xList.append(data[::self.skip, 0 : 2 * self.nbFishes : 2])
				yList.append(data[::self.skip, 1 : 2 * self.nbFishes + 1 : 2])

			#xList.append(data[::15, 0 : 2 * self.nbFishes : 2])
			#yList.append(data[::15, 1 : 2 * self.nbFishes + 1 : 2])
			## XXX DEBUG
			#xList.append(data[:150:15, 0 : 2 * self.nbFishes : 2])
			#yList.append(data[:150:15, 1 : 2 * self.nbFishes + 1 : 2])

		self.referenceFSet = FeaturesSet(xList, yList, self, 0.30)

		# Set domains and nbBins
		self.linearSpeedsInZonesDomain = (0.0, 0.40) # (0.0, 0.20)
		self.linearSpeedsInZonesNbBin = 20 # 10 #30
		self.linearSpeedsInSGOfSizeDomain = (0.0, 0.40) # (0.0, 0.20)
		self.linearSpeedsInSGOfSizeNbBin = 20 # 10 # 30
		self.angularSpeedsInZonesDomain = (-1. * np.pi, 1. * np.pi)  #(-30. / 180. * np.pi, 30. / 180. * np.pi) 
		self.angularSpeedsInZonesNbBin = 18 # 10
		self.angularSpeedsInSGOfSizeDomain = (-1. * np.pi, 1. * np.pi)
		self.angularSpeedsInSGOfSizeNbBin = 18 # 10
		self.interindivDistInZonesDomain = (0.0, 0.50) #(0.0, 0.25) #(0.0, 0.40)
		self.interindivDistInZonesNbBin = 25 # 10 #20
		self.interindivDistInSGsDomain = (0.0, 0.50) #(0.0, 0.25) #(0.0, 0.40)
		self.interindivDistInSGsNbBin = 25 # 10 #20

		self.polarizationInZonesDomain = (0.0, 1.0)
		self.polarizationInZonesNbBin = 20 # 10
		self.polarizationInSGOfSizeDomain = (0.0, 1.0)
		self.polarizationInSGOfSizeNbBin = 20 # 10

		self.distToWallsByZonesDomain = (0.0, 0.60) #(0.0, 0.20) #(0.0, 0.40)
		self.distToWallsByZonesNbBin = 30 # 10 #20

		self.gridResolution = (25, 25)



	def computeScore(self, fSet, verbose=False, scoreType="v4", totalOutOfBounds = 0):
		if scoreType == "v4":
			return self._computeScoreV4(fSet, verbose)
		elif scoreType == "v5":
			return self._computeScoreV5(fSet, verbose, totalOutOfBounds)

		return meanScore, allScores


	def _computeScoreV5(self, fSet, verbose=False, totalOutOfBounds = 0):
		refLinearSpeedsInZones = self.referenceFSet.getLinearSpeedInZones()[1:]
		linearSpeedsInZones = fSet.getLinearSpeedInZones()[1:]
		scoresLinearSpeedsInZones = []
		for i in range(len(refLinearSpeedsInZones)):
			scoresLinearSpeedsInZones.append((1. - hellingerDistance(refLinearSpeedsInZones[i], linearSpeedsInZones[i])) ** 2.)
		scoreLinearSpeedsInZones = np.mean(scoresLinearSpeedsInZones)
		if verbose:
			print("linearSpeedsInZones: ", refLinearSpeedsInZones, linearSpeedsInZones)

		#refAngularSpeedsInZones = self.referenceFSet.getAngularSpeedInZones()[1:]
		#angularSpeedsInZones = fSet.getAngularSpeedInZones()[1:]
		#scoresAngularSpeedsInZones = []
		#for i in range(len(refAngularSpeedsInZones)):
		#	scoresAngularSpeedsInZones.append((1. - hellingerDistance(refAngularSpeedsInZones[i], angularSpeedsInZones[i])) ** 2.)
		#scoreAngularSpeedsInZones = np.mean(scoresAngularSpeedsInZones)
		#if verbose:
		#	print("angularSpeedsInZones: ", refAngularSpeedsInZones, angularSpeedsInZones)

		refPolarizationInZones = self.referenceFSet.getPolarizationInZones()#[1:]
		polarizationInZones = fSet.getPolarizationInZones()#[1:]
		scoresPolarizationInZones = []
		for i in range(len(refPolarizationInZones)):
			scoresPolarizationInZones.append((1. - hellingerDistance(refPolarizationInZones[i], polarizationInZones[i])) ** 2.)
		scorePolarizationInZones = np.mean(scoresPolarizationInZones)
		if verbose:
			print("polarizationInZones: ", refPolarizationInZones, polarizationInZones)

		refInterindivDistInZones = self.referenceFSet.getInterindivDistInZones()[1:]
		interindivDistInZones = fSet.getInterindivDistInZones()[1:]
		scoresInterindivDistInZones = []
		for i in range(len(refInterindivDistInZones)):
			scoresInterindivDistInZones.append((1. - hellingerDistance(refInterindivDistInZones[i], interindivDistInZones[i])) ** 2.)
		scoreInterindivDistInZones = np.mean(scoresInterindivDistInZones)
		if verbose:
			print("interindivDistInZones: ", refInterindivDistInZones, interindivDistInZones)

		refProbabilityOfPresence = self.referenceFSet.getProbabilityOfPresence()[1:]
		probabilityOfPresence = fSet.getProbabilityOfPresence()[1:]
		scoresProbabilityOfPresence = []
		for i in range(len(refProbabilityOfPresence)):
			scoresProbabilityOfPresence.append((1. - hellingerDistance(refProbabilityOfPresence[i], probabilityOfPresence[i])) ** 2.)
		scoreProbabilityOfPresence = np.mean(scoresProbabilityOfPresence)
		if verbose:
			print("probabilityOfPresence: ", refProbabilityOfPresence, probabilityOfPresence)

		coordsShape = fSet.getCoordsShape()
		outOfBoundsScore = pow(1.0 - float(totalOutOfBounds) / float(coordsShape[0] * coordsShape[1]), 30)


		# TODO traj metric

		allScores = [scoreInterindivDistInZones, scoreLinearSpeedsInZones, scorePolarizationInZones, scoreProbabilityOfPresence]
		meanScore = scipy.stats.mstats.gmean(allScores)
		allScores = [scoreInterindivDistInZones, scoreLinearSpeedsInZones, scorePolarizationInZones, scoreProbabilityOfPresence, outOfBoundsScore]
		return meanScore, allScores



	def _computeScoreV4(self, fSet, verbose=False):
		#refZonesOccupation = self.referenceFSet.getZonesOccupation()[1:]
		#zonesOccupation = fSet.getZonesOccupation()[1:]
		#scoreZonesOccupation = (1. - hellingerDistance(refZonesOccupation, zonesOccupation)) ** 2.
		#if verbose:
		#	print("occupations: ", refZonesOccupation, zonesOccupation)

		refLinearSpeedsInZones = self.referenceFSet.getLinearSpeedInZones()[1:]
		linearSpeedsInZones = fSet.getLinearSpeedInZones()[1:]
		scoresLinearSpeedsInZones = []
		for i in range(len(refLinearSpeedsInZones)):
			scoresLinearSpeedsInZones.append((1. - hellingerDistance(refLinearSpeedsInZones[i], linearSpeedsInZones[i])) ** 2.)
		scoreLinearSpeedsInZones = np.mean(scoresLinearSpeedsInZones)
		if verbose:
			print("linearSpeedsInZones: ", refLinearSpeedsInZones, linearSpeedsInZones)

		refAngularSpeedsInZones = self.referenceFSet.getAngularSpeedInZones()[1:]
		angularSpeedsInZones = fSet.getAngularSpeedInZones()[1:]
		scoresAngularSpeedsInZones = []
		for i in range(len(refAngularSpeedsInZones)):
			scoresAngularSpeedsInZones.append((1. - hellingerDistance(refAngularSpeedsInZones[i], angularSpeedsInZones[i])) ** 2.)
		scoreAngularSpeedsInZones = np.mean(scoresAngularSpeedsInZones)
		if verbose:
			print("angularSpeedsInZones: ", refAngularSpeedsInZones, angularSpeedsInZones)

		refInterindivDistInZones = self.referenceFSet.getInterindivDistInZones()[1:]
		interindivDistInZones = fSet.getInterindivDistInZones()[1:]
		scoresInterindivDistInZones = []
		for i in range(len(refInterindivDistInZones)):
			scoresInterindivDistInZones.append((1. - hellingerDistance(refInterindivDistInZones[i], interindivDistInZones[i])) ** 2.)
		scoreInterindivDistInZones = np.mean(scoresInterindivDistInZones)
		if verbose:
			print("interindivDistInZones: ", refInterindivDistInZones, interindivDistInZones)

		refPolarizationInZones = self.referenceFSet.getPolarizationInZones()#[1:]
		polarizationInZones = fSet.getPolarizationInZones()#[1:]
		scoresPolarizationInZones = []
		for i in range(len(refPolarizationInZones)):
			scoresPolarizationInZones.append((1. - hellingerDistance(refPolarizationInZones[i], polarizationInZones[i])) ** 2.)
		scorePolarizationInZones = np.mean(scoresPolarizationInZones)
		if verbose:
			print("polarizationInZones: ", refPolarizationInZones, polarizationInZones)

		#refZonesTransitions = np.hstack(self.referenceFSet.getZonesTransitions()[1:,1:])
		#zonesTransitions = np.hstack(fSet.getZonesTransitions()[1:,1:])
		#scoreZonesTransitions = (1. - hellingerDistance(refZonesTransitions, zonesTransitions)) ** 2.
		#if verbose:
		#	print("transitions: ", refZonesTransitions, zonesTransitions)

		refDistToWallsByZones = self.referenceFSet.getDistToWallsByZones()[1:]
		distToWallsByZones = fSet.getDistToWallsByZones()[1:]
		scoresDistToWallsByZones = []
		for i in range(len(refDistToWallsByZones)):
			scoresDistToWallsByZones.append((1. - hellingerDistance(refDistToWallsByZones[i], distToWallsByZones[i])) ** 2.)
		scoreDistToWallsByZones = np.mean(scoresDistToWallsByZones)
		if verbose:
			print("distToWallsByZones: ", refDistToWallsByZones, distToWallsByZones)


#		refMeanFractionInSGOfSize = self.referenceFSet.getMeanFractionInSGOfSize()
#		meanFractionInSGOfSize = fSet.getMeanFractionInSGOfSize()
#		scoreMeanFractionInSGOfSize = (1. - hellingerDistance(refMeanFractionInSGOfSize, meanFractionInSGOfSize)) ** 2.
#
#		refLinearSpeedsInSGOfSize = self.referenceFSet.getLinearSpeedInSGOfSize()
#		linearSpeedsInSGOfSize = fSet.getLinearSpeedInSGOfSize()
#		scoresLinearSpeedsInSGOfSize = []
#		for i in range(len(refLinearSpeedsInSGOfSize)):
#			scoresLinearSpeedsInSGOfSize.append((1. - hellingerDistance(refLinearSpeedsInSGOfSize[i], linearSpeedsInSGOfSize[i])) ** 2.)
#		scoreLinearSpeedsInSGOfSize = np.mean(scoresLinearSpeedsInSGOfSize)
#
#		refAngularSpeedsInSGOfSize = self.referenceFSet.getAngularSpeedInSGOfSize()
#		angularSpeedsInSGOfSize = fSet.getAngularSpeedInSGOfSize()
#		scoresAngularSpeedsInSGOfSize = []
#		for i in range(len(refAngularSpeedsInSGOfSize)):
#			scoresAngularSpeedsInSGOfSize.append((1. - hellingerDistance(refAngularSpeedsInSGOfSize[i], angularSpeedsInSGOfSize[i])) ** 2.)
#		scoreAngularSpeedsInSGOfSize = np.mean(scoresAngularSpeedsInSGOfSize)

#		refInterindivDistInSGs = self.referenceFSet.getInterindivDistInSGs()
#		interindivDistInSGs = fSet.getInterindivDistInSGs()
#		scoresInterindivDistInSGs = []
#		for i in range(len(refInterindivDistInSGs)):
#			scoresInterindivDistInSGs.append((1. - hellingerDistance(refInterindivDistInSGs[i], interindivDistInSGs[i])) ** 2.)
#		scoreInterindivDistInSGs = np.mean(scoresInterindivDistInSGs)

#		refPolarizationInSGOfSize = self.referenceFSet.getPolarizationInSGOfSize()
#		polarizationInSGOfSize = fSet.getPolarizationInSGOfSize()
#		scoresPolarizationInSGOfSize = []
#		for i in range(len(refPolarizationInSGOfSize)):
#			scoresPolarizationInSGOfSize.append((1. - hellingerDistance(refPolarizationInSGOfSize[i], polarizationInSGOfSize[i])) ** 2.)
#		scorePolarizationInSGOfSize = np.mean(scoresPolarizationInSGOfSize)


		refProbabilityOfPresence = self.referenceFSet.getProbabilityOfPresence()[1:]
		probabilityOfPresence = fSet.getProbabilityOfPresence()[1:]
		scoresProbabilityOfPresence = []
		for i in range(len(refProbabilityOfPresence)):
			scoresProbabilityOfPresence.append((1. - hellingerDistance(refProbabilityOfPresence[i], probabilityOfPresence[i])) ** 2.)
		scoreProbabilityOfPresence = np.mean(scoresProbabilityOfPresence)
		if verbose:
			print("probabilityOfPresence: ", refProbabilityOfPresence, probabilityOfPresence)



		#allScores = [scoreInterindivDistInZones, scoreLinearSpeedsInZones, scoreAngularSpeedsInZones, scorePolarizationInZones, scoreDistToWallsByZones]
		allScores = [scoreInterindivDistInZones, scoreLinearSpeedsInZones, scoreAngularSpeedsInZones, scorePolarizationInZones, scoreDistToWallsByZones, scoreProbabilityOfPresence]
		meanScore = scipy.stats.mstats.gmean(allScores)

		#if verbose:
		#	print("SCORE=", meanScore)
		#	print(" zonesOccupation=", scoreZonesOccupation, " linearSpeedsInZones=", scoreLinearSpeedsInZones, scoresLinearSpeedsInZones, " angularSpeedsInZones=", scoreAngularSpeedsInZones, scoresAngularSpeedsInZones, " interindivDistInZones=", scoreInterindivDistInZones, scoresInterindivDistInZones, " polarizationInZones=", scorePolarizationInZones, scoresPolarizationInZones, " zonesTransitions=", scoreZonesTransitions, " distToWallsByZones=", scoreDistToWallsByZones, scoresDistToWallsByZones)
#		#	#print(" interindivDistInSGs=", scoresInterindivDistInSGs, " linearSpeedsInSGOfSize=", scoresLinearSpeedsInSGOfSize, " angularSpeedsInSGOfSize=", scoresAngularSpeedsInSGOfSize, " meanFractionInSGOfSize=", scoreMeanFractionInSGOfSize, " polarizationInSGOfSize=", scoresPolarizationInSGOfSize)

		return meanScore, allScores




	# Run model
	def genTrajectories(self, individual, nnBase, annStepsPerSimStep, dataSets, nbSimulatedAgents = 1, addOracleInputs = False):
		xList = []
		yList = []
		thetaList = []
		timeList = []
		totalOutOfBounds = 0
		for data in dataSets:
			sim = fishSim.FishSim(data.nbAgents)

			nns = []
			#if simulateOnlyLastAgent:
			#	simulatedAgentsId = [data.nbAgents - 1]
			#else:
			#	simulatedAgentsId = range(data.nbAgents)
			simulatedAgentsId = range(nbSimulatedAgents)
			for a in simulatedAgentsId:
				nn = nnBase.clone()
				nn.reinit()
				nn.setWeights(individual)
				nns.append(nn)

			nextPosX, nextPosY, nextPosTheta, nextTime = data.getNextPos()
			xs = [nextPosX]
			ys = [nextPosY]
			thetas = [nextPosTheta]
			times = [nextTime]
			sim.step(nextPosX, nextPosY, nextPosTheta, nextTime)
			for i in range(self.nbSteps):
				nextPosX, nextPosY, nextPosTheta, nextTime = data.getNextPos()

				#focalAgentId = data.nbAgents - 1
				for a in range(len(simulatedAgentsId)):
					focalAgentId, nn = simulatedAgentsId[a], nns[a]
					inputs = sim.inputs[focalAgentId]
					if addOracleInputs:
						deltaAngle = ((nextPosTheta[focalAgentId] - sim.agents[focalAgentId]['theta']) / np.pi + 1.0) / 2.0
						inputs = np.array(list(inputs) + [deltaAngle])
					nn.setInputs(inputs)
					nn.step(annStepsPerSimStep)
					deltaTime = nextTime - sim.time
					deltaAngle = (nn.outputs()[0] + 1.0) / 2.0 * (sim.maxDeltaAngle - sim.minDeltaAngle) + sim.minDeltaAngle 
					deltaLinSpeed = (nn.outputs()[1] + 1.0) / 2.0 * sim.maxLinearSpeed
					nextFocalPosTheta = sim.agents[focalAgentId]['theta'] + deltaAngle
					nextFocalLinSpeed = sim.instLinearSpeed[focalAgentId] + deltaLinSpeed
					nextFocalPosX = sim.agents[focalAgentId]['x'] + nextFocalLinSpeed * deltaTime * math.cos(nextFocalPosTheta)
					nextFocalPosY = sim.agents[focalAgentId]['y'] + nextFocalLinSpeed * deltaTime * math.sin(nextFocalPosTheta)
					nextPosX[focalAgentId], nextPosY[focalAgentId], nextPosTheta[focalAgentId], outOfBounds = sim._outOfBoundsCorrection(nextFocalPosX, nextFocalPosY, nextFocalPosTheta)
					if outOfBounds:
						totalOutOfBounds += 1

				sim.step(nextPosX, nextPosY, nextPosTheta, nextTime)
				#print(i, deltaAngle, deltaLinSpeed, nextPosX, nextPosY, nextPosTheta, nextTime)

				xs.append(nextPosX)
				ys.append(nextPosY)
				thetas.append(nextPosTheta)
				times.append(nextTime)
			xList.append(np.array(xs))
			yList.append(np.array(ys))
			thetaList.append(np.array(thetas))
			timeList.append(np.array(times))

			# Destroy nns
			del nns[:]
		return xList, yList, thetaList, timeList, totalOutOfBounds



	def fitness(self, individual, nnBase, annStepsPerSimStep, inputDir, nbSimulatedAgents = 1, outputTrajectoriesDir = None, addOracleInputs = False, fitnessType="v4"):
		inputFilenames = []
		for f in os.listdir(inputDir):
			if f.endswith(".txt"):
				inputFilenames.append(os.path.join(inputDir, f))

		dataSets = []
		if outputTrajectoriesDir != None:
			for inputFilename in inputFilenames:
				d = fishSim.DataSet()
				d.fromFile(inputFilename)
				dataSets.append(d)
		else:
			# RMQ: Only use the first file, to speed up evolution !
			d = fishSim.DataSet()
			d.fromFile(inputFilenames[0])
			dataSets.append(d)
		xList, yList, thetaList, timeList, totalOutOfBounds = self.genTrajectories(individual, nnBase, annStepsPerSimStep, dataSets, nbSimulatedAgents, addOracleInputs)

		if outputTrajectoriesDir != None:
			filenameList = [os.path.join(outputTrajectoriesDir, "%i.txt") % o for o in range(len(xList))]
			for xs, ys, thetas, times, filename in zip(xList, yList, thetaList, timeList, filenameList):
				data = np.empty((xs.shape[0], 2 * xs.shape[1] + 1))
				data[:, 0] = times[:]
				data[:, 1::2] = xs[:, :]
				data[:, 2::2] = ys[:, :]
				#for j in range(xs.shape[1])
				#	data[:, 1+j*2] = xs[:, j]
				#	data[:, 2+j*2] = ys[:, j]
				np.savetxt(filename, data, fmt='%1.3f')

		#for data in datas:
		#	d = data[:, np.sort(np.hstack([np.arange(self.nbFishes) * 3 + 1, np.arange(self.nbFishes) * 3 + 2]))]
		#	xList.append(d[:, 0 : 2 * self.nbFishes : 2])
		#	yList.append(d[:, 1 : 2 * self.nbFishes + 1 : 2])

		individualFSet = FeaturesSet(xList, yList, self, 0.30)
		individualScore, allScores = self.computeScore(individualFSet, False, fitnessType, totalOutOfBounds)
		#individualScore, allScores = self.computeScore(individualFSet, True)

		return individualScore, allScores



########### MAIN ########### {{{1
if __name__ == "__main__":
	np.seterr(divide='ignore', invalid='ignore')

	from optparse import OptionParser
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-i", "--inputDir", dest = "inputDir", default = "",
			help = "Path of input data files")
	parser.add_option("-a", "--arenaFilename", dest = "arenaFilename", default = "FishModel/arenas/SetupLargeModelzoneBlack70x70v2.png",
			help = "Filename of arena map file")
	parser.add_option("-z", "--nbZones", dest = "nbZones", default = 5,
			help = "Number of zones")

	parser.add_option("-s", "--seed", dest = "seed", default = 42,
			help = "Random number generator seed")
	parser.add_option("-n", "--nbRuns", dest = "nbRuns", default = 10,
			help = "Number of runs per eval")

	parser.add_option("-k", "--skip", dest = "skip", default = 0,
			help = "")

	parser.add_option("-S", "--nbSteps", dest = "nbSteps", default = 1800,
			help = "Number of steps per eval")

	parser.add_option("-o", "--outputDir", dest = "outputDir", default = None,
			help = "Path of output data files")
#	parser.add_option("-s", "--startingFrame", dest = "startingFrame", default = "40000",
#			help = "Starting frame")
#	parser.add_option("-e", "--endingFrame", dest = "endingFrame", default = "59999",
#			help = "Ending Frame")
#	parser.add_option("-d", "--dc", dest = "dc", default = "0.20",
#			help = "dc")
#	parser.add_option("-n", "--nbAgents", dest = "nbAgents", default = "5",
#			help = "Number of agents")
#	parser.add_option("-D", "--findGroupID", action="store_true", dest = "findGroupID", default = False,
#			help = "Find Group ID instead of group Type")

	parser.add_option("-T", "--annType", dest = "annType", default = "ESN",
			help = "Type of ANN (MLP, ESN, SESN)")

	parser.add_option("--annStepsPerSimStep", dest = "annStepsPerSimStep", default = 1,
			help = "Number of ANN steps per simulation step")

	(options, args) = parser.parse_args()

	np.random.seed(int(options.seed))
	annStepsPerSimStep = int(options.annStepsPerSimStep)

	f = FitnessCalibration(options.inputDir, mapFilename = options.arenaFilename, nbZones=int(options.nbZones), seed=int(options.seed), nbRuns=int(options.nbRuns), skip=int(options.skip), nbSteps=int(options.nbSteps))


	if options.annType == "ESN":
		from ESN import *
		nbSteps = 200
		nbInputs = 20
		nbOutputs = 2
		reservoirSize = 100
		nnBase = ESN(nbInputs, nbOutputs, reservoirSize, 0.10, 0.88, allowInputToOutputDirectConnections=False, allowOutputSelfRecurrentConnections=False, allowInputToReservoirConnections=True, fixedInputToReservoirConnections=False, allowOutputToReservoirConnections=True)

	elif options.annType == "SESN":
		from SESN import *
		nbSteps = 200
		nbInputs = 20
		nbOutputs = 2
		reservoirSize = 100
		nnBase = SESN(nbInputs, nbOutputs, reservoirSize, 0.10, 0.88, allowInputToOutputDirectConnections=False, allowOutputSelfRecurrentConnections=False, allowInputToReservoirConnections=True, fixedInputToReservoirConnections=False, allowOutputToReservoirConnections=True, densityOfStochasticConnections=0.05)

	elif options.annType == "MLP":
		from keras.models import Model, Input
		from keras.layers import Dense
		from evoKeras import *

		inputLayer = Input(shape=(1,20))
		hiddenLayer = Dense(100)(inputLayer)
		outputLayer = Dense(2)(hiddenLayer)
		model = Model(inputLayer, outputLayer)
		nnBase = EvolvableKerasModel(model, 20, 2)


	res = f.fitness(np.random.uniform(-0.5, 0.5, nnBase.getNbWeights()), nnBase, annStepsPerSimStep, options.inputDir, options.outputDir)
	print("RESULT: ", res)


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
