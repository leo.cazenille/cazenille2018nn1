#!/usr/bin/env python

"""
Scores
"""

import os
import sys
from math import *
import scipy.stats
from collections import Counter
from scipy.stats.mstats import gmean

from fitness import *

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"



#def analyse_subgroups_meanFractionInSGOfSize(dataDirs, arenaSizeX, arenaSizeY, nAnimals, nRobots, nColumn = 3):
#	nAgents = nAnimals + nRobots
#
#	cumulFractionInSGOfSizes = [ ]
#
#	for dataDirIndex, dataDir in enumerate(dataDirs):
#		cumulFractionInSGOfSize = []
#		files = []
#		for f in os.listdir(dataDir):
#			if f.endswith(".txt"):
#				files.append(f)
#
#		for ii in range(0, len(files)):
#			filename, fileext = os.path.splitext(files[ii])
#			with open(os.path.join(dataDir, files[ii]), "r") as source:
#				data_full = np.loadtxt(source, skiprows = 1)
#			time = data_full[:,0]
#			data = data_full[:,np.sort(np.hstack([np.arange(nAgents)* nColumn + 1, np.arange(nAgents) * nColumn + 2]))]
#
#			data2 = []
#			for i in range(data.shape[0]):
#				d = []
#				for j in range(data.shape[1] / 2):
#					d.append([data[i,j*2], data[i,j*2+1]])
#				data2.append(d)
#			groups = findGroups(data2, 0.30, True)
#
#			# Fraction In SG of Size X
#			fractionInSGOfSize = [ ]
#			for t in range(len(data2)):
#				vals = np.array(groups[t])
#				vals = vals[vals > 0]
#				counter = Counter(vals)
#				fractionInSGOfSizeTmp = [ 0. for i in range(1, nAgents+1) ]
#				for indiv in range(len(data2[t])):
#					if not (np.any(np.isnan(data2[t][indiv])) or groups[t][indiv] < 0):
#						c = counter[groups[t][indiv]] - 1
#						fractionInSGOfSizeTmp[c] += 1. / float(len(vals)) #nAgents
#				fractionInSGOfSize.append(fractionInSGOfSizeTmp)
#			cumul = np.cumsum(fractionInSGOfSize, axis=0) / float(len(data2))
#			cumulFractionInSGOfSize.append(cumul)
#
#		cumulFractionInSGOfSize = np.array(cumulFractionInSGOfSize)
#		cumulFractionInSGOfSizes.append(cumulFractionInSGOfSize)
#
#
#	fractions = [np.mean(cumulFractionInSGOfSize[:,-1], axis=0) for cumulFractionInSGOfSize in cumulFractionInSGOfSizes]
#	stds = [np.std(cumulFractionInSGOfSize[:,-1], axis=0) for cumulFractionInSGOfSize in cumulFractionInSGOfSizes]
#
#	return fractions
#
#
#
#
#
#def _interindivDist(d):
#	res = 0.
#	n = 0.
#	for i in range(len(d)):
#		for j in range(i, len(d)):
#			x1, y1 = d[i]
#			x2, y2 = d[j]
#			foo = np.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2))
#			if not np.isnan(foo):
#				res += foo
#				n += 1.
#	if n == 0.:
#		return 0.
#	else:
#		return res / n
#
#
#
#def analyse_subgroups_distances(dataDir, nAnimals, nRobots, nColumn = 3):
#	nAgents = nAnimals + nRobots
#	nbBins = 10
#	files = []
#	for f in os.listdir(dataDir):
#		if f.endswith(".txt"):
#			files.append(f)
#
#	distsSG = [ [] for i in range(0, nAgents-1) ]
#
#	for ii in range(0, len(files)):
#		filename, fileext = os.path.splitext(files[ii])
#		with open(os.path.join(dataDir ,files[ii]), "r") as source:
#			data_full = np.loadtxt(source, skiprows = 1)
#		time = data_full[:,0]
#		data = data_full[:,np.sort(np.hstack([np.arange(nAgents) * nColumn + 1, np.arange(nAgents) * nColumn + 2]))]
#		data2 = []
#		for i in range(data.shape[0]):
#			d = []
#			for j in range(data.shape[1] / 2):
#				d.append([data[i,j*2], data[i,j*2+1]])
#			data2.append(d)
#		groups = findGroups(data2, 0.20, True)
#
#		for t in range(len(data2)):
#			currentSGs = np.array(groups[t])
#			currentSGs = currentSGs[currentSGs > 0]
#			counterSGs = Counter(currentSGs)
#			distsCurrentSGs = {}
#			for k,v in counterSGs.items():
#				distsCurrentSGs[k] = []
#			for indiv in range(len(data2[t])):
#				if not (np.any(np.isnan(data2[t][indiv])) or groups[t][indiv] < 0):
#					distsCurrentSGs[groups[t][indiv]].append(data2[t][indiv])
#			for k,v in distsCurrentSGs.items():
#				d = _interindivDist(v)
#				SGsize = counterSGs[k] - 1
#				if SGsize >= 1:
#					distsSG[SGsize-1].append(d)
#
#	hists = [ np.histogram(x, nbBins)[0] / float(len(x)) for x in distsSG ]
#	return hists
#
#
#
#def analyse_speeds(dataDir, nAnimals, nRobots, picmaze, fps, nColumn = 3):
#	nAgents = nAnimals + nRobots
#	nbBins = 12
#
#	speedsRooms = []
#	speedsCorridor = []
#
#	files = []
#	for f in os.listdir(dataDir):
#		if f.endswith(".txt"):
#			files.append(f)
#
#	for ii in range(0, len(files)):
#		filename, fileext = os.path.splitext(files[ii])
#		with open(os.path.join(dataDir, files[ii]), "r") as source:
#			data_full = np.loadtxt(source, skiprows = 1)
#		time = data_full[:, 0]
#		data = data_full[:, np.sort(np.hstack([np.arange(nAgents) * nColumn + 1, np.arange(nAgents) * nColumn + 2]))]
#		_, _, posR = mazeexploit3rooms(data, nAgents, picmaze) # 0: nan, 1: room1, 2: room2, 4: corridor
#
#		linearSpeed = linSpeed(data, fps, nAgents)
#
#		for i in range(data.shape[0]):
#			for j in range(nAgents):
#				speed = linearSpeed[i, j]
#				if not np.isnan(speed):
#					if posR[i, j] == 1 or posR[i, j] == 2:
#						speedsRooms.append(speed)
#					elif posR[i, j] == 4:
#						speedsCorridor.append(speed)
#
#	histRooms = np.histogram(speedsRooms, nbBins)[0] / float(len(speedsRooms))
#	histCorridor = np.histogram(speedsCorridor, nbBins)[0] / float(len(speedsCorridor))
#	return histRooms, histCorridor


def _printResInTable(dist):
	maxIndex = np.argmax(dist)
	str = ""
	for i,x in enumerate(dist):
		if i == maxIndex:
			str += "{\\boldmath $%.3f$}" % x
		else:
			str += "$%.3f$" % x
		if i < len(dist):
			str += " & "
	print("\t\t" + str + "\\\\\n")



if __name__ == "__main__":
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option("-o", "--outDirectory", dest = "outDirectory", help = "Directory of the plots")
	parser.add_option("-x", "--sizeX", type = "float", dest = "sizeX", help = "Size of the square arena", default=1)
	parser.add_option("-y", "--sizeY", type = "float", dest = "sizeY", help = "Size of the square arena", default=1)
	parser.add_option("-a", "--nAnimals", type = "int", dest = "nAnimals", help = "Number of Animals")
	parser.add_option("-r", "--nRobots", type = "int", dest = "nRobots", help = "Number of robots")
	parser.add_option("-S", "--outputFilenameSuffix", dest = "outputFilenameSuffix", help = "Output Filename Suffix")
	parser.add_option("-C", "--nColumn", type="int", dest = "nColumn", help = "nColumn", default=3)
	parser.add_option("-m", "--picmaze", dest = "picmaze", help = "picture of the maze", default='2RColorLarge.png')
	parser.add_option("-f", "--fps", dest = "fps", help = "FPS", default=15.)

	parser.add_option("--inDirectoryRef", dest = "inDirectory0", help = "Directory of the data Ref")
	parser.add_option("--inDirectoryDNN", dest = "inDirectory1", help = "Directory of the data DNN")
	parser.add_option("--inDirectoryDBN", dest = "inDirectory2", help = "Directory of the data DBN")
	parser.add_option("--inDirectoryLSTM", dest = "inDirectory3", help = "Directory of the data LSTM")

	(options, args) = parser.parse_args()
	inDirectories = [options.inDirectory0, options.inDirectory1, options.inDirectory2, options.inDirectory3]

	lSpeeds = []
	aSpeeds = []
	interindivDists = []
	distsToWalls = []
	for i in range(len(inDirectories)):
		f = FitnessCalibration(inDirectories[i], mapFilename = options.picmaze, nbZones=2, seed=42, nbRuns=1, nbSteps=1, dt=1./3.)

		linearSpeedsInZonesDistrib, linearSpeedsInZones = f.referenceFSet.getLinearSpeedInZones()
		#print("linearSpeedsInZonesDistrib: ", linearSpeedsInZonesDistrib[1])
		lSpeeds.append(linearSpeedsInZonesDistrib[1])

		angularSpeedInZonesDistrib, angularSpeedInZones = f.referenceFSet.getAngularSpeedInZones()
		#print("angularSpeedInZonesDistrib: ", angularSpeedInZonesDistrib[1])
		aSpeeds.append(angularSpeedInZonesDistrib[1])

		interindivDistInZonesDistrib, interindivDistInZones = f.referenceFSet.getInterindivDistInZones()
		#print("interindivDistInZonesDistrib: ", interindivDistInZonesDistrib[1])
		interindivDists.append(interindivDistInZonesDistrib[1])

		distToWallsByZonesDistrib, distToWallsByZones = f.referenceFSet.getDistToWallsByZones()
		#print("distToWallsByZonesDistrib: ", distToWallsByZonesDistrib[1])
		distsToWalls.append(distToWallsByZonesDistrib[1])


	scoresLSpeeds = []
	scoresASpeeds = []
	scoresInterindivDists = []
	scoresDistsToWalls = []
	for i in range(1, len(inDirectories)):
		scoresLSpeeds.append(1. - hellingerDistance(lSpeeds[0], lSpeeds[i]))
		scoresASpeeds.append(1. - hellingerDistance(aSpeeds[0], aSpeeds[i]))
		scoresInterindivDists.append(1. - hellingerDistance(interindivDists[0], interindivDists[i]))
		scoresDistsToWalls.append(1. - hellingerDistance(distsToWalls[0], distsToWalls[i]))
	print("scoresLSpeeds", scoresLSpeeds)
	print("scoresASpeeds", scoresASpeeds)
	print("scoresInterindivDists", scoresInterindivDists)
	print("scoresDistsToWalls", scoresDistsToWalls)
	scores = np.array([scoresLSpeeds, scoresASpeeds, scoresInterindivDists, scoresDistsToWalls])
	gmeanScores = gmean(scores, axis = 0)
	print("gmeanScores:", gmeanScores)


#	histSpeeds = []
#	for i in range(len(inDirectories)):
#		histSpeeds.append(analyse_speeds(inDirectories[i], int(options.nAnimals), int(options.nRobots), options.picmaze, int(options.fps), int(options.nColumn)))
#	scoresSpeedsRooms = []
#	for i in range(1, len(histSpeeds)):
#		scoresSpeedsRooms.append(1. - hellingerDistance(histSpeeds[0][0], histSpeeds[i][0]))
#	scoresSpeedsCorridor = []
#	for i in range(1, len(histSpeeds)):
#		scoresSpeedsCorridor.append(1. - hellingerDistance(histSpeeds[0][1], histSpeeds[i][1]))
#
#	scoresDists = []
#	distRef = analyse_subgroups_distances(inDirectories[0], int(options.nAnimals), int(options.nRobots), int(options.nColumn))
#	for i in range(1, len(inDirectories)):
#		dists = analyse_subgroups_distances(inDirectories[i], int(options.nAnimals), int(options.nRobots), int(options.nColumn))
#		#print "DEBUG1:", dists
#		scoreTmp = []
#		for k in range(len(dists)):
#			scoreTmp.append(1. - hellingerDistance(distRef[k], dists[k]))
#		#	print "DEBUG2:", hellingerDistance(distRef[k], dists[k]), np.sum(distRef[k]), np.sum(dists[k])
#		scoresDists.append(scoreTmp)
#	scoresDists = np.array(scoresDists).T
#
#
#	sgSizes = analyse_subgroups_meanFractionInSGOfSize(inDirectories, int(options.sizeX), int(options.sizeY), int(options.nAnimals), int(options.nRobots), int(options.nColumn))
#	scoresSGSizes = []
#	for i in range(1, len(sgSizes)):
#		scoresSGSizes.append(1. - hellingerDistance(sgSizes[0], sgSizes[i]))
#
#	meanScores = []
#	for i in range(0, len(scoresSpeedsRooms)):
#		s = [scoresSpeedsRooms[i], scoresSpeedsCorridor[i], scoresSGSizes[i]]
#		for j in range(len(scoresDists)):
#			s.append(scoresDists[j][i])
#		meanScores.append(np.mean(np.array(s)))
#
#	print("""
#	\\begin{table}[h]
#		\\centering
#		\\begin{tabular}{@{} |c |c c c c| @{}}
#			\\hline
#			& \\textbf{C1} & \\textbf{C2} & \\textbf{C3} & \\textbf{C4} \\\\
#			\\cline{1-6}
#			\\hline
#	""")
#
#	print("\n\t\tLinear speed in the rooms &")
#	_printResInTable(scoresSpeedsRooms)
#
#
#	print("\n\t\tLinear speed in the corridor &")
#	_printResInTable(scoresSpeedsCorridor)
#
#
#	for i in range(len(scoresDists)):
#		print("\n\t\tInter-indiv. distances in SG of %d indiv. & " % (i + 2))
#		_printResInTable(scoresDists[i])
#
#	print("\n\t\tDistribution of SG size &")
#	_printResInTable(scoresSGSizes)
#
#	print("\n\t\t\\hline")
#	print("\n\t\tSocial integration index (Mean score) &")
#	_printResInTable(meanScores)
#
#	print("""\n
#			\\hline
#		\\end{tabular}
#		\\caption{\\TODO{Score: Hellinger distance}}
#		\\label{tab:scores}
#	\\end{table}
#	""")


#\begin{table}[ht]
#\centering
#\begin{tabular}{|l|l|l|l|}
#\hline
# & DNN & DBN & LSTM \\
#\hline
# inter-individual distances & $0.00$ & $0.00$ & $0.00$ \\
#\hline
# linear speeds & $0.00$ & $0.00$ & $0.00$ \\
#\hline
# angular speeds & $0.00$ & $0.00$ & $0.00$ \\
#\hline
# distances to nearest wall & $0.00$ & $0.00$ & $0.00$ \\
#%\hline
#% polarisation & $0.00$ & $0.00$ & $0.00$ \\
#\hline
#\end{tabular}
#\caption{\label{tab:score} Comparison between experimental data and simulations}
#\end{table}



