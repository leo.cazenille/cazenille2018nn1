#!/usr/bin/env python

"""
Make plot of scores
"""

import os
import sys
from math import *
import scipy.stats
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
#import seaborn as sns
#sns.set_style("ticks")

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"



interindivDists = [ 0.296,  0.321,  0.329,  0.319,  0.368,  0.342,  0.339,  0.328, 0.335]
linearSpeeds = [ 0.587,  0.645,  0.606,  0.804,  0.836,  0.747,  0.805,  0.76 , 0.721]
angularSpeeds = [ 0.387,  0.38 ,  0.426,  0.483,  0.515,  0.492,  0.485,  0.491, 0.482]
distToWalls = [ 0.843,  0.91 ,  0.89 ,  0.883,  0.935,  0.88 ,  0.865,  0.9  , 0.884]
meanScores = [ 0.488,  0.517,  0.524,  0.575,  0.621,  0.577,  0.581,  0.576, 0.566]



if __name__ == "__main__":
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option("-o", "--outputFilename", dest = "outputFilename", help = "Output path of the plot")
	parser.add_option("-t", "--type", dest = "type", help = "Type of data (interindivDists, linearSpeeds, angularSpeeds, distToWalls, meanScores)", default="meanScores")

	(options, args) = parser.parse_args()

	dataType = options.type

	#matplotlib.rcParams.update({'font.size': 10})

	fig = plt.figure(figsize=(4,4))
	ax = fig.add_subplot(111)
	fig.subplots_adjust(bottom=0.3)

	X = np.array(range(1, 10))

	if dataType == "interindivDists":
		ax.plot(X, interindivDists, 'r', label='inter-individual distances')
		ax.set_title("Inter-individual distances", y=1.15, fontsize=18)
	elif dataType == "linearSpeeds":
		ax.plot(X, linearSpeeds, 'g', label='linear speeds')
		ax.set_title("Linear speeds", y=1.15, fontsize=18)
	elif dataType == "angularSpeeds":
		ax.plot(X, angularSpeeds, 'b', label='angular speeds')
		ax.set_title("Angular speeds", y=1.15, fontsize=18)
	elif dataType == "distToWalls":
		ax.plot(X, distToWalls, 'orange', label='distances to nearest wall')
		ax.set_title("Distances to nearest wall", y=1.15, fontsize=18)
	elif dataType == "meanScores":
		ax.plot(X, meanScores, 'k', label='mean scores')
		ax.set_title("Mean scores", y=1.15, fontsize=18)

	##legend = ax.legend(loc="upper right", frameon = True, bbox_to_anchor=(1.1, 1.05))
	#legend = ax.legend(loc="upper left", frameon = True)

	ax.xaxis.set_ticks(X)
	ax.set_ylabel("Score", fontsize=18)
	ax.set_xlim([1, 9])
	plt.yticks(fontsize=16)

	yl = ax.get_ylim()
	yl = (yl[0], yl[1] + 0.025)
	#ax.set_ylim([0.25, 1.0])
	ax.set_ylim(yl)

	X2 = [r"$\frac{%u}{3}$" % x for x in X] # FPS=3.0sec
	ax.set_xticklabels(X2, fontsize=20)
	ax.set_xlabel("Time (s)", fontsize=18)


	ax2 = ax.twiny()
	# Move twinned axis ticks and label from top to bottom
	#ax2.xaxis.set_ticks_position("bottom")
	#ax2.xaxis.set_label_position("bottom")
	ax2.xaxis.set_ticks_position("top")
	ax2.xaxis.set_label_position("top")
	## Offset the twin axis below the host
	#ax2.spines["bottom"].set_position(("axes", -0.15))
	## Turn on the frame for the twin axis, but then hide all 
	## but the bottom spine
	#ax2.set_frame_on(True)
	#ax2.patch.set_visible(False)
	##for sp in ax2.spines.itervalues():
	##	sp.set_visible(False)
	#ax2.spines["bottom"].set_visible(True)
	ax2.set_xlim([1, 9])
	#ax2.set_ylim([0.25, 1.0])
	ax2.set_ylim(yl)

	ax2.xaxis.set_ticks(X)
	ax2.set_xticklabels(X, fontsize=16)
	ax2.set_xlabel("Size of time window", fontsize=18)
	ax2.xaxis.set_label_coords(0.5, 0.90)
	#ax.annotate("Time (s)", fontsize=14, xy=(0.5, 0.9), ha='left', va='top', xycoords='axes fraction', textcoords='offset points')

	plt.axvline(x=5.0, linestyle=':', alpha=0.7, color='k')

	#sns.despine()
	plt.tight_layout()
	#plt.subplots_adjust(left=0.01, right=0.99, top=0.99, bottom=0.01)
	fig.savefig(options.outputFilename)
	plt.close(fig)



# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
