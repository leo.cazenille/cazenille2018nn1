#!/bin/bash


OUTPUTDIR=../tikz/plots
mkdir -p $OUTPUTDIR


function copyPlotsDir {
	dir=$1
	shift;
	cp $dir/FstSplittedIndiv0Trace0-*.pdf $OUTPUTDIR
	cp $dir/hist-zone1-angularSpeedInZones-*.pdf $OUTPUTDIR
	cp $dir/hist-zone1-distToWallsByZones-*.pdf $OUTPUTDIR
	cp $dir/hist-zone1-interindivDistInZones-*.pdf $OUTPUTDIR
	cp $dir/hist-zone1-linearSpeedInZones-*.pdf $OUTPUTDIR
	cp $dir/hist-zone1-polarizationInZones-*.pdf $OUTPUTDIR

	for i in $(cd $dir; ls MeanPresence2D-*.pdf); do
		convert $dir/$i $OUTPUTDIR/`echo $i | sed 's/\.pdf/\.png/'` &
	done
}


for fps in 15; do
	copyPlotsDir plots6/referenceNoGap${fps}FPS/1
#	for nbAgents in 1 3 5; do
#		copyPlotsDir plots6/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_500_240/1
#		copyPlotsDir plots6/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_500_240/1
#	done

done

copyPlotsDir plots6/15FPS_1AGENTS_CMAESV5_MLP_500_120/9
copyPlotsDir plots6/15FPS_3AGENTS_CMAESV5_MLP_500_120/1
copyPlotsDir plots6/15FPS_5AGENTS_CMAESV5_MLP_500_120/10
copyPlotsDir plots6/15FPS_1AGENTS_NSGA3V5_MLP_500_120/3
copyPlotsDir plots6/15FPS_3AGENTS_NSGA3V5_MLP_500_120/3 # XXX
copyPlotsDir plots6/15FPS_5AGENTS_NSGA3V5_MLP_500_120/2 # 9 best but bad presence
copyPlotsDir plots6/15FPS_1AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120/1 # XXX
copyPlotsDir plots6/15FPS_3AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120/6 # XXX
copyPlotsDir plots6/15FPS_5AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120/1 # XXX
copyPlotsDir plots6/15FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_120/6 # XXX
copyPlotsDir plots6/15FPS_3AGENTS_NSGA3V5TRAJENV2_MLP_500_120/5
copyPlotsDir plots6/15FPS_5AGENTS_NSGA3V5TRAJENV2_MLP_500_120/9 # 2 best but bad presence
copyPlotsDir plots6/15FPS_1AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120/6
copyPlotsDir plots6/15FPS_3AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120/1 # XXX
copyPlotsDir plots6/15FPS_5AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120/8

copyPlotsDir plots6/3FPS_1AGENTS_SL_MLP/1
copyPlotsDir plots6/3FPS_3AGENTS_SL_MLP/1
copyPlotsDir plots6/3FPS_5AGENTS_SL_MLP/1


#copyPlotsDir plots6/5FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_240/2
#cp plots6/5FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_240/1/FstSplittedIndiv0Trace3-5FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_240.pdf $OUTPUTDIR/FstSplittedIndiv0Trace0-5FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_240.pdf
#copyPlotsDir plots6/5FPS_2AGENTS_NSGA3V5TRAJENV2_MLP_500_240/1
#cp plots6/5FPS_2AGENTS_NSGA3V5TRAJENV2_MLP_500_240/1/FstSplittedIndiv0Trace7-5FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_240.pdf $OUTPUTDIR/FstSplittedIndiv0Trace0-5FPS_2AGENTS_NSGA3V5TRAJENV2_MLP_500_240.pdf
#copyPlotsDir plots6/5FPS_3AGENTS_NSGA3V5TRAJENV2_MLP_500_240/2
#copyPlotsDir plots6/5FPS_4AGENTS_NSGA3V5TRAJENV2_MLP_500_240/1
#copyPlotsDir plots6/5FPS_5AGENTS_NSGA3V5TRAJENV2_MLP_500_240/3


cp plots6/*BestScores.pdf $OUTPUTDIR
cp plots6/*Fitness.pdf $OUTPUTDIR


#for fps in 1 5 15; do
#	copyPlotsDir plots4/referenceNoGap${fps}FPS
#	for nbAgents in 1 2 3 4 5; do
#		copyPlotsDir plots4/${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_500_120/1
#		copyPlotsDir plots4/${fps}FPS_${nbAgents}AGENTS_NSGA3ALLOBJ_MLP_500_120/1
#	done
#done


#cp goodPlots9/AllScores1.pdf ../plots/AllScores1.pdf
#cp goodPlots9/AllScores2.pdf ../plots/AllScores2.pdf
#
##cp goodPlots9/reference/FstSplittedIndiv0Trace0-ref.pdf ../plots/FstSplittedIndiv0Traces0-reference.pdf
#cp goodPlots9/reference/FstSplittedIndiv4Trace0-ref.pdf ../plots/FstSplittedIndiv4Traces0-reference.pdf
#cp goodPlots9/reference/hist-zone1-angularSpeedInZones-ref.pdf ../plots/hist-zone1-angularSpeedInZones-reference.pdf
#cp goodPlots9/reference/hist-zone1-distToWallsByZones-ref.pdf ../plots/hist-zone1-distToWallsByZones-reference.pdf
#cp goodPlots9/reference/hist-zone1-interindivDistInZones-ref.pdf ../plots/hist-zone1-interindivDistInZones-reference.pdf
#cp goodPlots9/reference/hist-zone1-linearSpeedInZones-ref.pdf ../plots/hist-zone1-linearSpeedInZones-reference.pdf
#cp goodPlots9/reference/hist-zone1-polarizationInZones-ref.pdf ../plots/hist-zone1-polarizationInZones-reference.pdf
##cp goodPlots9/reference/MeanPresence2D-ref.pdf ../plots/MeanPresence2D-reference.pdf
#convert goodPlots9/reference/MeanPresence2D-ref.pdf ../plots/MeanPresence2D-reference.png
#
#
##cp goodPlots9/LAST_CMAES_MLP_100/1/FstSplittedIndiv0Trace0-res.pdf ../plots/FstSplittedIndiv0Traces0-CMAES.pdf
#cp goodPlots9/LAST_CMAES_MLP_100/1/FstSplittedIndiv4Trace0-res.pdf ../plots/FstSplittedIndiv4Traces0-CMAES.pdf
#cp goodPlots9/LAST_CMAES_MLP_100/1/hist-zone1-angularSpeedInZones-res.pdf ../plots/hist-zone1-angularSpeedInZones-CMAES.pdf
#cp goodPlots9/LAST_CMAES_MLP_100/1/hist-zone1-distToWallsByZones-res.pdf ../plots/hist-zone1-distToWallsByZones-CMAES.pdf
#cp goodPlots9/LAST_CMAES_MLP_100/1/hist-zone1-interindivDistInZones-res.pdf ../plots/hist-zone1-interindivDistInZones-CMAES.pdf
#cp goodPlots9/LAST_CMAES_MLP_100/1/hist-zone1-linearSpeedInZones-res.pdf ../plots/hist-zone1-linearSpeedInZones-CMAES.pdf
#cp goodPlots9/LAST_CMAES_MLP_100/1/hist-zone1-polarizationInZones-res.pdf ../plots/hist-zone1-polarizationInZones-CMAES.pdf
##cp goodPlots9/LAST_CMAES_MLP_100/1/MeanPresence2D-res.pdf ../plots/MeanPresence2D-CMAES.pdf
#convert goodPlots9/LAST_CMAES_MLP_100/1/MeanPresence2D-res.pdf ../plots/MeanPresence2D-CMAES.png
#
##cp goodPlots9/LAST_NSGA3_MLP_100/1/FstSplittedIndiv0Trace0-res.pdf ../plots/FstSplittedIndiv0Traces0-NSGAIII.pdf
#cp goodPlots9/LAST_NSGA3_MLP_100/1/FstSplittedIndiv4Trace0-res.pdf ../plots/FstSplittedIndiv4Traces0-NSGAIII.pdf
#cp goodPlots9/LAST_NSGA3_MLP_100/1/hist-zone1-angularSpeedInZones-res.pdf ../plots/hist-zone1-angularSpeedInZones-NSGAIII.pdf
#cp goodPlots9/LAST_NSGA3_MLP_100/1/hist-zone1-distToWallsByZones-res.pdf ../plots/hist-zone1-distToWallsByZones-NSGAIII.pdf
#cp goodPlots9/LAST_NSGA3_MLP_100/1/hist-zone1-interindivDistInZones-res.pdf ../plots/hist-zone1-interindivDistInZones-NSGAIII.pdf
#cp goodPlots9/LAST_NSGA3_MLP_100/1/hist-zone1-linearSpeedInZones-res.pdf ../plots/hist-zone1-linearSpeedInZones-NSGAIII.pdf
#cp goodPlots9/LAST_NSGA3_MLP_100/1/hist-zone1-polarizationInZones-res.pdf ../plots/hist-zone1-polarizationInZones-NSGAIII.pdf
##cp goodPlots9/LAST_NSGA3_MLP_100/1/MeanPresence2D-res.pdf ../plots/MeanPresence2D-NSGAIII.pdf
#convert goodPlots9/LAST_NSGA3_MLP_100/1/MeanPresence2D-res.pdf ../plots/MeanPresence2D-NSGAIII.png
#
##cp goodPlots9/LAST_NSGA3_ESN_100/1/FstSplittedIndiv0Trace0-res.pdf ../plots/FstSplittedIndiv0Traces0-NSGAIII-ESN.pdf
#cp goodPlots9/LAST_NSGA3_ESN_100/1/FstSplittedIndiv4Trace0-res.pdf ../plots/FstSplittedIndiv4Traces0-NSGAIII-ESN.pdf
#cp goodPlots9/LAST_NSGA3_ESN_100/1/hist-zone1-angularSpeedInZones-res.pdf ../plots/hist-zone1-angularSpeedInZones-NSGAIII-ESN.pdf
#cp goodPlots9/LAST_NSGA3_ESN_100/1/hist-zone1-distToWallsByZones-res.pdf ../plots/hist-zone1-distToWallsByZones-NSGAIII-ESN.pdf
#cp goodPlots9/LAST_NSGA3_ESN_100/1/hist-zone1-interindivDistInZones-res.pdf ../plots/hist-zone1-interindivDistInZones-NSGAIII-ESN.pdf
#cp goodPlots9/LAST_NSGA3_ESN_100/1/hist-zone1-linearSpeedInZones-res.pdf ../plots/hist-zone1-linearSpeedInZones-NSGAIII-ESN.pdf
#cp goodPlots9/LAST_NSGA3_ESN_100/1/hist-zone1-polarizationInZones-res.pdf ../plots/hist-zone1-polarizationInZones-NSGAIII-ESN.pdf
##cp goodPlots9/LAST_NSGA3_ESN_100/1/MeanPresence2D-res.pdf ../plots/MeanPresence2D-NSGAIII-ESN.pdf
#convert goodPlots9/LAST_NSGA3_ESN_100/1/MeanPresence2D-res.pdf ../plots/MeanPresence2D-NSGAIII-ESN.png



# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
