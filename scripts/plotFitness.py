#!/usr/bin/env python3

"""
Plot fitness
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab
from fitness import *
import os
from optparse import OptionParser
import scipy.constants
import copy
import pickle

import seaborn as sns
sns.set_style("ticks")

import pandas as pd


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


########### MAIN ########### {{{1
if __name__ == "__main__":
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-o", "--outputDir", dest = "outputDir", default = "scores", help = "Directory of output files")
	parser.add_option("--outputSuffix", dest = "outputSuffix", default = "Scores", help = "Suffix of output plots filenames.")
	parser.add_option("--nbEvalsPerGen", dest = "nbEvalsPerGen", default = 120, help = "Number of evals per generation.")


	(options, args) = parser.parse_args()
	outputSuffix = options.outputSuffix
	nbEvalsPerGen = int(options.nbEvalsPerGen)
	labels = args[0::3]
	paths = args[1::3]
	dataNbAgents = args[2::3]

	print("labels:", labels)
	print("paths:", paths)
	print("dataNbAgents:", dataNbAgents)

	scores = []
	scoresPerGen = []

	for lab, path in zip(labels, paths):
		print("Computing scores for expe '%s' on path '%s'" % (lab, path))
		scores.append([])
		scoresPerGen.append([])

		d = next(os.walk(path))

		d_ = d[1]
		dPath = [os.path.join(path, o) for o in d_]
		#print("DEBUG3: ", dPath)
		for dExpeIndex in range(len(dPath)):
			dExpe = dPath[dExpeIndex]
			if not len(next(os.walk(dExpe))[2]):
				print("Directory '%s' is empty !" % dExpe)
				continue # Directory empty
			with open(os.path.join(dExpe, "best.p"), 'rb') as f:
				expeRes = pickle.load(f, encoding='latin1')
				fbest = expeRes['fbest'][:,0]
				scores[-1].append(max(fbest))
				scoresPerGen[-1].append(fbest)
		#print("DEBUG4: ", d, scores[-1])



	#scores = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]

	plotNames = ['PerfFitness']
	plotTypes = ["Performance Fitness"]
	plotDomain = [(0.5, 0.8)]
	scoresList = [scores]
	labels2 = [a.replace("\\n", "\n").replace("\"", "").replace("~", " ") for a in labels]
	labelTypes = list(sorted(list(set(labels2))))
	if 'Control' in labelTypes:
		labelTypes.remove('Control')
	#print("DEBUG5:", labelTypes)

	plt.style.use('grayscale')
	for plotN, plotT, plotD, s in zip(plotNames, plotTypes, plotDomain, scoresList):
		fig = plt.figure(figsize=(4.*scipy.constants.golden * 1.5, 4.2))
		ax = fig.add_subplot(111)
		fig.subplots_adjust(bottom=0.3)

		ax.set_ylim(plotD) #([0.3, 1.0])
		plt.ylabel(plotT, fontsize=18)
		yticksDomain = [int(i * 10) for i in plotD]
		yticks = [float(i) / 10. for i in list(range(yticksDomain[0], yticksDomain[1] + 1))]
		yticksTop = plotD[1]
		plt.yticks(yticks, fontsize=18)

		bp = plt.boxplot(s, positions=np.array(range(len(labels2))), sym='', widths=0.6)
		plt.xticks(np.array(range(len(labels2))), labels2, fontsize=16, rotation=30, ha='right')

		agentText = (lambda i: "%s agent" % i if i == '1' else "%s agents" % i)
		uniqueNbAgents = list(sorted(list(set(dataNbAgents))))
		if '0' in uniqueNbAgents:
			uniqueNbAgents.remove('0')
		#print("DEBUG3:", uniqueNbAgents, dataNbAgents)

		for i in range(len(uniqueNbAgents)):
			#ax.text((i+1) * len(labelTypes) - (len(labelTypes) - 1) + len(labelTypes) / 2, 0.99, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
			ax.text((i + 0.5) * len(labelTypes) - 0.4, yticksTop + 0.01, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
			#plt.hlines(0.99, (i) * len(labelTypes) - 0.3, (i+1) * len(labelTypes) - 0.4)
			plt.hlines(yticksTop - 0.01, (i) * len(labelTypes) - 0.25, (i+1) * len(labelTypes) - 0.75)
			if i != 0:
				plt.axvline(x=(i) * len(labelTypes) - 0.5, ls=':')

		sns.despine()
		plt.tight_layout(rect=[0, 0, 1.0, 0.95])
		fig.savefig(os.path.join(options.outputDir, "%s-%s.pdf" % (plotN, outputSuffix)))
		plt.close(fig)



	# Plot evolution of fitness per gen
	for label, nbAgents, s in zip(labels, dataNbAgents, scoresPerGen):
		plotName = "%sAGENTS_%s" % (nbAgents, label.replace('"', ''))
		plotFile = os.path.join(options.outputDir, "%s-%s.pdf" % (plotName, outputSuffix))
		#print("DEBUG10:", plotName, plotFile)
		fig = plt.figure(figsize=(3.*scipy.constants.golden, 3.))
		ax = fig.add_subplot(111)
		fig.subplots_adjust(bottom=0.3)
		#x = range(len(s))
		s2 = np.array(s).T
		#x = range(0, len(s2) * nbEvalsPerGen, nbEvalsPerGen)
		x = range(0, len(s2), len(s2)//5)
		#d = pd.DataFrame(s2, index=x)
		d = pd.DataFrame(s2)
		#print("DEBUGTSPLOT: ", d.values.shape, len(x), s2.shape, nbEvalsPerGen, d)
		#sns.lineplot(x=x, y=s2, ax=ax)
		sns.tsplot(data=d.values.T, ax=ax, ci="sd")
		plt.xlabel("Evaluations", fontsize=15)
		plt.xticks(x, fontsize=15)
		ax.set_xticklabels([str(i * nbEvalsPerGen) for i in x])
		plt.ylabel("Fitness", fontsize=15)
		plt.yticks(fontsize=15)
		sns.despine()
		plt.tight_layout(rect=[0, 0, 1.0, 0.95])
		fig.savefig(plotFile)
		plt.close(fig)


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
