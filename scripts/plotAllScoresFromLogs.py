#!/usr/bin/env python3

"""
Compute scores between fish-only expes
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab
from fitness import *
import os
from optparse import OptionParser
import scipy.constants
import copy

import seaborn as sns
sns.set_style("ticks")


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


########### MAIN ########### {{{1
if __name__ == "__main__":
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-o", "--outputDir", dest = "outputDir", default = "scores", help = "Directory of output files")
	#parser.add_option("-F", "--nbFish", dest = "nbFish", default = 4, help = "Number of fish")
	parser.add_option("--outputSuffix", dest = "outputSuffix", default = "Scores", help = "Suffix of output plots filenames.")


	(options, args) = parser.parse_args()

	#nbFish = int(options.nbFish)
	outputSuffix = options.outputSuffix

	labels = args[0::3]
	paths = args[1::3]
	dataNbAgents = args[2::3]
	#refLabel = options.refLabel #labels[0]
	#refPath = options.refPath #paths[0]

	print("labels:", labels)
	print("paths:", paths)
	print("dataNbAgents:", dataNbAgents)

	scoresInterindivDist = []
	scoresLinearSpeeds = []
	scoresPolarity = []
	scoresProbabilityOfPresence = []
	scoresOutOfBoundsScore = []
	scores = []

	for lab, path in zip(labels, paths):
		print("Computing scores for expe '%s' on path '%s'" % (lab, path))
		scoresInterindivDist.append([])
		scoresLinearSpeeds.append([])
		scoresPolarity.append([])
		scoresProbabilityOfPresence.append([])
		scoresOutOfBoundsScore.append([])
		scores.append([])

		d = next(os.walk(path))

		d_ = d[2]
		dPath = [os.path.join(path, o) for o in d_]
		#print("DEBUG3: ", dPath)
		for fPath in dPath:
			with open(fPath, 'r') as f:
				lines = f.read().splitlines()
				lastLine = lines[-1]
			if not "bestever Scores" in lastLine:
				continue
			lineBesteverScores = lastLine.replace("[", "").replace("]", "").replace("(", "").replace(")", "").replace(" ", "")
			besteverScores = [float(a) for a in lineBesteverScores.split(",")[1:]]
			individualScore, scoreInterindivDist, scoreLinearSpeeds, scorePolarity, scoreProbabilityOfPresence, outOfBoundsScore = besteverScores
			scoresInterindivDist[-1].append(scoreInterindivDist)
			scoresLinearSpeeds[-1].append(scoreLinearSpeeds)
			scoresPolarity[-1].append(scorePolarity)
			scoresProbabilityOfPresence[-1].append(scoreProbabilityOfPresence)
			scoresOutOfBoundsScore[-1].append(outOfBoundsScore)
			scores[-1].append(individualScore)
			print("DEBUG4: ", fPath, lineBesteverScores, besteverScores)



#	fRef = FitnessCalibration(refPath, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
#
#
#	dirs = []
#	dirsPath = []
#	scoresInterindivDist = []
#	scoresLinearSpeeds = []
#	scoresPolarity = []
#	scoresProbabilityOfPresence = []
#	scoresOutOfBoundsScore = []
#	scores = []
#
#
#	for lab, path in zip(labels, paths):
#		print("Computing scores for expe '%s' on path '%s'" % (lab, path))
#		scoresInterindivDist.append([])
#		scoresLinearSpeeds.append([])
#		scoresPolarity.append([])
#		scoresProbabilityOfPresence.append([])
#		scoresOutOfBoundsScore.append([])
#		scores.append([])
#
#		d = next(os.walk(path))
#
#		d_ = d[1]
#		dPath = [os.path.join(path, o) for o in d_]
#		print("DEBUG3: ", dPath)
#		dirsPath.append(dPath)
#		for dExpeIndex in range(len(dPath)):
#			dExpe = dPath[dExpeIndex]
#			if not len(next(os.walk(dExpe))[2]):
#				print("Directory '%s' is empty !" % dExpe)
#				continue # Directory empty
#			f2 = FitnessCalibration(dExpe, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
#			individualScore, allScores = fRef.computeScore(f2.referenceFSet, False, "v5")
#			scoreInterindivDist, scoreLinearSpeeds, scorePolarity, scoreProbabilityOfPresence, outOfBoundsScore = allScores
#			scoresInterindivDist[-1].append(scoreInterindivDist)
#			scoresLinearSpeeds[-1].append(scoreLinearSpeeds)
#			scoresPolarity[-1].append(scorePolarity)
#			scoresProbabilityOfPresence[-1].append(scoreProbabilityOfPresence)
#			scoresOutOfBoundsScore[-1].append(outOfBoundsScore)
#			scores[-1].append(individualScore)
#			print("DEBUG4: ", dExpe, individualScore, allScores)



#	scores = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresInterindivDist = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresLinearSpeeds = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresPolarity = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresProbabilityOfPresence = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]




	plotNames = ['Biomimetism', 'InterindivDist', 'LinearSpeeds', 'Polarity', 'ProbabilityOfPresence']
	plotTypes = ["Biomimetism Score", "Inter-individual Score", "Linear speed Score", "Polarity Score", "Probability of\npresence Score"]
	plotDomain = [(0.5, 0.9), (0.3, 0.9), (0.4, 1.0), (0.5, 1.0), (0.3, 0.9)]
	scoresList = [scores, scoresInterindivDist, scoresLinearSpeeds, scoresPolarity, scoresProbabilityOfPresence]

	labels2 = [a.replace("\\n", "\n").replace("\"", "").replace("~", " ") for a in labels]

	labelTypes = list(sorted(list(set(labels2))))
	if 'Control' in labelTypes:
		labelTypes.remove('Control')
	print("DEBUG5:", labelTypes)

	plt.style.use('grayscale')
	for plotN, plotT, plotD, s in zip(plotNames, plotTypes, plotDomain, scoresList):
		fig = plt.figure(figsize=(4.*scipy.constants.golden * 1.5, 4.2))
		ax = fig.add_subplot(111)
		fig.subplots_adjust(bottom=0.3)

		ax.set_ylim(plotD) #([0.3, 1.0])
		plt.ylabel(plotT, fontsize=18)
		yticksDomain = [int(i * 10) for i in plotD]
		yticks = [float(i) / 10. for i in list(range(yticksDomain[0], yticksDomain[1] + 1))]
		yticksTop = plotD[1]
		plt.yticks(yticks, fontsize=18)

		bp = plt.boxplot(s, positions=np.array(range(len(labels2))), sym='', widths=0.6)
		plt.xticks(np.array(range(len(labels2))), labels2, fontsize=16, rotation=30, ha='right')

		agentText = (lambda i: "%s agent" % i if i == '1' else "%s agents" % i)
		uniqueNbAgents = list(sorted(list(set(dataNbAgents))))
		if '0' in uniqueNbAgents:
			uniqueNbAgents.remove('0')
		#print("DEBUG3:", uniqueNbAgents, dataNbAgents)

		for i in range(len(uniqueNbAgents)):
			#ax.text((i+1) * len(labelTypes) - (len(labelTypes) - 1) + len(labelTypes) / 2, 0.99, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
			ax.text((i + 0.5) * len(labelTypes) - 0.4, yticksTop + 0.01, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
			#plt.hlines(0.99, (i) * len(labelTypes) - 0.3, (i+1) * len(labelTypes) - 0.4)
			plt.hlines(yticksTop - 0.01, (i) * len(labelTypes) - 0.25, (i+1) * len(labelTypes) - 0.75)
			if i != 0:
				plt.axvline(x=(i) * len(labelTypes) - 0.5, ls=':')

		sns.despine()
		plt.tight_layout(rect=[0, 0, 1.0, 0.95])
		fig.savefig(os.path.join(options.outputDir, "%s-%s.pdf" % (plotN, outputSuffix)))
		plt.close(fig)


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
