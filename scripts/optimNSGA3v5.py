#!/usr/bin/env python

"""
Calibrate the NN using NSGA3
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

# Use only CPU in Keras
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import matplotlib.pyplot as plt

import pickle
import math

from evoKeras import *
from ESN import *
from SESN import *

from fitness import *
import sys
import numpy as np
from deap import algorithms
from deap import base
from deap import benchmarks
from deap.benchmarks.tools import diversity, convergence #, hypervolume
from deap import creator
from deap import tools
import array
import copy

from nsgaiii import *

import tensorflow as tf
from keras import backend as K

import multiprocessing
import threading
import gc
import scipy.stats.mstats
from scipy.spatial.distance import euclidean

np.seterr(divide='ignore', invalid='ignore')



########### OPTIM ########### {{{1


def evalFit(params):
	global f
	indiv = params['indiv']
	meanGenomeVals = params['meanGenomeVals']

	config = tf.ConfigProto(intra_op_parallelism_threads=1,
						inter_op_parallelism_threads=1,
						allow_soft_placement=True)
	session = tf.Session(config=config)
	K.set_session(session)

	fC = copy.deepcopy(f)

	perf, allScores = fC.fitness(indiv, nnBase, annStepsPerSimStep, fC.referencePath, nbSimulatedAgents, None, False, "v5")
	K.clear_session()


	if fitnessType == "PerfGendivBehavdiv":
		# Compute genotypic diversity
		genDiversity = 0.
		genDiversityNormalization = 0.
		for val, mean in zip(indiv, meanGenomeVals):
			genDiversity += np.abs(val - mean)
			genDiversityNormalization += 1.
		genDiversity /= genDiversityNormalization

		# Compute behavioural diversity
		behavDiversity = 0.
		behavDiversityNormalization = 0.
		for i, s1 in enumerate(allScores):
			for j, s2 in enumerate(allScores):
				if i == j: continue
				behavDiversity += np.abs(s1 - s2)
				behavDiversityNormalization += 1.
		behavDiversity /= behavDiversityNormalization
		return (perf, genDiversity, behavDiversity)

	elif fitnessType == "PerfNovelty":
		behaviouralArchive = params['behaviouralArchive']
		novelty = 0.0
		for scores in behaviouralArchive:
			dist = euclidean(scores, allScores)
			if dist < novelty or novelty <= 0:
				novelty = dist
		fitness = (perf, novelty)
		return (fitness, allScores)

	elif fitnessType == "PerfNovelty5":
		behaviouralArchive = params['behaviouralArchive']
		if len(behaviouralArchive):
			distsBehaviouralArchive = [euclidean(s, allScores) for s in behaviouralArchive]
			neighbors = sorted(distsBehaviouralArchive)[:5]
			novelty = np.mean(neighbors)
		else:
			novelty = 0.0
		fitness = (perf, novelty)
		return (fitness, allScores)

	elif fitnessType == "PerfNovelty20":
		behaviouralArchive = params['behaviouralArchive']
		if len(behaviouralArchive):
			distsBehaviouralArchive = [euclidean(s, allScores) for s in behaviouralArchive]
			neighbors = sorted(distsBehaviouralArchive)[:20]
			novelty = np.mean(neighbors)
		else:
			novelty = 0.0
		fitness = (perf, novelty)
		return (fitness, allScores)

	elif fitnessType == "PerfTrajobjEnvobj":
		scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence, outOfBoundsScore = allScores
		score0 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence])
		score1 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreLinearSpeeds, scorePolarization])
		score2 = scipy.stats.mstats.gmean([scoreProbabilityOfPresence, outOfBoundsScore])
		return (score0, score1, score2)

	elif fitnessType == "PerfTrajobjEnvobj2":
		scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence, outOfBoundsScore = allScores
		score0 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence])
		score1 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreLinearSpeeds, scorePolarization])
		score2 = scipy.stats.mstats.gmean([scoreProbabilityOfPresence, scoreLinearSpeeds, scorePolarization])
		return (score0, score1, score2)

	elif fitnessType == "PerfTrajobjEnvobj3":
		scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence, outOfBoundsScore = allScores
		score0 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence])
		score1 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreLinearSpeeds])
		score2 = scipy.stats.mstats.gmean([scoreLinearSpeeds, scorePolarization])
		score3 = scipy.stats.mstats.gmean([scorePolarization, scoreProbabilityOfPresence])
		score4 = scipy.stats.mstats.gmean([scoreInterindivDist, scoreProbabilityOfPresence])
		return (score0, score1, score2, score3, score4)

	elif fitnessType == "PerfAllobj":
		scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence, outOfBoundsScore = allScores
		#return (perf, scoreInterindivDist * outOfBoundsScore, scoreLinearSpeeds * outOfBoundsScore, scorePolarization * outOfBoundsScore, scoreProbabilityOfPresence * outOfBoundsScore)
		return (perf, scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence)

	elif fitnessType == "Envobj":
		scoreInterindivDist, scoreLinearSpeeds, scorePolarization, scoreProbabilityOfPresence, outOfBoundsScore = allScores
		return (scoreProbabilityOfPresence, outOfBoundsScore)

	else:
		return (0.0)




def uniform(low, up, size=None):
	try:
		return [np.random.uniform(a, b) for a, b in zip(low, up)]
	except TypeError:
		return [np.random.uniform(a, b) for a, b in zip([low] * size, [up] * size)]




########### MAIN ########### {{{1
if __name__ == "__main__":
	np.seterr(divide='ignore', invalid='ignore')

	from optparse import OptionParser
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-i", "--inputDir", dest = "inputDir", default = "",
			help = "Path of input data files")
	parser.add_option("-I", "--inputGenerationDir", dest = "inputGenerationDir", default = None,
			help = "Path of input data files")
	parser.add_option("-o", "--outputPrefix", dest = "outputPrefix", default = None,
			help = "Path prefix of output files")
	parser.add_option("-a", "--arenaFilename", dest = "arenaFilename", default = "FishModel/arenas/SetupLargeModelzoneBlack70x70v2.png",
			help = "Filename of arena map file")
	parser.add_option("-z", "--nbZones", dest = "nbZones", default = 6,
			help = "Number of zones")
	parser.add_option("-s", "--seed", dest = "seed", default = 42,
			help = "Random number generator seed")
	parser.add_option("-n", "--nbRuns", dest = "nbRuns", default = 10,
			help = "Number of runs per eval")
	parser.add_option("-S", "--nbSteps", dest = "nbSteps", default = 1800,
			help = "Number of steps per eval")
	parser.add_option("-g", "--nbGen", dest = "nbGen", default = 200,
			help = "Number of generations")
	parser.add_option("-L", "--lambda", dest = "nbIndiv", default = 40,
			help = "Number of individuals per generation")
	parser.add_option("-F", "--FPS", dest = "FPS", default = 15,
			help = "Frames per seconds")
	parser.add_option("--nbStepsGeneration", dest = "nbStepsGeneration", default = 26996,
			help = "Number of steps when generating simulation results on best individual")

	parser.add_option("-T", "--annType", dest = "annType", default = "ESN",
			help = "Type of ANN (MLP, ESN, SESN, CMLPWP)")

	parser.add_option("--fitnessType", dest = "fitnessType", default = "PerfGendivBehavdiv",
			help = "Type of ANN (PerfGendivBehavdiv, PerfTrajobjEnvobj)")

	parser.add_option("--annStepsPerSimStep", dest = "annStepsPerSimStep", default = 1,
			help = "Number of ANN steps per simulation step")

	parser.add_option("--nbSimulatedAgents", dest = "nbSimulatedAgents", default = 1,
			help = "Number of agents to simulate (others are taken from dataset)")


	(options, args) = parser.parse_args()

	np.random.seed(int(options.seed))
	annStepsPerSimStep = int(options.annStepsPerSimStep)
	fitnessType = options.fitnessType
	nbSimulatedAgents = int(options.nbSimulatedAgents)
	prefix = str(options.outputPrefix)
	f = FitnessCalibration(options.inputDir, mapFilename = options.arenaFilename, nbZones=int(options.nbZones), seed=int(options.seed), nbRuns=int(options.nbRuns), nbSteps=int(options.nbSteps), dt=1./float(options.FPS))
	inputGenerationDir = options.inputGenerationDir or options.inputDir
	fGeneration = FitnessCalibration(options.inputDir, mapFilename = options.arenaFilename, nbZones=int(options.nbZones), seed=int(options.seed), nbRuns=int(options.nbRuns), nbSteps=int(options.nbStepsGeneration), dt=1./float(options.FPS))

	if fitnessType == "PerfGendivBehavdiv":
		nbObjectives = 3
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0, 1.0))
	elif fitnessType == "PerfNovelty":
		nbObjectives = 2
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0))
	elif fitnessType == "PerfNovelty5":
		nbObjectives = 2
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0))
	elif fitnessType == "PerfNovelty20":
		nbObjectives = 2
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0))
	elif fitnessType == "PerfTrajobjEnvobj":
		nbObjectives = 3
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0, 1.0))
	elif fitnessType == "PerfTrajobjEnvobj2":
		nbObjectives = 3
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0, 1.0))
	elif fitnessType == "PerfTrajobjEnvobj3":
		nbObjectives = 5
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0, 1.0, 1.0, 1.0))
	elif fitnessType == "PerfAllobj":
		nbObjectives = 5
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0, 1.0, 1.0, 1.0))
	elif fitnessType == "Envobj":
		nbObjectives = 2
		creator.create("FitnessMax", base.Fitness, weights = (1.0, 1.0))
	else:
		raise ValueException("Unknown fitness type '%s'" % fitnessType)
	creator.create("Individual", list, fitness=creator.FitnessMax)


	# Create NN
	nbInputs = 22
	nbOutputs = 2
	nbHiddenNeurons = 100
	reservoirSize = nbHiddenNeurons
	if options.annType == "ESN":
		from ESN import *
		nnBase = ESN(nbInputs, nbOutputs, reservoirSize, 0.10, 0.88, allowInputToOutputDirectConnections=False, allowOutputSelfRecurrentConnections=False, allowInputToReservoirConnections=True, fixedInputToReservoirConnections=False, allowOutputToReservoirConnections=True)

	elif options.annType == "ESN2":
		from ESN import *
		nnBase = ESN(nbInputs, nbOutputs, reservoirSize, 0.10, 0.88, allowInputToOutputDirectConnections=True, allowOutputSelfRecurrentConnections=True, allowInputToReservoirConnections=True, fixedInputToReservoirConnections=False, allowOutputToReservoirConnections=True)

	elif options.annType == "MLP":
		from keras.models import Model, Input, Sequential
		from keras.layers import Dense, Activation
		#from evoKeras import *
		model = Sequential()
		model.add(Dense(units=nbHiddenNeurons, input_dim=nbInputs))
		model.add(Activation('tanh'))
		model.add(Dense(units=nbOutputs))
		model.add(Activation('tanh'))
		#model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) # XXX do NOT use compile in main thread
		#nbWeights = nbInputs * nbHiddenNeurons + nbHiddenNeurons + nbHiddenNeurons * nbOutputs + nbOutputs
		nnBase = EvolvableKerasModel(model, nbInputs, nbOutputs)

	elif options.annType == "MLP2":
		from keras.models import Model, Input, Sequential
		from keras.layers import Dense, Activation
		#from evoKeras import *
		model = Sequential()
		model.add(Dense(units=40, input_dim=nbInputs))
		model.add(Activation('tanh'))
		model.add(Dense(units=40))
		model.add(Activation('tanh'))
		model.add(Dense(units=40))
		model.add(Activation('tanh'))
		model.add(Dense(units=nbOutputs))
		model.add(Activation('tanh'))
		#model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy']) # XXX do NOT use compile in main thread
		#nbWeights = nbInputs * nbHiddenNeurons + nbHiddenNeurons + nbHiddenNeurons * nbOutputs + nbOutputs
		nnBase = EvolvableKerasModel(model, nbInputs, nbOutputs)

	elif options.annType == "MLP10":
		from keras.models import Model, Input, Sequential
		from keras.layers import Dense, Activation
		#from evoKeras import *
		model = Sequential()
		model.add(Dense(units=10, input_dim=nbInputs))
		model.add(Activation('tanh'))
		model.add(Dense(units=nbOutputs))
		model.add(Activation('tanh'))
		nnBase = EvolvableKerasModel(model, nbInputs, nbOutputs)


	elif options.annType == "CMLPWP":
		from keras.models import Model, Input, Sequential
		from keras.layers import Dense, Activation
		from keras.layers.merge import concatenate
		#from evoKeras import *
		inputLayer = Input(shape=(nbInputs,), trainable=False, bias_initializer='zeros', kernel_initializer='random_uniform')
		dLayers = []
		for i in range(5):
			l = Dense(10, activation='tanh')(inputLayer)
			dLayers.append(l)
			dLayers.append(l)
			dLayers.append(l)
		mergeLayer = concatenate(dLayers)
		d5 = Dense(50, activation='tanh')(mergeLayer)
		outputLayer = Dense(nbOutputs, activation='tanh')(d5)
		model = Model(inputs=inputLayer, outputs=outputLayer)
		nnBase = EvolvableKerasModel(model, nbInputs, nbOutputs)



	N = nnBase.getNbWeights()
	nbGen = int(options.nbGen)
	nbIndiv = int(options.nbIndiv)
	print("N = ", N)
	#CXPB = 0.8
	CXPB = 1.0
	mutpb = 0.2 # 1.0/N
	BOUND_LOW, BOUND_UP = -1.0, 1.0

	toolbox = base.Toolbox()
	toolbox.register("attr_float", uniform, BOUND_LOW, BOUND_UP, N)
	toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.attr_float)
	toolbox.register("population", tools.initRepeat, list, toolbox.individual)
	toolbox.register("evaluate", evalFit)
	toolbox.register("mate", tools.cxSimulatedBinaryBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0)
	toolbox.register("mutate", tools.mutPolynomialBounded, low=BOUND_LOW, up=BOUND_UP, eta=20.0, indpb=mutpb)
	toolbox.register("select", sel_nsga_iii)
	#toolbox.pop_size = nbIndiv
	#toolbox.max_gen = nbGen
	#toolbox.mut_prob = mutpb
	#toolbox.cross_prob = CXPB


	halloffame = tools.HallOfFame(1)
	stats = tools.Statistics(lambda ind: ind.fitness.values)
	stats.register("avg", np.mean, axis=0)
	stats.register("std", np.std, axis=0)
	stats.register("min", np.min, axis=0)
	stats.register("max", np.max, axis=0)
	logbook = tools.Logbook()
	logbook.header = "gen", "evals", "std", "min", "avg", "max"


	pool = multiprocessing.Pool()
	toolbox.register("map", pool.map)

	pop = toolbox.population(n=nbIndiv)

	# Compute mean genome values
	meanGenomeVals = np.zeros(N)
	for indiv in pop:
		meanGenomeVals += np.array(indiv)
	meanGenomeVals /= len(pop)

	if fitnessType == "PerfNovelty" or fitnessType == "PerfNovelty5" or fitnessType == "PerfNovelty20":
		# Init behavioural archive
		behaviouralArchive = []
		# Evaluate the individuals with an invalid fitness
		invalid_ind = [ind for ind in pop if not ind.fitness.valid]
		invalid_indDict = [ {'indiv': i, 'meanGenomeVals': meanGenomeVals, 'behaviouralArchive': behaviouralArchive} for i in invalid_ind ]
		fitnesses = toolbox.map(toolbox.evaluate, invalid_indDict)
		for ind, fit in zip(invalid_ind, fitnesses):
			ind.fitness.values = fit[0]
			behaviouralArchive.append(fit[1])
	else:
		# Evaluate the individuals with an invalid fitness
		invalid_ind = [ind for ind in pop if not ind.fitness.valid]
		invalid_indDict = [ {'indiv': i, 'meanGenomeVals': meanGenomeVals} for i in invalid_ind ]
		fitnesses = toolbox.map(toolbox.evaluate, invalid_indDict)
		for ind, fit in zip(invalid_ind, fitnesses):
			ind.fitness.values = fit

	# This is just to assign the crowding distance to the individuals
	# no actual selection is done
	pop = toolbox.select(pop, len(pop))

	record = stats.compile(pop)
	logbook.record(gen=0, evals=len(invalid_ind), **record)
	print(logbook.stream)

	fbest = np.ndarray((nbGen,nbObjectives))
	best = np.ndarray((nbGen,N))
	halloffame.update(pop)
	#print("halloffame: ", halloffame[0].fitness.values)
	sys.stdout.flush()

	# Begin the generational process
	for gen in range(1, nbGen):
		# Vary the population
		#joffspring = algorithms.varOr(pop, toolbox, nbIndiv, CXPB, mutpb)
		offspring = algorithms.varAnd(pop, toolbox, CXPB, mutpb)

		# Compute mean genome values
		meanGenomeVals = np.zeros(N)
		for indiv in pop:
			meanGenomeVals += np.array(indiv)
		meanGenomeVals /= len(pop)

		# Evaluate the individuals with an invalid fitness
		if fitnessType == "PerfNovelty" or fitnessType == "PerfNovelty5" or fitnessType == "PerfNovelty20":
			invalid_ind = [ind for ind in offspring]
			invalid_indDict = [ {'indiv': i, 'meanGenomeVals': meanGenomeVals, 'behaviouralArchive': behaviouralArchive} for i in invalid_ind ]
			fitnesses = toolbox.map(toolbox.evaluate, invalid_indDict)
			for ind, fit in zip(invalid_ind, fitnesses):
				ind.fitness.values = fit[0]
				behaviouralArchive.append(fit[1])
		else:
			invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
			invalid_indDict = [ {'indiv': i, 'meanGenomeVals': meanGenomeVals} for i in invalid_ind ]
			fitnesses = toolbox.map(toolbox.evaluate, invalid_indDict)
			for ind, fit in zip(invalid_ind, fitnesses):
				ind.fitness.values = fit

		# Select the next generation population
		pop = toolbox.select(pop + offspring, nbIndiv)
		record = stats.compile(pop)
		logbook.record(gen=gen, evals=len(invalid_ind), **record)
		print(logbook.stream)

		halloffame.update(pop)
		fbest[gen] = halloffame[0].fitness.values
		best[gen, :N] = halloffame[0]
		#print("halloffame: ", halloffame[0].fitness.values)
		sys.stdout.flush()

		# Empty cache
		while gc.collect() > 0:
			pass

	bestever = [best[-1]]
	fbestever = [fbest[-1,0]]
	for gen in range(1, nbGen):
		if fbest[gen,0] > fbestever[-1]:
			fbestever.append(fbest[gen,0])
			bestever.append(best[gen])
		else:
			fbestever.append(fbestever[-1])
			bestever.append(bestever[-1])

	outputDict = {'best': best, 'fbest': fbest, 'bestever': bestever, 'fbestever': fbestever}
	# Save the best individuals
	if prefix:
		pickle.dump(outputDict, open(os.path.join(prefix, "best.p"), "wb"))
	print("fbestever:", fbestever[-1])

	nn = nnBase.clone()
	nn.reinit()
	#res, allScores = f.fitness(bestever[-1], nn, annStepsPerSimStep, inputGenerationDir, nbSimulatedAgents, options.outputPrefix)
	res, allScores = fGeneration.fitness(bestever[-1], nn, annStepsPerSimStep, inputGenerationDir, nbSimulatedAgents, options.outputPrefix, False, "v5")
	print("bestever Scores: ", res, allScores)


	pool.close()
	pool.terminate()


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
