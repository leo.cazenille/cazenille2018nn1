#!/usr/bin/env python3

"""
Generate plots listing histograms of all relevant features + fitness score
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pydot
import seaborn as sns
sns.set_style("ticks")
from matplotlib.ticker import FormatStrFormatter


########### PLOTS FUNCTIONS ########### {{{1

def hist(outputname, data, domainX = [0., 1.], domainY = [0., 1.], labelX = "", labelY = "", color='grey'):
	fig = plt.figure(figsize=(6,6))
	#fig = plt.figure()
	ax = fig.add_subplot(111)

	nbBins = len(data)
	bins = np.linspace(domainX[0], domainX[1], nbBins+1)
	data = np.array(data)
	#data=data[~np.isnan(data)]
	#plt.hist(data, bins, weights = np.ones_like(data) / len(data), histtype='bar', alpha=0.5, linewidth=1, color=color)
	ax.bar(range(nbBins), data, alpha=0.5, linewidth=1, color=color)

	#ax.set_xlim(domainX)
	ax.set_xticks(range(0, nbBins, 5))
	ax.set_xticklabels([str(o) for o in bins[::5]])
	#for label in ax.get_xticklabels():
	#	label.set_visible(False)
	#for label in ax.get_xticklabels()[0::5]:
	#	label.set_visible(True)

	if len(labelX) > 0:
		plt.xlabel(labelX, fontsize=24)
	if len(labelY) > 0:
		plt.ylabel(labelY, fontsize=24)

	ax.tick_params(axis='both', which='major', labelsize=24)
	#for label in ax.get_yticklabels()[1::2]:
	#	label.set_visible(False)
	ax.set_ylim(domainY)
	ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
	#ax.ylim([0.,1.])
	#plt.axis('off')

	#maskednandata = np.ma.masked_array(data, np.isnan(data))
	#datamaskednanmean = np.round(np.mean(maskednandata), 3)
	#datamaskednanmedian = np.round(np.median(maskednandata),3)
	#datamaskednanstd = np.round(np.std(maskednandata), 3)
	#plt.text(0.06, 0.90, 'Median =' + str(datamaskednanmedian), ha='left', va='center', transform=ax.transAxes, fontsize=25)
	#plt.text(0.06, 0.80, 'Mean ' + r'$\pm$' + ' std : ' + str(datamaskednanmean) + r'$\pm$' + str(datamaskednanstd), ha='left', va='center', transform=ax.transAxes, color='0.30', fontsize=25)
	#plt.axvline(datamaskednanmedian, color='black', linestyle='dashed', linewidth=2)
	#plt.axvline(datamaskednanmean, color='0.50', linestyle='dashdot', linewidth=2)

	sns.despine()
	plt.tight_layout()
	#plt.subplots_adjust(left=0.01, right=0.99, top=0.99, bottom=0.01)
	fig.savefig(outputname)
	plt.close(fig)


def circularHist(outputname, data, nbBins = 10, domain = [0.,1.], color='grey'):
	fig = plt.figure(figsize=(6,6))
	ax = fig.add_subplot(111, polar=True)

	#data = np.array(data)
	#data = data[~np.isnan(data)]
	theta = np.linspace(domain[0], domain[1], nbBins, endpoint=False)
	width = (domain[1] - domain[0]) / nbBins
	bars = ax.bar(theta, data, width=width, bottom=0.0, linewidth=1, color=color, edgecolor='k')

	ax.tick_params(axis='both', which='major', labelsize=25)
	for label in ax.get_xticklabels()[1::2]:
		label.set_visible(False)
	for label in ax.get_yticklabels()[1::2]:
		label.set_visible(False)
	#ax.set_ylim(domain)
	ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
	##ax.ylim([0.,1.])
	##plt.axis('off')

	for r, bar in zip(data, bars):
		bar.set_alpha(0.5)

	#sns.despine()
	plt.tight_layout()
	fig.savefig(outputname)
	plt.close(fig)


########### MAIN ########### {{{1
if __name__ == "__main__":
	from optparse import OptionParser
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-i", "--inputDir", dest = "inputDir", default = "",
			help = "Path of input data files")
#	parser.add_option("-I", "--bestIndividualFilename", dest = "bestIndividualFilename", default = "",
#			help = "Path of picked model parameter")
	parser.add_option("-o", "--outputDir", dest = "outputDir", default = "plots/",
			help = "Directory of output plots")
	parser.add_option("-O", "--outputFilenameSuffix", dest = "outputFilenameSuffix", help = "Output Filename Suffix")
	parser.add_option("-a", "--arenaFilename", dest = "arenaFilename", default = "FishModel/arenas/SetupLargeModelzoneBlack70x70v2.png",
			help = "Filename of arena map file")
	parser.add_option("-z", "--nbZones", dest = "nbZones", default = 6,
			help = "Number of zones")
	parser.add_option("-F", "--FPS", dest = "FPS", default = 15,
			help = "Frames per seconds")
#	parser.add_option("-C", "--zonesLabels", dest = "zonesLabels", default = "Void,Corridor,Center of Room 1,Center of Room 2,Walls of Room 1,Walls of Room 2",
#			help = "Labels of all zones, separated by ','")
#	parser.add_option("-s", "--seed", dest = "seed", default = 42,
#			help = "Random number generator seed")
#	parser.add_option("-S", "--nbSteps", dest = "nbSteps", default = 1800,
#			help = "Number of steps per eval")
#	parser.add_option("-n", "--nbRuns", dest = "nbRuns", default = 10,
#			help = "Number of runs per eval")
	parser.add_option("-M", "--color", dest = "color", help = "color", default='blue')

	(options, args) = parser.parse_args()

	nbZones = int(options.nbZones)
	#zonesNames = options.zonesLabels.split(",")
	#zonesLabels = ["%s\n%.2f%s" % (zonesNames[i], zonesOccupation[i] * 100., '%') for i in range(nbZones)]
	#zonesLabels = zonesLabels[1:]

	from fitness import *
	f = FitnessCalibration(options.inputDir, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, dt=1./float(options.FPS))

	linearSpeedsInZones = f.referenceFSet.getLinearSpeedInZones()
	print("linearSpeedsInZones: ", linearSpeedsInZones[1:])
	for z in range(1,len(linearSpeedsInZones)):
		data = linearSpeedsInZones[z]
		hist(os.path.join(options.outputDir, "hist-zone%i-linearSpeedInZones-%s" % (z, options.outputFilenameSuffix)), data, f.linearSpeedsInZonesDomain, [0., 0.20], "Linear Speed (m/s)", "Frequency", options.color)

	angularSpeedInZones = f.referenceFSet.getAngularSpeedInZones()
	print("angularSpeedInZones: ", angularSpeedInZones[1:])
	for z in range(1,len(angularSpeedInZones)):
		data = angularSpeedInZones[z]
		circularHist(os.path.join(options.outputDir, "hist-zone%i-angularSpeedInZones-%s" % (z, options.outputFilenameSuffix)), data, f.angularSpeedsInZonesNbBin, f.angularSpeedsInZonesDomain, options.color)

	interindivDistInZones = f.referenceFSet.getInterindivDistInZones()
	print("interindivDistInZones: ", interindivDistInZones[1:])
	for z in range(1,len(interindivDistInZones)):
		data = interindivDistInZones[z]
		#hist(os.path.join(options.outputDir, "hist-zone%i-interindivDistInZones-%s" % (z, options.outputFilenameSuffix)), data, 12, (0.0, 0.6), options.color)
		hist(os.path.join(options.outputDir, "hist-zone%i-interindivDistInZones-%s" % (z, options.outputFilenameSuffix)), data, f.interindivDistInZonesDomain, [0., 0.30], "Inter-indiv. Dist. (m)", "Frequency", options.color)

	polarizationInZones = f.referenceFSet.getPolarizationInZones()
	print("polarizationInZones: ", polarizationInZones)
	for z in range(0,len(polarizationInZones)):
		data = polarizationInZones[z]
		hist(os.path.join(options.outputDir, "hist-zone%i-polarizationInZones-%s" % (z+1, options.outputFilenameSuffix)), data, f.polarizationInZonesDomain, [0., 0.20], "Polarisation", "Frequency", options.color)

	# Distance to walls
	distToWallsByZones = f.referenceFSet.getDistToWallsByZones()
	print("distToWallsByZones: ", distToWallsByZones[1:])
	for z in range(1,len(distToWallsByZones)):
		data = distToWallsByZones[z]
		hist(os.path.join(options.outputDir, "hist-zone%i-distToWallsByZones-%s" % (z, options.outputFilenameSuffix)), data, f.distToWallsByZonesDomain, [0., 0.25], "Distance to nearest wall (m)", "Frequency", options.color)


# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
