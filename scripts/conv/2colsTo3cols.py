#!/usr/bin/env python


import os
import sys
from optparse import OptionParser
import numpy as np


parser = OptionParser()
parser.add_option("-i", "--inputFilename", dest = "inputFilename", help = ".txt file to analyse")
parser.add_option("-o", "--outputFilename", dest = "outputFilename", help = ".txt file to analyse")
parser.add_option("-r", "--rotation", dest = "rotation", help = "rotation: 0, 90, 180, 270", default=0)
(options, args) = parser.parse_args()

with open(options.inputFilename, "r") as ifile:
	brut_data = np.loadtxt(ifile, skiprows=1)

nbAgents = (brut_data.shape[1] - 1) / 2

header = "TimeStep"
for i in range(nbAgents):
	header += " Fish" + str(i) + "X"
	header += " Fish" + str(i) + "Y"
	header += " Fish" + str(i) + "Direction"
	header += "\t"

data = np.empty(shape=(brut_data.shape[0], 1 + nbAgents * 3))
data[:,1::3] = brut_data[:,1::2]
data[:,2::3] = brut_data[:,2::2]
data[:,3::3] = np.nan
data[:,0] = brut_data[:,0]

rotation = int(options.rotation)

if rotation == 90:
	data[:, 1 : 3 * nbAgents : 3] = 1.0 - data[:, 1 : 3 * nbAgents : 3]
elif rotation == 180:
	data[:, 1 : 3 * nbAgents : 3] = 1.0 - data[:, 1 : 3 * nbAgents : 3]
	data[:, 2 : 3 * nbAgents + 1 : 3] = 1.0 - data[:, 2 : 3 * nbAgents + 1 : 3]
elif rotation == 270:
	data[:, 2 : 3 * nbAgents + 1 : 3] = 1.0 - data[:, 2 : 3 * nbAgents + 1 : 3]


np.savetxt(options.outputFilename, data, fmt='%1.3f', header = header)

