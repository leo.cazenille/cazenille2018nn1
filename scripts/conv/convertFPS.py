#!/usr/bin/env python3


import os
import sys
from optparse import OptionParser
import numpy as np
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d


parser = OptionParser()
parser.add_option("-i", "--inputFilename", dest = "inputFilename", help = ".txt file to analyse")
parser.add_option("-o", "--outputFilename", dest = "outputFilename", help = ".txt file to analyse")
parser.add_option("--targetFPS", dest = "targetFPS", default = 3.0, help = "Target fps")
(options, args) = parser.parse_args()


with open(options.inputFilename, "r") as ifile:
	brut_data = np.loadtxt(ifile, skiprows=1)

nbAgents = int((brut_data.shape[1]) / 2)

header = "TimeStep"
for i in range(nbAgents):
	header += " Agent" + str(i) + "X"
	header += " Agent" + str(i) + "Y"
#	header += " Agent" + str(i) + "Direction"
	header += "\t"

dataTime = brut_data[:,0]
dataX = savgol_filter(brut_data[:,1::2], 5, 2, axis=0)
dataY = savgol_filter(brut_data[:,2::2], 5, 2, axis=0)
print(dataTime.shape, dataX.shape, dataY.shape)

interpDataX = [interp1d(dataTime, dataX[:,i], kind="linear") for i in range(nbAgents) ]
interpDataY = [interp1d(dataTime, dataY[:,i], kind="linear") for i in range(nbAgents) ]

newDataTime = np.arange(dataTime[0], dataTime[-1], 1./float(options.targetFPS))
newDataX = np.array([interpDataX[i](newDataTime) for i in range(nbAgents)]).T
newDataY = np.array([interpDataY[i](newDataTime) for i in range(nbAgents)]).T
print(newDataTime.shape, newDataX.shape, newDataY.shape)

data = np.empty(shape=(newDataTime.shape[0], 1 + nbAgents * 2))
data[:,0] = newDataTime
data[:,1::2] = newDataX
data[:,2::2] = newDataY
np.savetxt(options.outputFilename, data, fmt='%1.3f', header = header)

#data = np.empty(shape=(brut_data.shape[0]//5+1, 1 + nbAgents * 2))
#data[:,1::2] = brut_data[::5,1::2]
#data[:,2::2] = brut_data[::5,2::2]
#data[:,0] = brut_data[::5, 0]
#
#np.savetxt(options.outputFilename, data, fmt='%1.3f', header = header)

