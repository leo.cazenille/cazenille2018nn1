#!/usr/bin/env python


import os
import sys
from optparse import OptionParser
import numpy as np


parser = OptionParser()
parser.add_option("-i", "--inputFilename", dest = "inputFilename", help = ".txt file to analyse")
parser.add_option("-o", "--outputFilename", dest = "outputFilename", help = ".txt file to analyse")
(options, args) = parser.parse_args()


with open(options.inputFilename, "r") as ifile:
	brut_data = np.loadtxt(ifile, skiprows=1)

nbAgents = int((brut_data.shape[1]) / 2)

header = "TimeStep"
for i in range(nbAgents):
	header += " Agent" + str(i) + "X"
	header += " Agent" + str(i) + "Y"
#	header += " Agent" + str(i) + "Direction"
	header += "\t"

data = np.empty(shape=(brut_data.shape[0]//5+1, 1 + nbAgents * 2))
data[:,1::2] = brut_data[::5,1::2]
data[:,2::2] = brut_data[::5,2::2]
data[:,0] = brut_data[::5, 0]

np.savetxt(options.outputFilename, data, fmt='%1.3f', header = header)

