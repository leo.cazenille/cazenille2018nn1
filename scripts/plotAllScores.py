#!/usr/bin/env python3

"""
Compute scores between fish-only expes
"""

__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"

########### IMPORTS AND BASE GLOBALS ########### {{{1

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab
from fitness import *
import os
from optparse import OptionParser
import scipy.constants
import copy

import seaborn as sns
sns.set_style("ticks")


def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)


########### MAIN ########### {{{1
if __name__ == "__main__":
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("-o", "--outputDir", dest = "outputDir", default = "scores", help = "Directory of output files")
	parser.add_option("-a", "--arenaFilename", dest = "arenaFilename", default = "FishModel/arenas/SetupLargeModelzoneBlack70x70v2.png", help = "Filename of arena map file")
	parser.add_option("-z", "--nbZones", dest = "nbZones", default = 2, help = "Number of zones")
	parser.add_option("-k", "--skip", dest = "skip", default = 0, help = "")
	parser.add_option("-F", "--nbFish", dest = "nbFish", default = 5, help = "Number of fish")
	parser.add_option("--byTrajFile", action="store_true", dest = "byTrajFile", default = False, help = "Analyse each trajectory file individually") # byTrajFile=True:bestScores, byTrajFile=False:allScores
	parser.add_option("--outputSuffix", dest = "outputSuffix", default = "Scores", help = "Suffix of output plots filenames.")
	parser.add_option("--refPath", dest = "refPath", default = "", help = "Path of reference data files")
	parser.add_option("--refLabel", dest = "refLabel", default = "Control", help = "Label of reference data")
	#parser.add_option("-R", "--nbRobots", dest = "nbRobots", default = 1, help = "Number of robots")


	(options, args) = parser.parse_args()

	nbZones = int(options.nbZones)
	nbFish = int(options.nbFish)
	byTrajFile = bool(options.byTrajFile)
	outputSuffix = options.outputSuffix

	labels = args[0::3]
	paths = args[1::3]
	dataNbAgents = args[2::3]
	refLabel = options.refLabel #labels[0]
	refPath = options.refPath #paths[0]


	fRef = FitnessCalibration(refPath, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)


	dirs = []
	dirsPath = []
	scoresInterindivDist = []
	scoresLinearSpeeds = []
	scoresPolarity = []
	scoresProbabilityOfPresence = []
	scoresOutOfBoundsScore = []
	scores = []


	for lab, path in zip(labels, paths):
		print("Computing scores for expe '%s' on path '%s'" % (lab, path))
		scoresInterindivDist.append([])
		scoresLinearSpeeds.append([])
		scoresPolarity.append([])
		scoresProbabilityOfPresence.append([])
		scoresOutOfBoundsScore.append([])
		scores.append([])

		d = next(os.walk(path))

		if byTrajFile:
			d_ = d[2]
			dPath = [os.path.join(path, o) for o in d_ if o.endswith(".txt")]

			if not len(dPath):
				d_ = copy.deepcopy(d[1])
				d_.sort(key=int)
				dPath = [os.path.join(path, o) for o in d_]
				dirs.append(d_)
				dirsPath.append(dPath)
				for dExpeIndex in range(len(dPath)):
					dExpe = dPath[dExpeIndex]
					filesList = next(os.walk(dExpe))[2]
					filesPathList = [os.path.join(dExpe, o) for o in filesList if o.endswith(".txt")]
					for fileIndex in range(len(filesPathList)):
						currentFilePath = filesPathList[fileIndex]
						f2 = FitnessCalibration(currentFilePath, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
						individualScore, allScores = fRef.computeScore(f2.referenceFSet, False, "v5")
						scoreInterindivDist, scoreLinearSpeeds, scorePolarity, scoreProbabilityOfPresence, outOfBoundsScore = allScores
						scoresInterindivDist[-1].append(scoreInterindivDist)
						scoresLinearSpeeds[-1].append(scoreLinearSpeeds)
						scoresPolarity[-1].append(scorePolarity)
						scoresProbabilityOfPresence[-1].append(scoreProbabilityOfPresence)
						scoresOutOfBoundsScore[-1].append(outOfBoundsScore)
						scores[-1].append(individualScore)
						print("DEBUG1: ", currentFilePath, individualScore, allScores)

			else:
				dirs.append(d_)
				dirsPath.append(dPath)
				for dExpeIndex in range(len(dPath)):
					dExpe = dPath[dExpeIndex]
					f2 = FitnessCalibration(dExpe, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
					individualScore, allScores = fRef.computeScore(f2.referenceFSet, False, "v5")
					scoreInterindivDist, scoreLinearSpeeds, scorePolarity, scoreProbabilityOfPresence, outOfBoundsScore = allScores
					scoresInterindivDist[-1].append(scoreInterindivDist)
					scoresLinearSpeeds[-1].append(scoreLinearSpeeds)
					scoresPolarity[-1].append(scorePolarity)
					scoresProbabilityOfPresence[-1].append(scoreProbabilityOfPresence)
					scoresOutOfBoundsScore[-1].append(outOfBoundsScore)
					scores[-1].append(individualScore)
					print("DEBUG2: ", dExpe, individualScore, allScores)

		else:
			d_ = d[1]
			dPath = [os.path.join(path, o) for o in d_]
			print("DEBUG3: ", dPath)
			dirsPath.append(dPath)
			for dExpeIndex in range(len(dPath)):
				dExpe = dPath[dExpeIndex]
				if not len(next(os.walk(dExpe))[2]):
					print("Directory '%s' is empty !" % dExpe)
					continue # Directory empty
				f2 = FitnessCalibration(dExpe, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
				individualScore, allScores = fRef.computeScore(f2.referenceFSet, False, "v5")
				scoreInterindivDist, scoreLinearSpeeds, scorePolarity, scoreProbabilityOfPresence, outOfBoundsScore = allScores
				scoresInterindivDist[-1].append(scoreInterindivDist)
				scoresLinearSpeeds[-1].append(scoreLinearSpeeds)
				scoresPolarity[-1].append(scorePolarity)
				scoresProbabilityOfPresence[-1].append(scoreProbabilityOfPresence)
				scoresOutOfBoundsScore[-1].append(outOfBoundsScore)
				scores[-1].append(individualScore)
				print("DEBUG4: ", dExpe, individualScore, allScores)



#	scores = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresInterindivDist = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresLinearSpeeds = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresPolarity = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]
#	scoresProbabilityOfPresence = [np.random.uniform(0.4, 1.0, 5) for i in range(len(labels))]




#	plotNames = ['Biomimetism', 'InterindivDist', 'LinearSpeeds', 'Polarity', 'Polarisation', 'DistToWalls', 'ProbabilityOfPresence']
#	plotTypes = ['Biomimetism\nScore', 'Inter-individual\nScore', 'Linear speed\nScore', 'Angular speed\nScore', 'Polarisation\nScore', 'Nearest wall\nScore', 'Probability\nof presence\nScore']
#	scoresList = [scores, scoresInterindivDist, scoresLinearSpeeds, scoresAngularSpeeds, scoresPolarisation, scoresDistToWalls, scoresProbabilityOfPresence]
	plotNames = ['Biomimetism', 'InterindivDist', 'LinearSpeeds', 'Polarity', 'ProbabilityOfPresence']
	plotTypes = ["Biomimetism Score", "Inter-individual Score", "Linear speed Score", "Polarity Score", "Probability of\npresence Score"]
	#plotDomain = [(0.5, 0.9), (0.3, 0.9), (0.6, 1.0), (0.5, 1.0), (0.3, 0.9)]
	plotDomain = [(0.0, 0.9), (0.0, 0.9), (0.0, 1.0), (0.0, 1.0), (0.0, 0.9)]
	scoresList = [scores, scoresInterindivDist, scoresLinearSpeeds, scoresPolarity, scoresProbabilityOfPresence]

	labels2 = [a.replace("\\n", "\n").replace("\"", "").replace("~", " ") for a in labels]

	labelTypes = list(sorted(list(set(labels2))))
	if 'Control' in labelTypes:
		labelTypes.remove('Control')
	print("DEBUG5:", labelTypes)

	plt.style.use('grayscale')
	for plotN, plotT, plotD, s in zip(plotNames, plotTypes, plotDomain, scoresList):
		fig = plt.figure(figsize=(4.*scipy.constants.golden * 1.5, 4.2))
		ax = fig.add_subplot(111)
		fig.subplots_adjust(bottom=0.3)

		ax.set_ylim(plotD) #([0.3, 1.0])
		plt.ylabel(plotT, fontsize=18)
		yticksDomain = [int(i * 10) for i in plotD]
		yticks = [float(i) / 10. for i in list(range(yticksDomain[0], yticksDomain[1] + 1))]
		yticksTop = plotD[1]
		plt.yticks(yticks[::2], fontsize=18)

		bp = plt.boxplot(s, positions=np.array(range(len(labels2))), sym='', widths=0.6)
		plt.xticks(np.array(range(len(labels2))), labels2, fontsize=16, rotation=30, ha='right')

		agentText = (lambda i: "%s agent" % i if i == '1' else "%s agents" % i)
		uniqueNbAgents = list(sorted(list(set(dataNbAgents))))
		if '0' in uniqueNbAgents:
			uniqueNbAgents.remove('0')
		#print("DEBUG3:", uniqueNbAgents, dataNbAgents)

		if byTrajFile:
			for i in range(len(uniqueNbAgents)):
				#ax.text((i+1) * len(labelTypes) - (len(labelTypes) - 1) + len(labelTypes) / 2, 0.99, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
				ax.text((i + 0.5) * len(labelTypes) + 0.5, yticksTop + 0.01, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
				#plt.hlines(0.99, (i+1) * len(labelTypes) - (len(labelTypes) - 1), (i+1) * len(labelTypes))
				plt.hlines(yticksTop - 0.01, (i) * len(labelTypes) + 0.6, (i+1) * len(labelTypes) + 0.4)
				plt.axvline(x=(i) * len(labelTypes) + 0.5, ls=':')
		else:
			for i in range(len(uniqueNbAgents)):
				#ax.text((i+1) * len(labelTypes) - (len(labelTypes) - 1) + len(labelTypes) / 2, 0.99, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
				ax.text((i + 0.5) * len(labelTypes) - 0.4, yticksTop + 0.01, agentText(uniqueNbAgents[i]), fontsize=16, ha='center')
				#plt.hlines(0.99, (i) * len(labelTypes) - 0.3, (i+1) * len(labelTypes) - 0.4)
				plt.hlines(yticksTop - 0.01, (i) * len(labelTypes) - 0.25, (i+1) * len(labelTypes) - 0.75)
				if i != 0:
					plt.axvline(x=(i) * len(labelTypes) - 0.5, ls=':')

		sns.despine()
		plt.tight_layout(rect=[0, 0, 1.0, 0.95])
		fig.savefig(os.path.join(options.outputDir, "%s-%s.pdf" % (plotN, outputSuffix)))
		plt.close(fig)









########### MAIN ########### {{{1
#if __name__ == "__main__":
def oldmain():
	usage = "%prog [command] [options]"
	parser = OptionParser(usage=usage)

	parser.add_option("--inputDir0", dest = "inputDir0", default = "", help = "Path of fish-only input data files")
	parser.add_option("--inputDir1", dest = "inputDir1", default = "", help = "Path of Ctrl1-controled agents input data files")
	parser.add_option("--inputDir2", dest = "inputDir2", default = "", help = "Path of Ctrl2-controled agents input data files")
	parser.add_option("--inputDir3", dest = "inputDir3", default = "", help = "Path of Ctrl3-controled agents input data files")
	#parser.add_option("--fishReferenceInputDir", dest = "fishReferenceInputDir", default = "", help = "Path of fish-only input data files")
	parser.add_option("-o", "--outputDir", dest = "outputDir", default = "scores", help = "Directory of output files")
	parser.add_option("-a", "--arenaFilename", dest = "arenaFilename", default = "FishModel/arenas/SetupLargeModelzoneBlack70x70v2.png", help = "Filename of arena map file")
	parser.add_option("-z", "--nbZones", dest = "nbZones", default = 6, help = "Number of zones")
	parser.add_option("-k", "--skip", dest = "skip", default = 0, help = "")
	parser.add_option("-F", "--nbFish", dest = "nbFish", default = 4, help = "Number of fish")
	#parser.add_option("-R", "--nbRobots", dest = "nbRobots", default = 1, help = "Number of robots")

#	parser.add_option("-C", "--zonesLabels", dest = "zonesLabels", default = "Void,Corridor,Center of Room 1,Center of Room 2,Walls of Room 1,Walls of Room 2",
#			help = "Labels of all zones, separated by ','")
#	parser.add_option("-s", "--seed", dest = "seed", default = 42,
#			help = "Random number generator seed")
#	parser.add_option("-S", "--nbSteps", dest = "nbSteps", default = 1800,
#			help = "Number of steps per eval")
#	parser.add_option("-n", "--nbRuns", dest = "nbRuns", default = 10,
#			help = "Number of runs per eval")

	(options, args) = parser.parse_args()

	nbZones = int(options.nbZones)
	nbFish = int(options.nbFish)


	#fRef = FitnessCalibration(options.fishReferenceInputDir, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
	fRef = FitnessCalibration(options.inputDir0, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)



	dirsExpesFish = next(os.walk(options.inputDir0))[2]
	#dirsExpesFish.sort(key=int)
	dirsExpesFishPath = [os.path.join(options.inputDir0, o) for o in dirsExpesFish if o.endswith(".txt")]
	print(dirsExpesFishPath)

	scoresInterindivDistInZonesFish = []
	scoresLinearSpeedsFish = []
	scoresAngularSpeedsFish = []
	scoresPolarisationFish = []
	scoresDistToWallsFish = []
	scoresFish = []

	for dExpeIndex2 in range(len(dirsExpesFishPath)):
		dExpe2 = dirsExpesFishPath[dExpeIndex2]
		f2 = FitnessCalibration(dExpe2, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
		individualScore, allScores = fRef.computeScore(f2.referenceFSet, False)

		interindivDistInZones, linearSpeedsInZones, angularSpeedsInZones, polarisation, distToWallsByZones = allScores
		scoresInterindivDistInZonesFish.append(interindivDistInZones)
		scoresLinearSpeedsFish.append(linearSpeedsInZones)
		scoresAngularSpeedsFish.append(angularSpeedsInZones)
		scoresPolarisationFish.append(polarisation)
		scoresDistToWallsFish.append(distToWallsByZones)
		scoresFish.append(individualScore)

	print("scoresFish", scoresFish)
	print("scoresInterindivDistInZonesFish", scoresInterindivDistInZonesFish)
	print("scoresLinearSpeedsFish", scoresLinearSpeedsFish)
	print("scoresAngularSpeedsFish", scoresAngularSpeedsFish)
	print("scoresPolarisationFish", scoresPolarisationFish)
	print("scoresDistToWallsFish", scoresDistToWallsFish)



	dirsExpesMLP = next(os.walk(options.inputDir1))[1]
	dirsExpesMLP.sort(key=int)
	dirsExpesMLPPath = [os.path.join(options.inputDir1, o) for o in dirsExpesMLP]
	print(dirsExpesMLPPath)

	scoresInterindivDistInZonesMLP = []
	scoresLinearSpeedsMLP = []
	scoresAngularSpeedsMLP = []
	scoresPolarisationMLP = []
	scoresDistToWallsMLP = []
	scoresMLP = []

	for dExpeIndex2 in range(len(dirsExpesMLPPath)):
		dExpe2 = dirsExpesMLPPath[dExpeIndex2]

		filesList = next(os.walk(dExpe2))[2]
		filesPathList = [os.path.join(dExpe2, o) for o in filesList if o.endswith(".txt")]

		for fileIndex in range(len(filesPathList)):
			currentFilePath = filesPathList[fileIndex]
			f2 = FitnessCalibration(currentFilePath, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
			individualScore, allScores = fRef.computeScore(f2.referenceFSet, False)

			interindivDistInZones, linearSpeedsInZones, angularSpeedsInZones, polarisation, distToWallsByZones = allScores
			scoresInterindivDistInZonesMLP.append(interindivDistInZones)
			scoresLinearSpeedsMLP.append(linearSpeedsInZones)
			scoresAngularSpeedsMLP.append(angularSpeedsInZones)
			scoresPolarisationMLP.append(polarisation)
			scoresDistToWallsMLP.append(distToWallsByZones)
			scoresMLP.append(individualScore)

	print("scoresMLP", scoresMLP)
	print("scoresInterindivDistInZonesMLP", scoresInterindivDistInZonesMLP)
	print("scoresLinearSpeedsMLP", scoresLinearSpeedsMLP)
	print("scoresAngularSpeedsMLP", scoresAngularSpeedsMLP)
	print("scoresPolarisationMLP", scoresPolarisationMLP)
	print("scoresDistToWallsMLP", scoresDistToWallsMLP)



	dirsExpesNSGA = next(os.walk(options.inputDir2))[1]
	dirsExpesNSGA.sort(key=int)
	dirsExpesNSGAPath = [os.path.join(options.inputDir2, o) for o in dirsExpesNSGA]
	print(dirsExpesNSGAPath)

	scoresInterindivDistInZonesNSGA = []
	scoresLinearSpeedsNSGA = []
	scoresAngularSpeedsNSGA = []
	scoresPolarisationNSGA = []
	scoresDistToWallsNSGA = []
	scoresNSGA = []

	for dExpeIndex2 in range(len(dirsExpesNSGAPath)):
		dExpe2 = dirsExpesNSGAPath[dExpeIndex2]

		filesList = next(os.walk(dExpe2))[2]
		filesPathList = [os.path.join(dExpe2, o) for o in filesList if o.endswith(".txt")]

		for fileIndex in range(len(filesPathList)):
			currentFilePath = filesPathList[fileIndex]

			f2 = FitnessCalibration(currentFilePath, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
			individualScore, allScores = fRef.computeScore(f2.referenceFSet, False)

			interindivDistInZones, linearSpeedsInZones, angularSpeedsInZones, polarisation, distToWallsByZones = allScores
			scoresInterindivDistInZonesNSGA.append(interindivDistInZones)
			scoresLinearSpeedsNSGA.append(linearSpeedsInZones)
			scoresAngularSpeedsNSGA.append(angularSpeedsInZones)
			scoresPolarisationNSGA.append(polarisation)
			scoresDistToWallsNSGA.append(distToWallsByZones)
			scoresNSGA.append(individualScore)

	print("scoresNSGA", scoresNSGA)
	print("scoresInterindivDistInZonesNSGA", scoresInterindivDistInZonesNSGA)
	print("scoresLinearSpeedsNSGA", scoresLinearSpeedsNSGA)
	print("scoresAngularSpeedsNSGA", scoresAngularSpeedsNSGA)
	print("scoresPolarisationNSGA", scoresPolarisationNSGA)
	print("scoresDistToWallsNSGA", scoresDistToWallsNSGA)



	dirsExpesESN = next(os.walk(options.inputDir3))[1]
	dirsExpesESN.sort(key=int)
	dirsExpesESNPath = [os.path.join(options.inputDir3, o) for o in dirsExpesESN]
	print(dirsExpesESNPath)

	scoresInterindivDistInZonesESN = []
	scoresLinearSpeedsESN = []
	scoresAngularSpeedsESN = []
	scoresPolarisationESN = []
	scoresDistToWallsESN = []
	scoresESN = []

	for dExpeIndex2 in range(len(dirsExpesESNPath)):
		dExpe2 = dirsExpesESNPath[dExpeIndex2]

		filesList = next(os.walk(dExpe2))[2]
		filesPathList = [os.path.join(dExpe2, o) for o in filesList if o.endswith(".txt")]

		for fileIndex in range(len(filesPathList)):
			currentFilePath = filesPathList[fileIndex]

			f2 = FitnessCalibration(currentFilePath, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
			individualScore, allScores = fRef.computeScore(f2.referenceFSet, False)

			interindivDistInZones, linearSpeedsInZones, angularSpeedsInZones, polarisation, distToWallsByZones = allScores
			scoresInterindivDistInZonesESN.append(interindivDistInZones)
			scoresLinearSpeedsESN.append(linearSpeedsInZones)
			scoresAngularSpeedsESN.append(angularSpeedsInZones)
			scoresPolarisationESN.append(polarisation)
			scoresDistToWallsESN.append(distToWallsByZones)
			scoresESN.append(individualScore)

	print("scoresESN", scoresESN)
	print("scoresInterindivDistInZonesESN", scoresInterindivDistInZonesESN)
	print("scoresLinearSpeedsESN", scoresLinearSpeedsESN)
	print("scoresAngularSpeedsESN", scoresAngularSpeedsESN)
	print("scoresPolarisationESN", scoresPolarisationESN)
	print("scoresDistToWallsESN", scoresDistToWallsESN)


#	dirsExpesFish = next(os.walk(options.inputDir0))[1]
#	dirsExpesFish.sort(key=int)
#	dirsExpesFishPath = [os.path.join(options.inputDir0, o) for o in dirsExpesFish]
#	print(dirsExpesFishPath)
#
#	scoresInterindivDistInZonesFish = []
#	scoresLinearSpeedsFish = []
#	scoresAngularSpeedsFish = []
#	scoresDistToWallsFish = []
#	scoresFish = []
#
#	for dExpeIndex1 in range(len(dirsExpesFishPath)):
#		dExpe1 = dirsExpesFishPath[dExpeIndex1]
#		fRef = FitnessCalibration(dExpe1, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
#
#		for dExpeIndex2 in range(dExpeIndex1 + 1, len(dirsExpesFishPath)):
#			dExpe2 = dirsExpesFishPath[dExpeIndex2]
#
#			f2 = FitnessCalibration(dExpe2, mapFilename = options.arenaFilename, nbZones=nbZones, seed=42, nbRuns=1, nbSteps=1, skip=int(options.skip), nbFishes=nbFish)
#			individualScore, allScores = fRef.computeScore(f2.referenceFSet, True)
#
#			interindivDistInZones, linearSpeedsInZones, angularSpeedsInZones, distToWallsByZones = allScores
#			scoresInterindivDistInZonesFish.append(interindivDistInZones)
#			scoresLinearSpeedsFish.append(linearSpeedsInZones)
#			scoresAngularSpeedsFish.append(angularSpeedsInZones)
#			scoresDistToWallsFish.append(distToWallsByZones)
#			scoresFish.append(individualScore)
#
#	print("scoresFish", scoresFish)
#	print("scoresInterindivDistInZonesFish", scoresInterindivDistInZonesFish)
#	print("scoresLinearSpeedsFish", scoresLinearSpeedsFish)
#	print("scoresAngularSpeedsFish", scoresAngularSpeedsFish)
#	print("scoresDistToWallsFish", scoresDistToWallsFish)
#


	allFish1 = [scoresFish, scoresInterindivDistInZonesFish, scoresLinearSpeedsFish]
	allMLP1 = [scoresMLP, scoresInterindivDistInZonesMLP, scoresLinearSpeedsMLP]
	allNSGA1 = [scoresNSGA, scoresInterindivDistInZonesNSGA, scoresLinearSpeedsNSGA]
	allESN1 = [scoresESN, scoresInterindivDistInZonesESN, scoresLinearSpeedsESN]
	allFish2 = [scoresAngularSpeedsFish, scoresPolarisationFish, scoresDistToWallsFish]
	allMLP2 = [scoresAngularSpeedsMLP, scoresPolarisationMLP, scoresDistToWallsMLP]
	allNSGA2 = [scoresAngularSpeedsNSGA, scoresPolarisationNSGA, scoresDistToWallsNSGA]
	allESN2 = [scoresAngularSpeedsESN, scoresPolarisationESN, scoresDistToWallsESN]

#	allFish1 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allMLP1 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allNSGA1 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allESN1 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allFish2 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allMLP2 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allNSGA2 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]
#	allESN2 = [np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5), np.random.uniform(0.5, 1.0, 5)]


	plt.style.use('grayscale')
	fig = plt.figure(figsize=(6.*scipy.constants.golden * 2.0, 6.))
	ax = fig.add_subplot(111)
	fig.subplots_adjust(bottom=0.3)
	plt.yticks(fontsize=24)
	ax.set_ylim([0.5, 1.0])

	bpFish = plt.boxplot(allFish1, positions=np.array(range(len(allFish1)))*4.0-0.4, sym='', widths=0.6)
	bpMLP = plt.boxplot(allMLP1, positions=np.array(range(len(allMLP1)))*4.0+0.4, sym='', widths=0.6)
	bpNSGA = plt.boxplot(allNSGA1, positions=np.array(range(len(allNSGA1)))*4.0+1.2, sym='', widths=0.6)
	bpESN = plt.boxplot(allESN1, positions=np.array(range(len(allESN1)))*4.0+2.0, sym='', widths=0.6)
#	set_box_color(bpFish, '#969696')
#	set_box_color(bpMLP, '#636363')
#	set_box_color(bpESN, '#252525')

	for i in range(len(allFish1)):
		ax.text(i * 4.0 - 0.4, 1.03, "Control", horizontalalignment='center', fontsize=18)
		ax.text(i * 4.0 + 0.4, 1.0, "Sim-MonoObj-MLP", horizontalalignment='center', fontsize=18)
		ax.text(i * 4.0 + 1.2, 1.03, "Sim-MultiObj-MLP", horizontalalignment='center', fontsize=18)
		ax.text(i * 4.0 + 2.0, 1.0, "Sim-MultiObj-ESN", horizontalalignment='center', fontsize=18)

	ticks = ['Biomimetism\nScore', 'Inter-individual\nScore', 'Linear speed\nScore']
	plt.xticks(0.75 + np.array(range(0, len(ticks) * 4, 4)), ticks, fontsize=24)
	plt.xlim(-1.0, len(ticks)*4 - 1.2)

#	plt.plot([], c='#969696', label="Fish")
#	plt.plot([], c='#636363', label="MLP")
#	plt.plot([], c='#252525', label="ESN")
#	ax.legend(shadow=False, fontsize=24)

	sns.despine()
	plt.tight_layout(rect=[0, 0, 1.0, 0.95])
	fig.savefig(os.path.join(options.outputDir, "AllScores1.pdf"))
	plt.close(fig)




	plt.style.use('grayscale')
	fig = plt.figure(figsize=(6.*scipy.constants.golden * 2.0, 6.))
	ax = fig.add_subplot(111)
	fig.subplots_adjust(bottom=0.3)
	plt.yticks(fontsize=24)
	ax.set_ylim([0.5, 1.0])

	bpFish = plt.boxplot(allFish2, positions=np.array(range(len(allFish2)))*4.0-0.4, sym='', widths=0.6)
	bpMLP = plt.boxplot(allMLP2, positions=np.array(range(len(allMLP2)))*4.0+0.4, sym='', widths=0.6)
	bpNSGA = plt.boxplot(allNSGA2, positions=np.array(range(len(allNSGA2)))*4.0+1.2, sym='', widths=0.6)
	bpESN = plt.boxplot(allESN2, positions=np.array(range(len(allESN2)))*4.0+2.0, sym='', widths=0.6)
#	set_box_color(bpFish, '#969696')
#	set_box_color(bpMLP, '#636363')
#	set_box_color(bpESN, '#252525')

	for i in range(len(allFish2)):
		ax.text(i * 4.0 - 0.4, 1.03, "Control", horizontalalignment='center', fontsize=18)
		ax.text(i * 4.0 + 0.4, 1.0, "Sim-MonoObj-MLP", horizontalalignment='center', fontsize=18)
		ax.text(i * 4.0 + 1.2, 1.03, "Sim-MultiObj-MLP", horizontalalignment='center', fontsize=18)
		ax.text(i * 4.0 + 2.0, 1.0, "Sim-MultiObj-ESN", horizontalalignment='center', fontsize=18)

	ticks = ['Angular speed\nScore', 'Polarisation\nScore', 'Nearest wall\nScore']
	plt.xticks(0.75 + np.array(range(0, len(ticks) * 4, 4)), ticks, fontsize=24)
	plt.xlim(-1.0, len(ticks)*4 - 1.2)

#	plt.plot([], c='#969696', label="Fish")
#	plt.plot([], c='#636363', label="MLP")
#	plt.plot([], c='#252525', label="ESN")
#	ax.legend(shadow=False, fontsize=24)

	sns.despine()
	#plt.tight_layout()
	plt.tight_layout(rect=[0, 0, 1.0, 0.95])
	fig.savefig(os.path.join(options.outputDir, "AllScores2.pdf"))
	plt.close(fig)



# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
