#!/bin/bash

export DISPLAY=:0.0

RESULTSDIR=res6
LOGSDIR=logs6

NBRUNS=1
NBGENS=500 #1500 #300
NBINDIV=120 #240 #60
NBRESTARTS=5
NBSIMSPEREVALFORSTOCHANN=5

#NBSTEPS=26996 #1800 #4500 #9000 #26996
#NBSTEPSGENERATION=26996

declare -A nbSteps
declare -A nbStepsGeneration
nbSteps[1]=1796
nbStepsGeneration[1]=1796
nbSteps[5]=600 #3600 #8996
nbStepsGeneration[5]=8996
nbSteps[15]=10800 #3600 #26996
nbStepsGeneration[15]=26996

declare -A optimRefDataDir
declare -A genRefDataDir
optimRefDataDir[1]=data/referenceNoGap1FPS/ #data/smallReferenceNoGap1FPS/
genRefDataDir[1]=data/referenceNoGap1FPS/
optimRefDataDir[5]=data/referenceNoGap5FPS/ #data/smallReferenceNoGap5FPS/
genRefDataDir[5]=data/referenceNoGap5FPS/
optimRefDataDir[15]=data/smallReferenceNoGap15FPS/
genRefDataDir[15]=data/referenceNoGap15FPS/



##optimRefDataDir=data/small27000ReferenceNoGap15FPS/
#optimRefDataDir=data/small27000ReferenceNoGap15FPS/
##optimRefDataDir=data/smallReferenceNoGap15FPS
#genRefDataDir=data/referenceNoGap15FPS/
##genRefDataDir=data/small27000ReferenceNoGap15FPS/
#
#optimRef1FPSDataDir=data/small27000ReferenceNoGap1FPS/
#genRef1FPSDataDir=data/small27000ReferenceNoGap1FPS/



export OPENBLAS_MAIN_FREE=1

mkdir -p $RESULTSDIR
mkdir -p $LOGSDIR


declare -a pids





#
##### CMAESV5_MLP ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 5; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 1 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimCMAESv5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#

#
##### CMAESV5_MLP10 ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 3; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP10_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP10_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP10_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 1 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimCMAESv5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP10 -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#


#
##### NSGA3V5_MLP ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 3; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 6 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP --fitnessType PerfGendivBehavdiv -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#

#
##### NSGA3V5TRAJENV2_MLP ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 1; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 7 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP --fitnessType PerfTrajobjEnvobj2 -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#

#
##### NSGA3V5TRAJENV3_MLP ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 5; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV3_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV3_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV3_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 1 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP --fitnessType PerfTrajobjEnvobj3 -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#

#
##### NSGA3V5PERFALLOBJ_MLP ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 3; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 2 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP --fitnessType PerfAllobj -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#

#
##### NSGA3V5PERFNOVELTY ####
#for fps in 15; do
#	for nbAgents in 3; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 8 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP --fitnessType PerfNovelty -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#

#
##### NSGA3V5PERFNOVELTY5_MLP10 ####
#for fps in 15; do
#	for nbAgents in 5; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY5_MLP10_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY5_MLP10_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY5_MLP10_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 1 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP10 --fitnessType PerfNovelty5 -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#











#
##### NSGA3V5_MLP2 ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 5; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 1 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP2 -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#





#
##### NSGA3V5TRAJENV_MLP ####
#for fps in 15; do
#	#for nbAgents in 1 2 3 4 5; do
#	for nbAgents in 1; do
#		echo "\n\n#############  ${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}  #############"
#		OUTPUTDIR=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}
#		LOGDIR=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}/
#		mkdir -p $OUTPUTDIR
#		mkdir -p $LOGDIR
#		for i in $(seq 1 $NBRUNS); do
#			INSTANCEOUTPUTDIR=$OUTPUTDIR/$i
#			INSTANCELOGFILE=$LOGDIR/$i
#			mkdir -p $INSTANCEOUTPUTDIR
#			nice -n 19 ./optimNSGA3v5.py -i ${optimRefDataDir[$fps]} -I ${genRefDataDir[$fps]} -o $INSTANCEOUTPUTDIR -a ZonedEmpty.bmp -F $fps -z 2 -s $i -n 1 -S ${nbSteps[$fps]} --nbStepsGeneration ${nbStepsGeneration[$fps]} --annStepsPerSimStep 1 --nbSimulatedAgents $nbAgents -T MLP --fitnessType PerfTrajobjEnvobj -g $NBGENS -L $NBINDIV > $INSTANCELOGFILE 2>&1 #& disown
#		done
#	done
#done
#






# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
