#!/usr/bin/env python3

"""
TODO
"""


############################### IMPORTS ################################ {{{1
########################################################################
from __future__ import print_function

import numpy as np
import math
import copy
import sys
import matplotlib.pyplot as plt
import os
import pickle
from collections import deque

from vision import *

import keras
from keras.models import model_from_yaml

#from joblib import Parallel, delayed


#import theano
#theano.config.openmp = True



__author__ = "Leo Cazenille"
__license__ = "WTFPL"
__version__ = "0.1"



############################### LOCALS ################################# {{{1
########################################################################

def _normAngle(angle):
	return math.fmod(angle + 2. * math.pi, 2. * math.pi)

################################## NN ################################## {{{1
########################################################################


############################ BASE FUNCTIONS ############################ {{{1
########################################################################


def saveData(x, y, theta, time, outputFilename):
	ofile = open(outputFilename, 'w')

	# Create labels
	labels = ['timeStep']
	for i in range(x.shape[1]):
		labels.append('fish' + str(i) + 'X')
		labels.append('fish' + str(i) + 'Y')
		#labels.append('fish' + str(i) + 'Direction')
	ofile.write("\t".join(labels) + "\n")

	# Save agents positions
	for t in range(x.shape[0]):
		stepResults = []
		stepResults.append("%.3f" % (time[t]))
		for i in range(x.shape[1]):
			stepResults.append(str(np.round( float(x[t,i]), 3)))
			stepResults.append(str(np.round( float(y[t,i]), 3)))
			#stepResults.append(str(np.round( float(theta[t,i]), 3)))
		ofile.write("\t".join(stepResults) + "\n")

	ofile.close()


#def performParallel(yamlModel, modelWeights, dt, nbSteps, nbAgents, outputFilename):
#	model = model_from_yaml(yamlModel)
#	model.set_weights(modelWeights)
#	model.reset_states()
#	print("performing: ", outputFilename)
#	performSim(model, dt, nbSteps, nbAgents, outputFilename)


def performSim(model, i, dt, nbSteps, config, outputFilename):
	nbAgents = config['nbAgents']
	timeWindowSize = config['timeWindowSize']
	distancePerceptionAgents = config['distancePerceptionAgents']
	distancePerceptionWalls = config['distancePerceptionWalls']
	visionSim = Vision(nbAgents)
	#time = 0.0
	#nextX = np.random.random(nbAgents)
	#nextY = np.random.random(nbAgents)
	#nextTheta = np.random.uniform(0.0, 2*np.pi, nbAgents)
	#visionSim.step(nextX, nextY, nextTheta, time)
	time, nextX, nextY, nextTheta = initVisionSim(visionSim, i)

	x = np.empty(shape = (nbSteps, nbAgents))
	y = np.empty(shape = (nbSteps, nbAgents))
	theta = np.empty(shape = (nbSteps, nbAgents))
	timeArray = np.empty(shape = (nbSteps))

	yamlModel = model.to_yaml()
	modelWeights = model.get_weights()
	models = [model_from_yaml(yamlModel) for i in range(nbAgents)]
	for m in models:
		m.set_weights(modelWeights)
		m.reset_states()


	# Init prevInputs
	prevInputs = [ deque(maxlen=timeWindowSize) for i in range(nbAgents) ]
	for i in range(nbAgents):
		for w in range(timeWindowSize):
			prevInputs[i].append(np.zeros(shape=visionSim.nbInputs()))

	for t in range(nbSteps):
		x[t] = nextX
		y[t] = nextY
		theta[t] = nextTheta
		timeArray[t] = time

		for i in range(nbAgents):
			#inputs = np.array(visionSim.inputs[i])
			prevInputs[i].append(np.copy(visionSim.inputs[i]))
			#inputs = inputs.reshape(1, 1, inputs.shape[0])
			inputs = np.array(prevInputs[i])
			#inputs = inputs.reshape(1, inputs.shape[0], inputs.shape[1])

			# Predict outputs
			outputs = models[i].predict(inputs)
			#deltaAngle = outputs[0,0] * (2. * np.pi)
			deltaAngle = outputs[0,0] * (visionSim.maxDeltaAngle - visionSim.minDeltaAngle) + visionSim.minDeltaAngle
			instLinearSpeed = outputs[0,1] * visionSim.maxLinearSpeed
			#instLinearSpeed = 0.16
			#print("inputs: ", inputs)
			#print("generated: ", deltaAngle, instLinearSpeed)

			# Generate next coords
			newDirection = _normAngle(visionSim.agents[i]['theta'] + deltaAngle)
			#print(deltaAngle, newDirection)
			newX = visionSim.agents[i]['x'] + dt * instLinearSpeed * np.cos(newDirection)
			newY = visionSim.agents[i]['y'] + dt * instLinearSpeed * np.sin(newDirection)

			if newX > 1.0 - visionSim.agentLength:
				newX = 1.0 - visionSim.agentLength
			#	if newY > 1.0 - visionSim.agentLength:
			#		dir1 = np.pi
			#		dir2 = 3. * np.pi / 2.
			#	else:
			#		dir1 = np.pi / 2.
			#		dir2 = 3. * np.pi / 2.
			#	if _normAngle(newDirection - dir1) > _normAngle(newDirection - dir2):
			#		newDirection = dir1
			#	else:
			#		newDirection = dir2
				newDirection = _normAngle(np.pi - newDirection)
			elif newX < 0.0 + visionSim.agentLength:
				newX = 0.0 + visionSim.agentLength
			#	if newY > 1.0 - visionSim.agentLength:
			#		dir1 = 0.
			#		dir2 = 3. * np.pi / 2.
			#	else:
			#		dir1 = np.pi / 2.
			#		dir2 = 3. * np.pi / 2.
			#	if _normAngle(newDirection - dir1) > _normAngle(newDirection - dir2):
			#		newDirection = dir1
			#	else:
			#		newDirection = dir2
				newDirection = _normAngle(np.pi - newDirection)
			if newY > 1.0 - visionSim.agentLength:
				newY = 1.0 - visionSim.agentLength
			#	if newX > 1.0 - visionSim.agentLength:
			#		dir1 = np.pi
			#		dir2 = 3. * np.pi / 2.
			#	else:
			#		dir1 = 0.
			#		dir2 = np.pi
			#	if _normAngle(newDirection - dir1) > _normAngle(newDirection - dir2):
			#		newDirection = dir1
			#	else:
			#		newDirection = dir2
				newDirection = _normAngle(-newDirection)
			elif newY < 0.0 + visionSim.agentLength:
				newY = 0.0 + visionSim.agentLength
			#	if newX > 1.0 - visionSim.agentLength:
			#		dir1 = np.pi
			#		dir2 = np.pi / 2.
			#	else:
			#		dir1 = 0.
			#		dir2 = np.pi
			#	if _normAngle(newDirection - dir1) > _normAngle(newDirection - dir2):
			#		newDirection = dir1
			#	else:
			#		newDirection = dir2
				newDirection = _normAngle(-newDirection)

			nextX[i] = newX
			nextY[i] = newY
			nextTheta[i] = newDirection

		time += dt
		#print("coords: ", nextX, nextY, nextTheta, time)
		visionSim.step(nextX, nextY, nextTheta, time)

	saveData(x, y, theta, timeArray, outputFilename)



# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
