#!/bin/bash

export DISPLAY=:0.0

NBRUNS=10
NBGENS=500
NBINDIV=120 #240
RESULTSDIR=res6
PLOTSDIR=plots6
LOGSDIR=logs6


declare -a pids

declare -A refDataDirs
declare -A refPlotDirs
declare -A refDataFPS
declare -A dataDirs
declare -A dataDirsBestInstance
declare -A logsDirs
declare -A plotDirs
declare -A dataFPS
declare -A dataName
declare -A dataNbAgents
declare -a dataNamesList
declare -A plotFitness


arenaFilename=ZonedEmpty.bmp


# Define data paths
#for fps in 1 5 15; do
for fps in 15; do
	# Define reference data paths
	refDataDirs[${fps}FPS]=data/referenceNoGap${fps}FPS/
	refPlotDirs[${fps}FPS]=$PLOTSDIR/referenceNoGap${fps}FPS/
	refDataFPS[${fps}FPS]=${fps}

	# Define experiment data paths
	#for nbAgents in 1 2 3 4 5; do
	for nbAgents in 1 3 5; do

		dataDirs[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}
		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}/1
		logsDirs[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}
		plotDirs[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}
		dataFPS[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=${fps}
		#dataName[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-SP\n${nbAgents}~Agents"
		dataName[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]="CMA-ES"
		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=$nbAgents
		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}")
		plotFitness[${fps}FPS_${nbAgents}AGENTS_CMAESV5_MLP_${NBGENS}_${NBINDIV}]=YES


#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-SP\n${nbAgents} agents"

#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3ALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3ALLOBJ_MLP_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3ALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3ALLOBJ_MLP_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3ALLOBJ_MLP_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-B\n${nbAgents} agents"

		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}
		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}/1
		logsDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}
		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}
		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=${fps}
		#dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-SP\n${nbAgents}~Agents"
		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-SP"
		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=$nbAgents
		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}")
		plotFitness[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP_${NBGENS}_${NBINDIV}]=YES

#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_MLP2_${NBGENS}_${NBINDIV}]="NSGA-III-SP\n${nbAgents}MLP2"
#
#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_ESN_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_ESN_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_ESN_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5_ESN_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_ESN_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5_ESN_${NBGENS}_${NBINDIV}]="NSGA-III-SP\n${nbAgents}ESN"
#
#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5ENVONLY_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5ENVONLY_MLP_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5ENVONLY_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5ENVONLY_MLP_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5ENVONLY_MLP_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5ENVONLY_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-EO\n${nbAgents}MLP"
#
#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-TE\n${nbAgents}MLP"

		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}
		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}/1
		logsDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}
		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}
		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=${fps}
		#dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-B\n${nbAgents}~Agents"
		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-Nov"
		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=$nbAgents
		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}")
		plotFitness[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFNOVELTY_MLP_${NBGENS}_${NBINDIV}]=YES

		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}
		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}/1
		logsDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}
		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}
		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=${fps}
		#dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-B\n${nbAgents}~Agents"
		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-CM"
		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=$nbAgents
		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}")
		plotFitness[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_MLP_${NBGENS}_${NBINDIV}]=YES


		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}
		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}/1
		logsDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}
		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}
		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=${fps}
		#dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-B\n${nbAgents}~Agents"
		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]="NSGA-III-AM"
		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=$nbAgents
		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}")
		plotFitness[${fps}FPS_${nbAgents}AGENTS_NSGA3V5PERFALLOBJ_MLP_${NBGENS}_${NBINDIV}]=YES


		dataDirs[3FPS_${nbAgents}AGENTS_SL_MLP]=$RESULTSDIR/3FPS_${nbAgents}AGENTS_SL_MLP
		dataDirsBestInstance[3FPS_${nbAgents}AGENTS_SL_MLP]=$RESULTSDIR/3FPS_${nbAgents}AGENTS_SL_MLP/1
		logsDirs[3FPS_${nbAgents}AGENTS_SL_MLP]=$LOGSDIR/3FPS_${nbAgents}AGENTS_SL_MLP
		plotDirs[3FPS_${nbAgents}AGENTS_SL_MLP]=$PLOTSDIR/3FPS_${nbAgents}AGENTS_SL_MLP
		dataFPS[3FPS_${nbAgents}AGENTS_SL_MLP]=3
		dataName[3FPS_${nbAgents}AGENTS_SL_MLP]="SL"
		dataNbAgents[3FPS_${nbAgents}AGENTS_SL_MLP]=$nbAgents
		dataNamesList+=("3FPS_${nbAgents}AGENTS_SL_MLP")
		plotFitness[3FPS_${nbAgents}AGENTS_SL_MLP]=NO


#		dataDirs[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100
#		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100/1
#		logsDirs[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]="Random"
#		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100]=$nbAgents
#		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_RANDOM_MLP_1_100")


#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}
#		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}/1
#		logsDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]=${fps}
#		#dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]="NSGA-III-B\n${nbAgents}~Agents"
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]="B"
#		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}]=$nbAgents
#		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV2_ESN2_${NBGENS}_${NBINDIV}")
#
#		dataDirs[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100
#		dataDirsBestInstance[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100/1
#		logsDirs[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]=$LOGSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]="Random"
#		dataNbAgents[${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100]=$nbAgents
#		dataNamesList+=("${fps}FPS_${nbAgents}AGENTS_RANDOM_ESN2_1_100")
	done

done


dataDirsBestInstance[15FPS_1AGENTS_CMAESV5_MLP_500_120]=$RESULTSDIR/15FPS_1AGENTS_CMAESV5_MLP_500_120/9
dataDirsBestInstance[15FPS_3AGENTS_CMAESV5_MLP_500_120]=$RESULTSDIR/15FPS_3AGENTS_CMAESV5_MLP_500_120/1
dataDirsBestInstance[15FPS_5AGENTS_CMAESV5_MLP_500_120]=$RESULTSDIR/15FPS_5AGENTS_CMAESV5_MLP_500_120/10
dataDirsBestInstance[15FPS_1AGENTS_NSGA3V5_MLP_500_120]=$RESULTSDIR/15FPS_1AGENTS_NSGA3V5_MLP_500_120/3
dataDirsBestInstance[15FPS_3AGENTS_NSGA3V5_MLP_500_120]=$RESULTSDIR/15FPS_3AGENTS_NSGA3V5_MLP_500_120/3 # XXX
dataDirsBestInstance[15FPS_5AGENTS_NSGA3V5_MLP_500_120]=$RESULTSDIR/15FPS_5AGENTS_NSGA3V5_MLP_500_120/9
dataDirsBestInstance[15FPS_1AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120]=$RESULTSDIR/15FPS_1AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120/1 # XXX
dataDirsBestInstance[15FPS_3AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120]=$RESULTSDIR/15FPS_3AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120/6 # XXX
dataDirsBestInstance[15FPS_5AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120]=$RESULTSDIR/15FPS_5AGENTS_NSGA3V5PERFNOVELTY_MLP_500_120/1 # XXX
dataDirsBestInstance[15FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_120]=$RESULTSDIR/15FPS_1AGENTS_NSGA3V5TRAJENV2_MLP_500_120/6 # XXX
dataDirsBestInstance[15FPS_3AGENTS_NSGA3V5TRAJENV2_MLP_500_120]=$RESULTSDIR/15FPS_3AGENTS_NSGA3V5TRAJENV2_MLP_500_120/5
dataDirsBestInstance[15FPS_5AGENTS_NSGA3V5TRAJENV2_MLP_500_120]=$RESULTSDIR/15FPS_5AGENTS_NSGA3V5TRAJENV2_MLP_500_120/2
dataDirsBestInstance[15FPS_1AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120]=$RESULTSDIR/15FPS_1AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120/6
dataDirsBestInstance[15FPS_3AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120]=$RESULTSDIR/15FPS_3AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120/1 # XXX
dataDirsBestInstance[15FPS_5AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120]=$RESULTSDIR/15FPS_5AGENTS_NSGA3V5PERFALLOBJ_MLP_500_120/8
#dataDirsBestInstance[15FPS_1AGENTS_RANDOM_MLP_500_100]=$RESULTSDIR/15FPS_1AGENTS_RANDOM_MLP_500_120/5
#dataDirsBestInstance[15FPS_3AGENTS_RANDOM_MLP_500_100]=$RESULTSDIR/15FPS_3AGENTS_RANDOM_MLP_500_120/5
#dataDirsBestInstance[15FPS_5AGENTS_RANDOM_MLP_500_100]=$RESULTSDIR/15FPS_5AGENTS_RANDOM_MLP_500_120/6
#dataDirsBestInstance[15FPS_1AGENTS_NSGA3V5TRAJENV2_ESN2_500_120]=$RESULTSDIR/15FPS_1AGENTS_NSGA3V5TRAJENV2_ESN2_500_120/1
#dataDirsBestInstance[15FPS_3AGENTS_NSGA3V5TRAJENV2_ESN2_500_120]=$RESULTSDIR/15FPS_3AGENTS_NSGA3V5TRAJENV2_ESN2_500_120/1
#dataDirsBestInstance[15FPS_5AGENTS_NSGA3V5TRAJENV2_ESN2_500_120]=$RESULTSDIR/15FPS_5AGENTS_NSGA3V5TRAJENV2_ESN2_500_120/1
#dataDirsBestInstance[15FPS_1AGENTS_RANDOM_ESN2_500_100]=$RESULTSDIR/15FPS_1AGENTS_RANDOM_ESN2_500_100/6
#dataDirsBestInstance[15FPS_3AGENTS_RANDOM_ESN2_500_100]=$RESULTSDIR/15FPS_3AGENTS_RANDOM_ESN2_500_100/8
#dataDirsBestInstance[15FPS_5AGENTS_RANDOM_ESN2_500_100]=$RESULTSDIR/15FPS_5AGENTS_RANDOM_ESN2_500_100/8

# XXX
dataDirsBestInstance[3FPS_1AGENTS_SL_MLP]=$RESULTSDIR/3FPS_1AGENTS_SL_MLP/1
dataDirsBestInstance[3FPS_3AGENTS_SL_MLP]=$RESULTSDIR/3FPS_3AGENTS_SL_MLP/1
dataDirsBestInstance[3FPS_5AGENTS_SL_MLP]=$RESULTSDIR/3FPS_5AGENTS_SL_MLP/1



#for fps in 1; do
#	for nbAgents in 5; do
#		dataDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_ESN2_${NBGENS}_${NBINDIV}]=$RESULTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_ESN2_${NBGENS}_${NBINDIV}
#		plotDirs[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_ESN2_${NBGENS}_${NBINDIV}]=$PLOTSDIR/${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_ESN2_${NBGENS}_${NBINDIV}
#		dataFPS[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_ESN2_${NBGENS}_${NBINDIV}]=${fps}
#		dataName[${fps}FPS_${nbAgents}AGENTS_NSGA3V5TRAJENV_ESN2_${NBGENS}_${NBINDIV}]="NSGA-III-TE\n${nbAgents}ESN2"
#	done
#done


function makePlot {
	local dataDir instanceDataDir plotDir instancePlotDir
	name=$1
	dataDir=$2
	plotDir=$3
	FPS=$4
	shift; shift; shift; shift;

	if [ -e $dataDir/1 ]; then
		for i in $(seq 1 $NBRUNS); do
			instanceDataDir=$dataDir/$i
			instancePlotDir=$plotDir/$i
			mkdir -p $instanceDataDir
			mkdir -p $instancePlotDir
			./analyse_presences.py -d $instanceDataDir -o $instancePlotDir -x 1 -y 1 -a 5 -r 0 -S "$name.pdf" -C 2 -M 'Blues' -R 4 &
			pids+=($!)
			./analyse_traces.py -d $instanceDataDir -o $instancePlotDir -x 1 -y 1 -a 5 -r 0 -S "$name.pdf" -C 2 -M 'Blues' -f 3 &
			pids+=($!)
			./plotsFeatures.py -i $instanceDataDir -o $instancePlotDir -a $arenaFilename -z 2 -O "$name.pdf" -M 'b' -F $FPS &
			pids+=($!)
			#./computeScores.py -r $referenceDataDirNoGap -i $instanceDataDir -o $instancePlotDir -a $arenaFilename -z 2 &
			#pids+=($!)
		done
	else
		instancePlotDir=$plotDir/1
		mkdir -p $instancePlotDir
		./analyse_presences.py -d $dataDir -o $instancePlotDir -x 1 -y 1 -a 5 -r 0 -S "$name.pdf" -C 2 -M 'Blues' -R 4 &
		pids+=($!)
		./analyse_traces.py -d $dataDir -o $instancePlotDir -x 1 -y 1 -a 5 -r 0 -S "$name.pdf" -C 2 -M 'Blues' -f 3 &
		pids+=($!)
		./plotsFeatures.py -i $dataDir -o $instancePlotDir -a $arenaFilename -z 2 -O "$name.pdf" -M 'b' -F $FPS &
		pids+=($!)
	fi
}


# Make all reference plots
for i in "${!refDataDirs[@]}"; do
	# Make sure data and plots directories exist
	mkdir -p "${refDataDirs[$i]}"
	mkdir -p "${refPlotDirs[$i]}"
	# Make plots
	makePlot "$i" "${refDataDirs[$i]}" "${refPlotDirs[$i]}" "${refDataFPS[$i]}"
done


# Make all experiment plots
for i in "${!dataDirs[@]}"; do
	# Make sure data and plots directories exist
	mkdir -p "${dataDirs[$i]}"
	mkdir -p "${plotDirs[$i]}"
	# Make plots
	makePlot "$i" "${dataDirs[$i]}" "${plotDirs[$i]}" "${dataFPS[$i]}"
	# Wait for plots to finish
	for i in "${pids[@]}"; do
		wait $i
	done
done




for fps in 15; do
	./plotFitness.py --nbEvalsPerGen $NBINDIV --outputSuffix "Fitness"  -o $PLOTSDIR `for j in "${dataNamesList[@]}"; do if [ "${plotFitness[$j]}" == "YES" ]; then echo \"${dataName[$j]}\" ${dataDirs[$j]} ${dataNbAgents[$j]}; fi; done`  &
	pids+=($!)
	./plotAllScores.py --byTrajFile --outputSuffix "BestScores"  -o $PLOTSDIR -a $arenaFilename -z 2 -F 5 --refPath ${refDataDirs["${fps}FPS"]} --refLabel "Control"  "Control" ${refDataDirs["${fps}FPS"]} 0 `for j in "${dataNamesList[@]}"; do echo \"${dataName[$j]}\" ${dataDirsBestInstance[$j]} ${dataNbAgents[$j]}; done`  &
	pids+=($!)
done


## OLD ?
#for fps in 15; do
##	./plotAllScoresFromLogs.py --outputSuffix "AllScores"  -o $PLOTSDIR `for j in "${dataNamesList[@]}"; do echo \"${dataName[$j]}\" ${logsDirs[$j]} ${dataNbAgents[$j]}; done`  &
##	pids+=($!)
##	./plotAllScores.py  --outputSuffix "AllScores"  -o $PLOTSDIR -a $arenaFilename -z 2 -F 5 --refPath ${refDataDirs["${fps}FPS"]} `for j in "${dataNamesList[@]}"; do echo \"${dataName[$j]}\" ${dataDirs[$j]} ${dataNbAgents[$j]}; done`  &
##	pids+=($!)
#
#	#./plotAllScores.py -o $PLOTSDIR -a $arenaFilename -z 2 -F 5   "Reference" ${refDataDirs["${fps}FPS"]} `for j in ${!dataName[@]}; do echo \"${dataName[$j]}\" ${dataDirs[$j]}; done`  &
#	#./plotAllScores.py -o $PLOTSDIR -a $arenaFilename -z 2 -F 5   "Reference" ${refDataDirs["${fps}FPS"]} `for j in ${!dataName[@]}; do echo \"${dataName[$j]}\" ${dataDirsBestInstance[$j]}; done`  &
#	#./plotAllScores.py -o $PLOTSDIR -a $arenaFilename -z 2 -F 5  `for j in ${!dataName[@]}; do echo \"${dataName[$j]}\" ${dataDirs[$j]}; done`  &
#done



# OLD
#
#referenceDataDir=data/referenceGap15FPS
#referenceDataDirNoGap=data/referenceNoGap15FPS
#referencePlotDir=$PLOTSDIR/reference
#
#reference1FPSDataDirNoGap=data/small27000ReferenceNoGap1FPS/
#reference1FPSPlotDir=$PLOTSDIR/reference1FPS
#reference050FPSDataDirNoGap=data/small27000ReferenceNoGap0.50FPS/
#reference050FPSPlotDir=$PLOTSDIR/reference0.50FPS
#
#ALL_CMAES_MLP_100_DataDir=$RESULTSDIR/ALL_CMAES_MLP_100

	#./plotAllScores.py -o $PLOTSDIR -a $arenaFilename -z 2 -F 5   "Reference" ${refDataDirs["${fps}FPS"]} `for j in ${!dataName[@]}; do echo \"${dataName[$j]}\" ${dataDirs[$j]}; done`  &
	#./plotAllScores.py -o $PLOTSDIR -a $arenaFilename -z 2 -F 5   "Reference" ${refDataDirs["${fps}FPS"]} `for j in ${!dataName[@]}; do echo \"${dataName[$j]}\" ${dataDirsBestInstance[$j]}; done`  &
	#./plotAllScores.py -o $PLOTSDIR -a $arenaFilename -z 2 -F 5  `for j in ${!dataName[@]}; do echo \"${dataName[$j]}\" ${dataDirs[$j]}; done`  &



# OLD
#
#referenceDataDir=data/referenceGap15FPS
#referenceDataDirNoGap=data/referenceNoGap15FPS
#referencePlotDir=$PLOTSDIR/reference
#
#reference1FPSDataDirNoGap=data/small27000ReferenceNoGap1FPS/
#reference1FPSPlotDir=$PLOTSDIR/reference1FPS
#reference050FPSDataDirNoGap=data/small27000ReferenceNoGap0.50FPS/
#reference050FPSPlotDir=$PLOTSDIR/reference0.50FPS
#
#ALL_CMAES_MLP_100_DataDir=$RESULTSDIR/ALL_CMAES_MLP_100
#ALL_CMAES_MLP_100_PlotDir=$PLOTSDIR/ALL_CMAES_MLP_100
#ALL_CMAES_ESN_100_DataDir=$RESULTSDIR/ALL_CMAES_ESN_100
#ALL_CMAES_ESN_100_PlotDir=$PLOTSDIR/ALL_CMAES_ESN_100
#
#ALL_NSGA3_MLP_100_DataDir=$RESULTSDIR/ALL_NSGA3_MLP_100
#ALL_NSGA3_MLP_100_PlotDir=$PLOTSDIR/ALL_NSGA3_MLP_100
#ALL_NSGA3_ESN_100_DataDir=$RESULTSDIR/ALL_NSGA3_ESN_100
#ALL_NSGA3_ESN_100_PlotDir=$PLOTSDIR/ALL_NSGA3_ESN_100
#
#ALL_NSGA3NOVELTY_MLP_100_DataDir=$RESULTSDIR/ALL_NSGA3NOVELTY_MLP_100
#ALL_NSGA3NOVELTY_MLP_100_PlotDir=$PLOTSDIR/ALL_NSGA3NOVELTY_MLP_100
#ALL_NSGA3ALLOBJ_MLP_100_DataDir=$RESULTSDIR/ALL_NSGA3ALLOBJ_MLP_100
#ALL_NSGA3ALLOBJ_MLP_100_PlotDir=$PLOTSDIR/ALL_NSGA3ALLOBJ_MLP_100
#
#ALL_CMAES_SESN_100_DataDir=$RESULTSDIR/ALL_CMAES_SESN_100
#ALL_CMAES_SESN_100_PlotDir=$PLOTSDIR/ALL_CMAES_SESN_100
#
#ALL_MF_NSGA3ANGULAR_MLP_100_240_DataDir=$RESULTSDIR/ALL_MF_NSGA3ANGULAR_MLP_100_240
#ALL_MF_NSGA3ANGULAR_MLP_100_240_PlotDir=$PLOTSDIR/ALL_MF_NSGA3ANGULAR_MLP_100_240
#
#ALL1FPS_NSGA3ALLOBJ_MLP_100_DataDir=$RESULTSDIR/ALL1FPS_NSGA3ALLOBJ_MLP_100
#ALL1FPS_NSGA3ALLOBJ_MLP_100_PlotDir=$PLOTSDIR/ALL1FPS_NSGA3ALLOBJ_MLP_100
#
#
#
#mkdir -p $referencePlotDir
#mkdir -p $reference1FPSPlotDir
#mkdir -p $reference050FPSPlotDir
#
#mkdir -p $ALL_CMAES_MLP_100_DataDir
#mkdir -p $ALL_CMAES_ESN_100_DataDir
#mkdir -p $ALL_NSGA3_MLP_100_DataDir
#mkdir -p $ALL_NSGA3_ESN_100_DataDir
#mkdir -p $ALL_NSGA3NOVELTY_MLP_100_DataDir
#mkdir -p $ALL_NSGA3ALLOBJ_MLP_100_DataDir
#mkdir -p $ALL_CMAES_SESN_100_DataDir
#mkdir -p $ALL1FPS_NSGA3ALLOBJ_MLP_100_DataDir



#
#./plotAllScores.py --inputDir0 $referenceDataDirNoGap --inputDir1 $ALL_CMAES_MLP_100_DataDir --inputDir2 $ALL_NSGA3_MLP_100_DataDir --inputDir3 $ALL_NSGA3_ESN_100_DataDir -o $PLOTSDIR -a $arenaFilename -z 2 -F 5 &
#pids+=($!)
#

#
###########################################################
####################### REFERENCE #########################
###########################################################
#./analyse_presences.py -d $referenceDataDir -o $referencePlotDir -x 1 -y 1 -a 5 -r 0 -S "ref.pdf" -C 2 -M 'Blues' -R 4 &
#pids+=($!)
#
#./analyse_traces.py -d $referenceDataDirNoGap -o $referencePlotDir -x 1 -y 1 -a 5 -r 0 -S "ref.pdf" -C 2 -M 'Blues' -f 3 &
#pids+=($!)
#
#./plotsFeatures.py -i $referenceDataDir -o $referencePlotDir -a $arenaFilename -z 2 -O "ref.pdf" -M 'b' -F 15 &
#pids+=($!)
#

#
###########################################################
#################### REFERENCE 1FPS #######################
###########################################################
#./analyse_presences.py -d $reference1FPSDataDirNoGap -o $reference1FPSPlotDir -x 1 -y 1 -a 5 -r 0 -S "ref.pdf" -C 2 -M 'Blues' -R 4 &
#pids+=($!)
#
#./analyse_traces.py -d $reference1FPSDataDirNoGap -o $reference1FPSPlotDir -x 1 -y 1 -a 5 -r 0 -S "ref.pdf" -C 2 -M 'Blues' -f 3 &
#pids+=($!)
#
#./plotsFeatures.py -i $reference1FPSDataDirNoGap -o $reference1FPSPlotDir -a $arenaFilename -z 2 -O "ref.pdf" -M 'b' -F 1 &
#pids+=($!)
#

#
###########################################################
################## REFERENCE 0.50FPS ######################
###########################################################
#./analyse_presences.py -d $reference050FPSDataDirNoGap -o $reference050FPSPlotDir -x 1 -y 1 -a 5 -r 0 -S "ref.pdf" -C 2 -M 'Blues' -R 4 &
#pids+=($!)
#
#./analyse_traces.py -d $reference050FPSDataDirNoGap -o $reference050FPSPlotDir -x 1 -y 1 -a 5 -r 0 -S "ref.pdf" -C 2 -M 'Blues' -f 3 &
#pids+=($!)
#
#./plotsFeatures.py -i $reference050FPSDataDirNoGap -o $reference050FPSPlotDir -a $arenaFilename -z 2 -O "ref.pdf" -M 'b' -F 1 &
#pids+=($!)
#


###########################################################
#################### OLD SIMULATIONS ######################
###########################################################
#makePlot $ALL_CMAES_MLP_100_DataDir $ALL_CMAES_MLP_100_PlotDir
#makePlot $ALL_CMAES_ESN_100_DataDir $ALL_CMAES_ESN_100_PlotDir
#makePlot $ALL_NSGA3_MLP_100_DataDir $ALL_NSGA3_MLP_100_PlotDir
#makePlot $ALL_NSGA3_ESN_100_DataDir $ALL_NSGA3_ESN_100_PlotDir
#makePlot $ALL_NSGA3NOVELTY_MLP_100_DataDir $ALL_NSGA3NOVELTY_MLP_100_PlotDir

#makePlot $ALL_CMAES_SESN_100_DataDir $ALL_CMAES_SESN_100_PlotDir
#makePlot $ALL_NSGA3ALLOBJ_MLP_100_DataDir $ALL_NSGA3ALLOBJ_MLP_100_PlotDir

#makePlot $ALL_MF_NSGA3ANGULAR_MLP_100_240_DataDir $ALL_MF_NSGA3ANGULAR_MLP_100_240_PlotDir
#makePlot $ALL1FPS_NSGA3ALLOBJ_MLP_100_DataDir $ALL1FPS_NSGA3ALLOBJ_MLP_100_PlotDir



###########################################################

# Wait for plots to finish
for i in "${pids[@]}"; do
	wait $i
done

# MODELINE	"{{{1
# vim:noexpandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
